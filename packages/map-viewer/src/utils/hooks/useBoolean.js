import { useState } from 'react'

/**
 *
 * @param {boolean} defaultState default value  of boolean
 * @returns {object}
 */
export const useBoolean = (defaultValue = false) => {
  const [value, setValue] = useState(!!defaultValue)

  const setTrue = () => setValue(true)
  const setFalse = () => setValue(false)
  const toggle = () => setValue((x) => !x)

  return { value, setValue, setTrue, setFalse, toggle }
}

/**
 * REFERENCE:
 * - [Documentation]: https://usehooks-ts.com/react-hook/use-boolean
 */
