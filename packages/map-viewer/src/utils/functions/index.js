import _ from 'lodash'

export const sortObjectByKey = (object) => {
  return Object.keys(object)
    .sort()
    .reduce(function (result, key) {
      result[key] = object[key]
      return result
    }, {})
}

export const filterObjectByMatchProperty = (object, parameters) => {
  return _.pickBy(object, _.matches(parameters))
}
