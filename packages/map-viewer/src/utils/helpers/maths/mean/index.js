import { mean as _mean } from 'lodash'

/**
 * Find average of the list
 * @param {Array} array list of numbers
 * @returns {number} average value of the list
 */
export const mean = (array = []) => {
  return _mean(array)
}
