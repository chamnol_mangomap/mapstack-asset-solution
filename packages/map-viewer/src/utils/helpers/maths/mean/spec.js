import { mean } from './index'

describe('Helper Function: calculation/mean', () => {
  it('case 1: should return the mean value of the list', () => {
    expect(mean([1, 2, 3, 4, 5])).toBe(3)
  })

  it('case 2: should return the mean value of the list', () => {
    expect(mean([-3, -2, -1, 0])).toBe(-1.5)
  })

  it('case 3: should return the mean value of the list', () => {
    expect(mean([4, 2, 8, 6])).toBe(5)
  })

  it('case 4: should return the mean value of the list', () => {
    expect(mean([24, 9, 19, 98])).toBe(37.5)
  })

  it('case 5: should return the mean value of the list', () => {
    expect(mean([69, 550, 291])).toBeCloseTo(303.33)
  })
})
