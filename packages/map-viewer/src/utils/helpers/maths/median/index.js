/**
 * Find Median or Middle Point of the list
 * @param {Array} array list of numbers
 * @returns {number} return  middle number of the list
 */
export const median = (array = []) => {
  array.sort((a, b) => b - a)
  const length = array.length
  if (length % 2 === 0) {
    return (array[length / 2] + array[length / 2 - 1]) / 2
  } else {
    return array[Math.floor(length / 2)]
  }
}

/**
 * Reference: https://github.com/lodash/lodash/issues/4762
 */
