import { median } from './index'

describe('Helper Function: calculation/median', () => {
  it('case 1: should return the median value of the list', () => {
    expect(median([1, 2, 3, 4, 5])).toBe(3)
  })

  it('case 2: should return the median value of the list', () => {
    expect(median([-3, -2, -1, 0])).toBe(-1.5)
  })

  it('case 3: should return the median value of the list', () => {
    expect(median([-999, 0, 999])).toBe(0)
  })

  it('case 4: should return the median value of the list', () => {
    expect(median([1, 2])).toBe(1.5)
  })

  it('case 5: should return the median value of the list', () => {
    expect(median([-1, -2, -3, -4, -5])).toBe(-3)
  })
})
