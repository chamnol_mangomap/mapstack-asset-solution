import { max } from './index'

describe('Helper Function: calculation/max', () => {
  it('case 1: should return the maximum value of the list', () => {
    expect(max([1, 2, 3, 4, 5])).toBe(5)
  })

  it('case 2: should return the maximum value of the list', () => {
    expect(max([-3, -2, -1, 0])).toBe(0)
  })

  it('case 3: should return the maximum value of the list', () => {
    expect(max([-999, 0, 999])).toBe(999)
  })
})
