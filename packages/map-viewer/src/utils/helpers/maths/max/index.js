import { max as _max } from 'lodash'

/**
 * Find maximum value of array
 * @param {Array} array list of number
 * @returns {number} a maximum value of the list
 */
export const max = (array = []) => {
  return _max(array)
}
