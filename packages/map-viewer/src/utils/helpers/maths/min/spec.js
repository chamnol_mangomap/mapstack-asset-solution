import { min } from './index'

describe('Helper Function: calculation/min', () => {
  it('case 1: should return the minimum value of the list', () => {
    expect(min([1, 2, 3, 4, 5])).toBe(1)
  })

  it('case 2: should return the minimum value of the list', () => {
    expect(min([-3, -2, -1, 0])).toBe(-3)
  })

  it('case 3: should return the minimum value of the list', () => {
    expect(min([-999, 0, 999])).toBe(-999)
  })
})
