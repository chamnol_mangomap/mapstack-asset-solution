import { min as _min } from 'lodash'

/**
 * Find minimum value of array
 * @param {Array} array list of numbers
 * @returns {number} a minimum value of the list
 */
export const min = (array = []) => {
  return _min(array)
}
