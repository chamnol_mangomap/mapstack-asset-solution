import { capitalize } from './index'

describe('Helper Function: capitalize', () => {
  it('case 1: should return first character of each words to be uppercase', () => {
    expect(capitalize('foo')).toBe('Foo')
  })

  it('case 2: should return first character of each words to be uppercase', () => {
    expect(capitalize('foo boo')).toBe('Foo Boo')
  })

  it('case 3: should return first character of each words to be uppercase', () => {
    expect(capitalize('foo boo moo')).toBe('Foo Boo Moo')
  })

  it('case 4: should return first character of each words to be uppercase', () => {
    expect(capitalize('foo Boo Moo')).toBe('Foo Boo Moo')
  })
})
