import { startCase, toLower } from 'lodash'

/**
 * Convert word or sentence to be capitalized.
 * @param {string} text passed
 * @returns capitalized word or sentence
 */
export const capitalize = (text = '') => {
  return startCase(toLower(text))
}
