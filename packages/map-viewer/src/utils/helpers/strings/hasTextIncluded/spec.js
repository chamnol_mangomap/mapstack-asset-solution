import { hasTextIncluded } from './index'

describe('Helper Function: strings/isTextIncludeMedian', () => {
  it('should include `Median` text', () => {
    expect(hasTextIncluded('Median income', 'Median')).toBeTruthy()
  })

  it('should include `Median` text and ignore case', () => {
    expect(hasTextIncluded('median income', 'Median')).toBeTruthy()
  })

  it('should not include `Median` text', () => {
    expect(hasTextIncluded('Population', 'Median')).toBeFalsy()
  })
})
