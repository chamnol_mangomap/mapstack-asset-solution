/**
 * Check if there is a text include inside the string and ignored cases.
 * @param {string} text text needed to check
 * @param {string} include text to check
 * @returns {boolean}
 */
export const hasTextIncluded = (text = '', include = '') => {
  const regex = new RegExp(include, 'ig')
  return regex.test(text)
}
