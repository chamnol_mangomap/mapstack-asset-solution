const metaData = {
  dataSet: '757b64e5-bb80-4316-baeb-334e1684344f',
  version: '55dfeeef-34d3-4e8a-abf7-30750fb0de2r',
  schema: {
    County: {
      type: 'string',
      quantity: false,
      category: true,
      summary: false,
      filter: true
    },
    'County id': {
      type: 'string',
      quantity: false,
      category: true,
      summary: false,
      filter: true
    },
    Population: {
      type: 'number',
      quantity: true,
      category: false,
      summary: true,
      filter: true
    },
    'Median age': {
      type: 'number',
      quantity: true,
      category: false,
      summary: false,
      filter: true
    },
    Households: {
      type: 'number',
      quantity: true,
      category: false,
      summary: true,
      filter: true
    },
    'Median income': {
      type: 'number',
      quantity: true,
      category: false,
      summary: false,
      filter: true
    },
    'Unemployment rate': {
      type: 'number',
      quantity: true,
      category: false,
      summary: false,
      filter: true
    },
    'Median household value': {
      type: 'number',
      quantity: true,
      category: false,
      summary: false,
      filter: true
    },
    'State code': {
      type: 'string',
      quantity: false,
      category: true,
      summary: false,
      filter: true
    },
    State: {
      type: 'string',
      quantity: false,
      category: true,
      summary: false,
      filter: true
    }
  }
}

const newVersionMetaData = {
  dataSet: '757b64e5-bb80-4316-baeb-334e1684344f',
  version: '15dfeeef-34d3-4e8a-abf7-30750fb0de2r'
}

const geojson = {
  name: 'Point Geojson',
  type: 'Feature',
  geometry: {
    type: 'Point',
    coordinates: [125.6, 10.1]
  },
  properties: {
    name: 'Dinagat Islands'
  }
}

const complexGeojson = {
  type: 'FeatureCollection',
  name: 'us-counties',
  crs: { type: 'name', properties: { name: 'urn:ogc:def:crs:OGC:1.3:CRS84' } },
  features: [
    {
      type: 'Feature',
      properties: {
        County: 'Stonewall County',
        'County id': '0500000US48433',
        Population: 1476.0,
        'Median age': 47.1,
        Households: 580.0,
        'Median income': 51250.0,
        'Unemployment rate': 2.6,
        'Median household value': 55600,
        'State code': 'TX',
        State: 'Texas',
        uuid: 'aa8fb43c-219b-4812-8417-77903b85'
      },
      geometry: { type: 'MultiPolygon', coordinates: [] }
    },
    {
      type: 'Feature',
      properties: {
        County: 'Baldwin County',
        'County id': '0500000US01003',
        Population: 2830,
        'Median age': 43,
        Households: 80930,
        'Median income': 58320,
        'Unemployment rate': 4.3,
        'Median household value': 197900,
        'State code': 'AL',
        State: 'Ohio',
        uuid: 'b7ba6b69-734e-4ffe-a92e-ebf45bb1'
      },
      geometry: { type: 'MultiPolygon', coordinates: [] }
    },
    {
      type: 'Feature',
      properties: {
        County: 'Yoakum County',
        'County id': '0500000US48501',
        Population: 8631.0,
        'Median age': 30.3,
        Households: 2617.0,
        'Median income': 70005.0,
        'Unemployment rate': 4.4,
        'Median household value': 116400,
        'State code': 'TX',
        State: 'Texas',
        uuid: 'a2e88596-513c-477b-b5b9-9562be1a'
      },
      geometry: { type: 'MultiPolygon', coordinates: [] }
    },
    {
      type: 'Feature',
      properties: {
        County: 'Terry County',
        'County id': '0500000US48445',
        Population: 12528.0,
        'Median age': 34.6,
        Households: 4061.0,
        'Median income': 44627.0,
        'Unemployment rate': 9.2,
        'Median household value': 76500,
        'State code': 'TX',
        State: 'Texas',
        uuid: 'da35faf1-649e-46ec-84eb-ac8d4d92'
      },
      geometry: { type: 'MultiPolygon', coordinates: [] }
    }
  ]
}

const viewPort = {
  longitude: -97.77832031250001,
  latitude: 37.84015683604136,
  zoom: 5
}

const features = [
  {
    geometry: {
      type: 'Polygon',
      coordinates: [
        [
          [-86.90185546875, 32.54218257955074],
          [-86.90185546875, 32.54218257955074]
        ]
      ]
    },
    type: 'Feature',
    properties: {
      County: 'Autauga County',
      'County id': '0500000US01001',
      Population: 55380,
      'Median age': 38.2,
      Households: 21397,
      'Median income': 58731,
      'Unemployment rate': 3.7,
      'Median household value': 154500,
      'State code': 'AL',
      State: 'Alabama',
      uuid: '7d09d461-5eb7-4520-b23a-46bfd20a'
    },
    id: 0,
    tile: {
      z: 3,
      x: 2,
      y: 3
    }
  },
  {
    type: 'Feature',
    properties: {
      County: 'Henry County',
      'County id': '0500000US01067',
      Population: 17133.0,
      'Median age': 44.4,
      Households: 6630.0,
      'Median income': 50017.0,
      'Unemployment rate': 5.9,
      'Median household value': 118800,
      'State code': 'AL',
      State: 'Alabama',
      uuid: '5de8e085-c5c8-4686-a338-371ecfe5'
    },
    id: 3,
    tile: {
      z: 3,
      x: 2,
      y: 3
    },
    geometry: { type: 'MultiPolygon', coordinates: [] }
  },
  {
    geometry: {
      type: 'Polygon',
      coordinates: [
        [
          [-87.989501953125, 30.557530797259176],
          [-88.0279541015625, 30.75127777625781]
        ]
      ]
    },
    type: 'Feature',
    properties: {
      County: 'Baldwin County',
      'County id': '0500000US01003',
      Population: 212830,
      'Median age': 43,
      Households: 80930,
      'Median income': 58320,
      'Unemployment rate': 4.3,
      'Median household value': 197900,
      'State code': 'AL',
      State: 'Ohio',
      uuid: 'b7ba6b69-734e-4ffe-a92e-ebf45bb1'
    },
    id: 1,
    tile: {
      z: 3,
      x: 2,
      y: 3
    }
  },
  {
    type: 'Feature',
    properties: {
      County: 'Houston County',
      'County id': '0500000US01069',
      Population: 104702.0,
      'Median age': 40.1,
      Households: 39311.0,
      'Median income': 47580.0,
      'Unemployment rate': 6.4,
      'Median household value': 137600,
      'State code': 'AL',
      State: 'Alabama',
      uuid: '8cbe86f9-3023-49a5-a77a-b9a5ab1a'
    },
    id: 4,
    tile: {
      z: 3,
      x: 2,
      y: 3
    },
    geometry: { type: 'MultiPolygon', coordinates: [] }
  }
]

const selectionSet = {
  source: 'geojson-layer',
  filters: [
    {
      id: '458c80db-5010-5938-a581-93a2e695ca81',
      filter: {
        property: 'State',
        operator: '==',
        value: 'Ohio'
      }
    }
  ]
}

const featureSelection = {
  source: 'geojson-layer',
  id: 252,
  geometry: {
    type: 'Polygon',
    coordinates: []
  },
  properties: {
    County: 'Hartford County',
    'County id': '0500000US09003',
    Population: 893561,
    'Median age': 40.4,
    Households: 350408,
    'Median income': 75148,
    'Unemployment rate': 5.8,
    'Median household value': 240600,
    'State code': 'CT',
    State: 'Connecticut',
    uuid: 'b98bfc07-c457-48c1-92f9-c39fa61a'
  }
}

const fitBounds = [125.6, 10.1, 125.6, 10.1]

const filterResults = [
  {
    id: 1,
    geometry: {
      type: 'Polygon',
      coordinates: [
        [-110.9454345703125, 31.728167146023935],
        [-110.4510498046875, 31.73283925365007]
      ]
    },
    properties: {
      County: 'Baldwin County',
      'County id': '0500000US01003',
      Population: 212830,
      'Median age': 43,
      Households: 80930,
      'Median income': 58320,
      'Unemployment rate': 4.3,
      'Median household value': 197900,
      'State code': 'AL',
      State: 'Alabama',
      uuid: 'b7ba6b69-734e-4ffe-a92e-ebf45bb1'
    }
  },
  {
    id: 7,
    geometry: {
      type: 'Polygon',
      coordinates: [
        [
          [-91.8731689453125, 33.39475921857799],
          [-91.4556884765625, 33.39017286472249],
          [-91.461181640625, 33.00405687168934],
          [-92.109375, 33.03169299978312],
          [-92.1258544921875, 33.206520451760625],
          [-91.9775390625, 33.30298618122413],
          [-91.988525390625, 33.399345330420914],
          [-91.8731689453125, 33.39475921857799]
        ]
      ]
    },
    properties: {
      County: 'Calhoun County',
      'County id': '0500000US01015',
      Population: 114618,
      'Median age': 39.6,
      Households: 44605,
      'Median income': 47255,
      'Unemployment rate': 8.1,
      'Median household value': 118000,
      'State code': 'AL',
      State: 'Alabama',
      uuid: 'aa624b90-683f-4948-9f03-e47a4747'
    }
  },
  {
    id: 27,
    geometry: {
      type: 'Polygon',
      coordinates: [
        [-111.68701171875, 33.206520451760625],
        [-111.5826416015625, 33.206520451760625],
        [-111.5826416015625, 33.46352547561378],
        [-111.038818359375, 33.46810795527897],
        [-110.7806396484375, 32.98562797456917]
      ]
    },
    properties: {
      County: 'Etowah County',
      'County id': '0500000US01055',
      Population: 102748,
      'Median age': 41.4,
      Households: 38942,
      'Median income': 44637,
      'Unemployment rate': 5.5,
      'Median household value': 113700,
      'State code': 'AL',
      State: 'Alabama',
      uuid: '1b32526b-674a-41e3-8605-aa9518d5'
    }
  }
]

const mapSelectionOption = {
  id: '1b32546b-674a-41a3-8608-aa9519d5',
  type: 'quantity',
  value: 'Population',
  intervals: ['1233', '1500']
}
const filters = [
  {
    id: '48f54bef-56cb-5f5c-9c43-ad31ce643085',
    filter: {
      property: 'State',
      operator: '==',
      value: 'Alabama'
    }
  },
  {
    id: '86e45228-beed-5812-a0fc-8d2f39d4af3d',
    filter: {
      property: 'Population',
      operator: '<',
      value: '40000'
    }
  },
  {
    id: '05843b94-81cf-515d-90fb-7eab8c0f33ef',
    filter: {
      property: 'Median age',
      operator: '<=',
      value: '24'
    }
  }
]

const filterResultTableData = {
  categories: ['County', 'County id', 'State code', 'State'],
  quantities: [
    'Population',
    'Median age',
    'Households',
    'Median income',
    'Unemployment rate',
    'Median household value'
  ],
  summaries: ['Population', 'Households']
}

export default {
  filterResults,
  viewPort,
  features,
  selectionSet,
  newVersionMetaData,
  metaData,
  geojson,
  complexGeojson,
  fitBounds,
  mapSelectionOption,
  filters,
  featureSelection,
  filterResultTableData
}
