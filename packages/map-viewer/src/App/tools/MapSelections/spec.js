import React from 'react'
import { Provider } from 'react-redux'
import { set, clear } from 'idb-keyval'
import given from 'utils/testing/steps/given'
import { MapSelection } from './index'
import { store } from 'state/store'
import { act } from 'react-dom/test-utils'
import '@testing-library/jest-dom'
import { render, screen, fireEvent } from '@testing-library/react'
import { setMetaData } from 'state/meta/metaSlice'

describe('App', () => {
  const setUp = async () => {
    await set(given.metaData.dataSet, given.complexGeojson)
    store.dispatch(setMetaData(given.metaData))
    render(
      <Provider store={store}>
        <MapSelection open onClose={() => {}} />
      </Provider>
    )
  }

  it('should have selection range', async () => {
    await act(async () => {
      setUp()
      await new Promise((resolve) => setTimeout(resolve, 1000))

      expect(screen.getByTestId('property-Population')).toBeInTheDocument()
      expect(screen.getByTestId('property-None')).toBeInTheDocument()
    })
  })

  it('should update state on add selection', async () => {
    await act(async () => {
      setUp()
      await new Promise((resolve) => setTimeout(resolve, 100))

      fireEvent.click(screen.getByTestId('property-Population'))
      expect(true).toBeTruthy()

      await new Promise((resolve) => setTimeout(resolve, 200))

      const { layerStyle } = store.getState().layerStyle

      expect(layerStyle).toHaveProperty('id')
      expect(layerStyle).toHaveProperty('type')
      expect(layerStyle).toHaveProperty('value')
      expect(layerStyle).toHaveProperty('intervals')
    })
  })

  it('should update state select None', async () => {
    await act(async () => {
      setUp()
      await new Promise((resolve) => setTimeout(resolve, 100))

      fireEvent.click(screen.getByTestId('property-None'))
      expect(true).toBeTruthy()

      await new Promise((resolve) => setTimeout(resolve, 200))

      const { layerStyle } = store.getState().layerStyle

      expect(layerStyle).toHaveProperty('id')
      expect(layerStyle).toHaveProperty('type')
      expect(layerStyle).not.toHaveProperty('value')
      expect(layerStyle).not.toHaveProperty('intervals')
    })
  })

  afterAll(() => {
    clear()
    jest.resetModules()
  })
})
