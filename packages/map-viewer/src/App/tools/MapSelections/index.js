import React, { memo, useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { Chance } from 'chance'
import _ from 'lodash'
import { get } from 'idb-keyval'
import { View } from './view'
import jmespath from 'jmespath'
import geostats from 'geostats'
import { setLayerStyle, layerStyle } from 'state/layerStyle/layerStyleSlice'
import { sortObjectByKey } from 'utils/functions'
import { dataSet, quantitySchema } from 'state/meta/metaSlice'

const chance = new Chance()

// Transform meta data to map selection range
const _transformSelectionOptionList = (arr, type) => {
  const transformedOptions = _.mapValues(arr, (__, key) => {
    return {
      type,
      value: key,
      isSelected: _.isEqual(key)
    }
  })
  return sortObjectByKey(transformedOptions)
}

export const MapSelection = memo((props) => {
  // props
  const { open, onClose } = props

  // states
  const [features, setFeatures] = useState([])
  const [selectionRange, setSelectionRange] = useState({})

  // store
  const dispatch = useDispatch()
  const styleData = useSelector(layerStyle)
  const selectedStyle = _.get(styleData, 'value', 'None')
  const dataSetId = useSelector(dataSet)
  const quantities = useSelector(quantitySchema)

  useEffect(() => {
    setSelectionRangeWithSelectedOption(quantities)
  }, [quantities])

  useEffect(() => {
    const fetchData = async () => {
      const geojson = await get(dataSetId)
      const featureProperties = _.map(geojson.features, 'properties')
      setFeatures(featureProperties)
    }
    dataSetId && fetchData()
  }, [dataSetId])

  const setSelectionRangeWithSelectedOption = (quantities) => {
    const selectionOptions = {
      ..._transformSelectionOptionList(quantities, 'quantity'),
      None: {
        type: 'default',
        value: 'None'
      }
    }
    setSelectionRange(selectionOptions)
  }

  const getFeaturePropsByKey = (key) => {
    return new Set(jmespath.search(features, '[*]."' + key + '"'))
  }

  const handleSelect = (type, key) => {
    let payload
    if (type === 'default') {
      payload = {
        id: chance.guid(),
        type
      }
    } else {
      // Util library mainly used for classification of numbers
      const series = new geostats([...getFeaturePropsByKey(key)])

      // Equally breaking numbers into {breakNumber} classification
      const intervals = series.getClassEqInterval(7)

      payload = {
        id: chance.guid(),
        type,
        value: key,
        intervals
      }
    }
    setSelectionRangeWithSelectedOption(quantities, key)
    dispatch(setLayerStyle(payload))
    onClose()
  }

  return (
    <View
      open={open}
      onClose={onClose}
      range={selectionRange}
      selectedValue={selectedStyle}
      onSelect={handleSelect}
    />
  )
})
