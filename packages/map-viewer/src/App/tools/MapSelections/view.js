import React from 'react'
import { PropChips, Drawer, Box, SideBarHeader } from 'ui-components'

export const View = (props) => {
  // Props
  const { open, range, selectedValue } = props

  // Callback
  const { onClose, onSelect } = props

  return (
    <>
      <Drawer open={open} onClose={onClose}>
        <SideBarHeader onClose={onClose} title='Map Selection' />
        <Box
          sx={{
            width: '100%',
            flex: 1,
            display: 'flex',
            flexDirection: 'column',
            overflowY: 'hidden',
            pl: '15px'
          }}
        >
          <PropChips
            schema={range}
            property={selectedValue}
            onPropertySelected={onSelect}
            direction='column'
          />
        </Box>
      </Drawer>
    </>
  )
}
