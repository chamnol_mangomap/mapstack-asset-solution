import React from 'react'
import { FilterEdit } from './FilterEdit'
import { FilterList } from './FilterList'
import { Drawer } from 'ui-components'

const EDIT = 'edit'
const LIST = 'list'
export const VIEWS = {
  EDIT,
  LIST
}

export const View = (props) => {
  // Props
  const { openPanel, view, editFilterData } = props

  // Callback
  const { onClose, onAddFilter, onFilterAdded, onFilterSelected } = props

  return (
    <>
      <Drawer open={openPanel} onClose={onClose}>
        {view === VIEWS.EDIT && (
          <FilterEdit
            data={editFilterData}
            onClose={onClose}
            onFilterAdded={onFilterAdded}
          />
        )}
        {view === VIEWS.LIST && (
          <FilterList
            onFilterSelected={onFilterSelected}
            onClose={onClose}
            onAddFilter={onAddFilter}
          />
        )}
      </Drawer>
      <div
        style={{
          position: 'absolute',
          height: 'auto',
          bottom: 10,
          right: 10
        }}
      />
    </>
  )
}
