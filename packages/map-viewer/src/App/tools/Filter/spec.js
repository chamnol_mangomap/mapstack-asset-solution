import React from 'react'
import { mount } from 'enzyme'
import { Filter } from './index'
import { Provider } from 'react-redux'
import { store } from 'state/store'
import { Drawer } from 'ui-components'

/**
 * Doing
 */
describe('Filter', () => {
  let filterComponent
  const setup = ({ props }) => {
    filterComponent = mount(
      <Provider store={store}>
        <Filter {...props} />
      </Provider>
    )
  }

  it('should not render a SwipeableDrawer when open set to false', () => {
    setup({
      props: {
        open: false
      }
    })

    const drawerComponent = filterComponent.find(Drawer)
    expect(drawerComponent.props().open).toBe(false)
  })

  it('should render a SwipeableDrawer when open set to true', () => {
    setup({
      props: {
        open: true
      }
    })

    const drawerComponent = filterComponent.find(Drawer)
    expect(drawerComponent.props().open).toBe(true)
  })
})
