import React from 'react'
import {
  FilterProps,
  AlertMsg,
  Range,
  StringOperators,
  StringOperatorAndValue,
  NumericOperatorAndValue,
  NumericOperators,
  SideBarHeader,
  PropChips,
  RegularButton,
  TextInput,
  ChipInput
} from 'ui-components'

export const View = (props) => {
  // Props
  const {
    autocompleteControlledState,
    schema,
    range,
    autoCompleteOptions,
    isStringOperator,
    isNumericOperator,
    filter,
    numericValueError,
    filterError
  } = props

  // Callbacks
  const {
    onClose,
    onConfirm,
    onPropertySelected,
    onOperatorSelected,
    onNumericValueChanged,
    onValueChanged
  } = props

  return (
    <div
      style={{
        height: '100%'
      }}
    >
      <SideBarHeader onClose={onClose} />
      <div style={{ padding: '0px 15px 15px 15px' }}>
        <FilterProps>
          <PropChips
            schema={schema}
            property={filter.property}
            onPropertySelected={onPropertySelected}
          />
        </FilterProps>
        {isStringOperator && (
          <StringOperatorAndValue
            operator={
              <StringOperators
                operator={filter.operator}
                onOperatorSelected={onOperatorSelected}
              />
            }
            value={
              <ChipInput
                controlledState={autocompleteControlledState}
                value={filter.value}
                options={autoCompleteOptions}
                onChange={(value) => {
                  onValueChanged(value)
                }}
              />
            }
          />
        )}
        {isNumericOperator && (
          <NumericOperatorAndValue
            operator={
              <NumericOperators
                operator={filter.operator}
                onOperatorSelected={onOperatorSelected}
              />
            }
            value={
              <TextInput
                color='primary'
                error={numericValueError ? true : false}
                helperText={numericValueError ? numericValueError : ''}
                data-testid='number-value-input'
                type='text'
                value={filter.value}
                name='value'
                onChange={(evt) => onNumericValueChanged(evt.target.value)}
              />
            }
            range={<Range min={range.min} max={range.max} />}
          />
        )}
        {filterError && <AlertMsg severity='error' msg={filterError} />}
        <div
          style={{
            marginTop: '30px'
          }}
        >
          <RegularButton
            data-testid='confirm-edit'
            onClick={onConfirm}
            color='primary'
            round
            block
          >
            Confirm
          </RegularButton>
        </div>
      </div>
    </div>
  )
}
