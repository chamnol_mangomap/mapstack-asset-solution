import React from 'react'
import { FilterEdit } from './index'
import { act } from 'react-dom/test-utils'
import { set, clear } from 'idb-keyval'
import { Provider } from 'react-redux'
import { clearAllFilters } from 'state/filter/filterSlice'
import { store } from 'state/store'
import given from 'utils/testing/steps/given'
import { render, fireEvent, screen } from '@testing-library/react'
import '@testing-library/jest-dom'
import { within } from '@testing-library/dom'
import { ThemeProvider as MUIThemeProvider } from '@mui/material/styles'
import { ThemeProvider } from 'emotion-theming'
import { MapStackTheme } from 'ui-components'
import { setMetaData } from 'state/meta/metaSlice'

describe('App', () => {
  const setUp = async (data) => {
    await set(given.metaData.dataSet, given.complexGeojson)
    const dataProp = data ? { data } : {}
    store.dispatch(setMetaData(given.metaData))
    render(
      <MUIThemeProvider theme={MapStackTheme}>
        <ThemeProvider theme={MapStackTheme}>
          <Provider store={store}>
            <FilterEdit
              onClose={() => {}}
              onFilterAdded={() => {}}
              {...dataProp}
            />
          </Provider>
        </ThemeProvider>
      </MUIThemeProvider>
    )
  }

  it('Checking schema', async () => {
    await act(async () => {
      setUp()
      await new Promise((resolve) => setTimeout(resolve, 100))
      expect(screen.getByTestId('property-County')).toBeInTheDocument()
      expect(
        screen.getByTestId('property-Unemployment rate')
      ).toBeInTheDocument()
    })
  })

  it("Create filter of 'County' property", async () => {
    await act(async () => {
      setUp()
      await new Promise((resolve) => setTimeout(resolve, 100))

      // Select property chip
      fireEvent.click(screen.getByTestId('property-County'))

      expect(screen.getByTestId('string-operator-Is')).toBeInTheDocument()
      expect(screen.getByTestId('string-operator-Is not')).toBeInTheDocument()

      // Select operator chip
      fireEvent.click(screen.getByTestId('string-operator-Is'))

      const autocomplete = screen.getByTestId('auto-complete-chip')

      const input = within(autocomplete).getByRole('textbox')

      autocomplete.click()
      autocomplete.focus()

      fireEvent.change(input, { target: { value: 'Terry County' } })

      // to be sure, or do `findAllByRole` which is also async
      await act(async () => {
        await new Promise((resolve) => setTimeout(resolve, 0))
      })

      fireEvent.click(screen.getAllByRole('option')[0])

      expect(screen.getByTestId('chip-value-0').textContent).toBe(
        'Terry County'
      )

      // Confirm edit
      fireEvent.click(screen.getByTestId('confirm-edit'))

      await new Promise((resolve) => setTimeout(resolve, 100))

      expect(store.getState().filter.filters[0].filter).toEqual({
        property: 'County',
        operator: '==',
        value: 'Terry County'
      })
    })
  })

  it("Create filter of 'Population' property", async () => {
    await act(async () => {
      setUp()
      await new Promise((resolve) => setTimeout(resolve, 100))

      // Select property chip
      fireEvent.click(screen.getByTestId('property-Population'))

      expect(
        screen.getByTestId('number-operator-Greater than')
      ).toBeInTheDocument()
      expect(
        screen.getByTestId('number-operator-Less than or equal to')
      ).toBeInTheDocument()

      // Select opertor chip
      fireEvent.click(
        screen.getByTestId('number-operator-Less than or equal to')
      )

      const field = screen
        .getByTestId('number-value-input')
        .querySelector('input')
      expect(field).toBeInTheDocument()

      fireEvent.change(field, { target: { value: 2000 } })
      expect(field.value).toBe('2000')

      fireEvent.keyDown(field, { key: 'Enter', code: 'Enter', charCode: 13 })

      // Confirm edit
      fireEvent.click(screen.getByTestId('confirm-edit'))

      await new Promise((resolve) => setTimeout(resolve, 100))

      // Assert value range
      expect(screen.getByTestId('number-value-range').textContent).toEqual(
        'Range: 1476 - 12528'
      )

      // checking the value inside store
      expect(store.getState().filter.filters[0].filter).toEqual({
        property: 'Population',
        operator: '<=',
        value: '2000'
      })
    })
  })

  it('Update a filter', async () => {
    await act(async () => {
      setUp(given.selectionSet.filters)
      await new Promise((resolve) => setTimeout(resolve, 100))

      const autocomplete = screen.getByTestId('auto-complete-chip')

      const input = within(autocomplete).getByRole('textbox')

      autocomplete.click()
      autocomplete.focus()

      fireEvent.change(input, { target: { value: 'Texas' } })

      // to be sure, or do `findAllByRole` which is also async
      await act(async () => {
        await new Promise((resolve) => setTimeout(resolve, 0))
      })

      fireEvent.click(screen.getAllByRole('option')[0])

      expect(screen.getByTestId('chip-value-1').textContent).toBe('Texas')

      // Confirm edit
      fireEvent.click(screen.getByTestId('confirm-edit'))

      await new Promise((resolve) => setTimeout(resolve, 100))

      // checking the value inside store
      expect(store.getState().filter.filters[0].filter).toEqual({
        property: 'State',
        operator: '==',
        value: 'Ohio'
      })
    })
  })

  afterEach(() => {
    store.dispatch(clearAllFilters())
  })

  afterAll(() => {
    clear()
    jest.resetModules()
  })
})
