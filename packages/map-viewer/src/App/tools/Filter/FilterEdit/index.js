import React, { useState, memo, useEffect } from 'react'
import jmespath from 'jmespath'
import { View } from './view'
import {
  addFilter,
  addFilters,
  removeFilters,
  updateFilter
} from 'state/filter/filterSlice'
import { useDispatch, useSelector } from 'react-redux'
import { get } from 'idb-keyval'
import Chance from 'chance'
import { sortObjectByKey } from 'utils/functions'
import { dataSet, filterSchema } from 'state/meta/metaSlice'
import _ from 'lodash'

const chance = new Chance()

export const FilterEdit = memo((props) => {
  // Callback
  const { onClose, onFilterAdded } = props

  // Props
  const { data } = props

  const [autocompleteControlledState, setAutocompleteControlledState] =
    useState(false)

  // Range state to show numeric range value (min, max)
  const [range, setRange] = useState(null)

  // NumericValueError state, resolve to true when the input value if out of range (min, max)
  const [numericValueError, setNumericValueError] = useState('')

  // Features state, store features from geojson data so that rage could be query from when numeric property is selected
  const [features, setFeatures] = useState([])

  // Feature schema, contain list of properties and it data types
  const [schema, setSchema] = useState(null)

  // AutoCompleteOptions state, contain options for autocomplete to be used as value when property type as string is selected
  const [autoCompleteOptions, setAutoCompleteOptions] = useState(null)

  const filterList = useSelector(filterSchema)
  const dataSetId = useSelector(dataSet)

  useEffect(() => {
    const orderedSchema = sortObjectByKey(filterList)
    setSchema(orderedSchema)
  }, [filterList])

  useEffect(() => {
    const fetchData = async () => {
      const geojson = await get(dataSetId)
      setFeatures(geojson.features)
    }
    dataSetId && fetchData()
  }, [dataSetId])

  // Handling filter editing
  useEffect(() => {
    // Init the editing once data, schema and features are available
    if (data && schema && features.length) editFilter()
  }, [data, schema, features])

  // filter state, contains the selected property, operator and value
  const [filter, setFilter] = useState({
    property: '',
    operator: '',
    value: ''
  })

  // isStringOperator state, resolve to true when property of string type is selected
  const [isStringOperator, setIsStringOperator] = useState(false)

  // isNumericOperator state, resolve to true when property of numeric type is selected
  const [isNumericOperator, setIsNumericOperator] = useState(false)

  // filterError state, resolve to true when filter is invalid and confirm button is being clicked
  const [filterError, setFilterError] = useState('')
  const dispatch = useDispatch()

  /**
   * Edit filter
   * data : is an array of filter object {property, operator, value}
   * if property is a numeric type, data is an array of single filter object
   * else data is an array of filter objects, where those object will have the same 'property' and 'operator'
   * to update filter state, we need to convert array of filter objects into a new filter object where on the 'value'
   * could be an array.
   */
  const editFilter = () => {
    let tmp = { ...filter }
    // Covert from array of filter objects into single filter object
    tmp['property'] = data[0].filter.property
    tmp['operator'] = data[0].filter.operator
    tmp['value'] = data.map((filter) => filter.filter.value)
    const key = tmp.property
    const type = schema[key].type
    //Updating the filter state
    setFilter(tmp)
    setAutocompleteControlledState(true)

    //Show the operator as numeric or string base on the type of 'property'
    if (type === 'string') showStringOperator(key)
    else showNumericOperator(key)
  }

  /**
   * Create an array of filters of properties of type string
   * @param {filter} filter is an object of {property, operator, value: [string]}}
   */
  const createMultipleFilters = (filter) => {
    const payloads = []
    filter.value.map((value) => {
      payloads.push({
        id: chance.guid(),
        filter: {
          property: filter.property,
          operator: filter.operator,
          value: value
        }
      })
    })
    dispatch(addFilters(payloads))
  }

  /**
   * Validate a filter,
   * resolve to false if .property || .operator || .value is invalid
   * else resolve to true
   */
  const validateFilter = () => {
    if (!filter.property) {
      setFilterError('Please select a property')
      return false
    }
    if (!filter.operator) {
      setFilterError('Please select an operator')
      return false
    }
    if (!filter.value) {
      setFilterError('Please fill in a value')
      return false
    }
    setFilterError('')
    return true
  }

  const handleUpdateFilter = async () => {
    if (Array.isArray(filter.value)) {
      // Remove the selected filters
      await dispatch(removeFilters(data))
      // Recreate the filters
      createMultipleFilters(filter)
    } else {
      // else create a filter
      const payload = {
        id: data[0].id,
        filter
      }
      dispatch(updateFilter(payload))
    }

    // Trigger a onFilterAdded Callback
    onFilterAdded()
  }

  const handleCreateFilter = () => {
    // Validate filter
    if (!validateFilter()) return

    // If filter's value is an array of string, then create createMultipleFilters by splitting the array of strings
    if (Array.isArray(filter.value)) createMultipleFilters(filter)
    else {
      // else create a filter
      const payload = {
        id: chance.guid(),
        filter
      }
      // Trigger a redux action 'addFilter'
      dispatch(addFilter(payload))
    }

    // Trigger a onFilterAdded Callback
    onFilterAdded()
  }

  /**
   * Handle confirm button clicked.
   */
  const handleConfirm = () => {
    if (data) handleUpdateFilter()
    else handleCreateFilter()
  }

  /**
   * Will show either numeric operator or string operator according to type of property selected
   * @param {type} : type of property
   * @param {key} : property
   */
  const handlePropertySelected = (type, key) => {
    if (type === 'string') showStringOperator(key)
    else showNumericOperator(key)
    updateFilterState('property', `${key}`)
  }

  const handleOperatorSelected = (operator) => {
    updateFilterState('operator', operator)
  }

  const handleValueChanged = (value) => {
    SetFilterValue(value)
  }

  const SetFilterValue = (value) => {
    updateFilterState('value', value)
  }

  const handleNumericValueChanged = async (value) => {
    SetFilterValue(value)
    setNumericValueError('')
    if (value > range.max || value < range.min)
      setNumericValueError('Value out of range')
  }

  /**
   * Update filter state
   */
  const updateFilterState = (key, value) => {
    const tmp = { ...filter }
    tmp[key] = value
    setFilter(tmp)
  }

  const getMinMaxOfSelectedNumericProperty = (key) => {
    const min = jmespath.search(features, `min_by(@, &properties."${key}")`)
      .properties[key]

    const max = jmespath.search(features, `max_by(@, &properties."${key}")`)
      .properties[key]
    return { min, max }
  }

  const showRange = (key) => {
    const { min, max } = getMinMaxOfSelectedNumericProperty(key)
    setRange({ min, max })
  }

  const showStringOperator = (key) => {
    setAutoCompleteOptions([
      ...new Set(jmespath.search(features, '[*].properties."' + key + '"'))
    ])
    setIsStringOperator(true)
    setIsNumericOperator(false)
  }

  const showNumericOperator = (key) => {
    showRange(key)
    setIsStringOperator(false)
    setIsNumericOperator(true)
  }

  return (
    <>
      {schema && (
        <View
          autocompleteControlledState={autocompleteControlledState}
          filterError={filterError}
          numericValueError={numericValueError}
          autoCompleteOptions={autoCompleteOptions}
          isStringOperator={isStringOperator}
          isNumericOperator={isNumericOperator}
          onPropertySelected={handlePropertySelected}
          onOperatorSelected={handleOperatorSelected}
          onValueChanged={handleValueChanged}
          onNumericValueChanged={handleNumericValueChanged}
          onClose={onClose}
          range={range}
          schema={schema}
          onConfirm={handleConfirm}
          filter={filter}
        />
      )}
    </>
  )
})
