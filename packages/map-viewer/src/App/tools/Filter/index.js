import React, { useState, useEffect } from 'react'
import { View, VIEWS } from './view'
import { useFilters } from 'state/filter/filterSlice'

export const Filter = (props) => {
  // state
  const [view, setView] = useState('')
  const [editFilterData, setEditFilterData] = useState(null)

  // prop
  const { open } = props

  // callback
  const { onClose } = props

  // store
  const filters = useFilters()

  useEffect(() => {
    const hasSelectionSets = filters && filters.length > 0
    if (hasSelectionSets) showList()
    else showEdit()
  }, [open, filters])

  const showEdit = () => {
    setView(VIEWS.EDIT)
  }

  const showList = () => {
    setView(VIEWS.LIST)
  }

  const handleAddFilter = () => {
    setEditFilterData(null)
    showEdit()
  }

  const handleFilterAdded = () => {
    showList()
  }

  const handleClose = () => {
    onClose()
  }

  const handleFilterSelected = (filters) => {
    setEditFilterData(filters)
    showEdit()
  }

  return (
    <View
      openPanel={open}
      view={view}
      editFilterData={editFilterData}
      onAddFilter={handleAddFilter}
      onFilterAdded={handleFilterAdded}
      onFilterSelected={handleFilterSelected}
      onClose={handleClose}
    />
  )
}
