import React from 'react'
import { FilterList } from './index'
import { Provider } from 'react-redux'
import { store } from 'state/store'
import { addFilters, removeFilters } from 'state/filter/filterSlice'
import given from 'utils/testing/steps/given'
import { render, fireEvent, screen } from '@testing-library/react'
import '@testing-library/jest-dom'

describe('Filter list', () => {
  const spiedObj = {
    onFilterSelected: function () {}
  }
  const setUp = async () => {
    store.dispatch(addFilters(given.selectionSet.filters))
    render(
      <Provider store={store}>
        <FilterList
          onClose={() => {}}
          onFilterSelected={spiedObj.onFilterSelected}
        />
      </Provider>
    )
  }

  it('Apply filter to show selectionSet', async () => {
    setUp()
    await new Promise((r) => setTimeout(r, 500))

    fireEvent.click(screen.getByTestId('apply-filter'))
    expect(true).toBeTruthy()

    await new Promise((r) => setTimeout(r, 200))

    expect(store.getState().map.selectionSet.filters).toEqual(
      given.selectionSet.filters
    )
  })

  it('Select a filter', async () => {
    const spy = jest.spyOn(spiedObj, 'onFilterSelected')
    setUp()
    await new Promise((r) => setTimeout(r, 500))
    fireEvent.click(screen.getByTestId('filter-chip-Ohio'))
    expect(spy).toHaveBeenCalledWith(given.selectionSet.filters)
  })

  it('Remove filter', async () => {
    setUp()
    expect(store.getState().filter.filters).toEqual(given.selectionSet.filters)
    await new Promise((r) => setTimeout(r, 500))
    fireEvent.click(screen.getByTestId('apply-filter'))
    fireEvent.click(screen.getByTestId('CancelIcon'))

    //Wait for redux thunk to finish
    await new Promise((r) => setTimeout(r, 200))
    expect(store.getState().filter.filters).toEqual([])
    expect(store.getState().map.selectionSet.filters).toEqual([])
  })

  it('Clear all filter', async () => {
    setUp()
    await new Promise((r) => setTimeout(r, 500))

    fireEvent.click(screen.getByTestId('clear-filter'))

    await new Promise((r) => setTimeout(r, 100))

    expect(store.getState().filter.filters).toEqual([])
  })

  afterEach(() => {
    store.dispatch(removeFilters(given.selectionSet.filters))
  })

  afterAll(() => {
    jest.resetModules()
  })
})
