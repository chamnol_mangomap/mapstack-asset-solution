import React from 'react'
import Box from '@mui/material/Box'
import _ from 'lodash'
import CloseIcon from '@mui/icons-material/Close'
import { Chip, IconAddCircleOutlined, RegularButton } from 'ui-components'

function Item(props) {
  const { sx, ...other } = props
  return (
    <Box
      sx={{
        p: '0px 15px 15px 15px',
        borderRadius: 1,
        fontSize: '1rem',
        fontWeight: '700',
        ...sx
      }}
      {...other}
    />
  )
}

export const View = (props) => {
  const { data } = props
  const {
    onDelete,
    onClose,
    onAddFilter,
    onFilterSelected,
    onClearAllFilter,
    onApplyFilter
  } = props

  return (
    <Box
      style={{
        // position: 'absolute',
        top: 0,
        right: 0,
        height: '100%',
        display: 'flex',
        flexDirection: 'column',
        p: 1,
        zIndex: 3
      }}
    >
      <Box sx={{ display: 'flex', flexDirection: 'row' }}>
        <Box>
          <CloseIcon sx={{ m: '10px', cursor: 'pointer' }} onClick={onClose} />
        </Box>
        <Item
          data-testid='clear-filter'
          onClick={onClearAllFilter}
          sx={{
            flexGrow: '1',
            textAlign: 'right',
            pt: '15px',
            fontSize: '1rem',
            fontWeight: '700',
            cursor: 'pointer'
          }}
        >
          Clear All
        </Item>
      </Box>
      <Item sx={{ flexGrow: 1, textAlign: 'right' }}>
        <div>
          {data &&
            data.map(([key, value]) => {
              return (
                <div key={key}>
                  {value.map((filter, index) => {
                    const { property, operator, value } = filter.filter
                    const prettyOperator =
                      operator === '=='
                        ? 'is'
                        : operator === '!='
                        ? 'is not'
                        : operator
                    return (
                      <div
                        key={index}
                        style={{
                          margin: '0px 0px 5px 5px',
                          display: 'inline-block'
                        }}
                      >
                        <Chip
                          data-testid={`filter-chip-${value}`}
                          onClick={() => onFilterSelected(filter)}
                          onDelete={() => onDelete(filter)}
                          color='success'
                          label={`${property} ${prettyOperator} ${value}`}
                        />
                      </div>
                    )
                  })}
                </div>
              )
            })}
        </div>
        <Chip
          sx={{ marginTop: '10px' }}
          color='primary'
          label='Add Filter'
          onClick={onAddFilter}
          onDelete={() => {}}
          deleteIcon={<IconAddCircleOutlined />}
        />
      </Item>
      <Item sx={{ textAlign: 'center' }}>
        <RegularButton
          data-testid='apply-filter'
          onClick={onApplyFilter}
          round
          block
        >
          Apply
        </RegularButton>
      </Item>
    </Box>
  )
}
