import React from 'react'
import { View } from './view'
import _ from 'lodash'
import {
  filters,
  clearAllFilters,
  removeFilter
} from 'state/filter/filterSlice'
import {
  setSelectionSet,
  removeFilterFromSelectionSet
} from 'state/map/mapSlice'
import { useSelector, useDispatch } from 'react-redux'

export const FilterList = (props) => {
  const dispatch = useDispatch()

  // Callback
  const { onClose, onAddFilter, onFilterSelected } = props

  // A redux action listener, listening to 'addFilter' action
  const filtersData = useSelector(filters)

  /**
   * highlightSelectionSets, trigger redux action 'setSelectionSet'
   * map component that are listening to action 'setSelectionSet' will recieve
   * the data and highlight features on the map
   */
  const highlightSelectionSets = () => {
    dispatch(
      setSelectionSet({
        source: 'geojson-layer',
        filters: filtersData
      })
    )
    onClose()
  }

  /**
   * clearAllFilters, trigger redux action 'setSelectionSet', passing empty selectionSet
   * causing map that is listening to map 'setSelectionSet' to clear all features highlight
   */
  const handleClearAllFilter = () => {
    dispatch(clearAllFilters())
    dispatch(
      setSelectionSet({
        source: 'geojson-layer',
        filters: []
      })
    )
  }

  /**
   * handleDelete, trigger redux action 'removeFilter' causing a filter to be removed from the list
   * and trigger redux action 'removeFilterFromSelectionSet' causing map to remove a feature highlight
   */
  const handleDelete = (filter) => {
    dispatch(removeFilter(filter.id))
    dispatch(removeFilterFromSelectionSet(filter.id))
  }

  const handleClose = () => {
    highlightSelectionSets()
    onClose()
  }

  const handleFilterSelected = (selectedFilter) => {
    const groupFilters = filtersData.filter(
      (filter) =>
        filter.filter.property === selectedFilter.filter.property &&
        filter.filter.operator === selectedFilter.filter.operator
    )
    onFilterSelected(groupFilters)
  }

  const filtersDataGroupByProperty = _.sortBy(
    Object.entries(
      _.groupBy(filtersData, function (n) {
        return `${n.filter.property}`
      })
    )
  )

  return (
    <>
      <View
        onFilterSelected={handleFilterSelected}
        onDelete={handleDelete}
        onClose={handleClose}
        onAddFilter={onAddFilter}
        onClearAllFilter={handleClearAllFilter}
        onApplyFilter={highlightSelectionSets}
        data={filtersDataGroupByProperty}
      />
    </>
  )
}
