import React, { useState } from 'react'
import _ from 'lodash'
import { View } from './view'
import { Filter } from './Filter'
import { Legend } from './Legend'
import { FilterResults } from './FilterResults'
import { MapSelection } from './MapSelections'
import { useLayerStyle } from 'state/layerStyle/layerStyleSlice'
import { useFilters, useFilterResults } from 'state/filter/filterSlice'
import { useBoolean } from 'utils/hooks/useBoolean'
import { useDispatch } from 'react-redux'
import { clearFeatureSelection } from 'state/featureSelection/featureSelectionSlice'

const STATE_CLOSED = ''
const MAPS = 'maps'
const LEGEND = 'legend'
const FILTERS = 'filters'
const RESULTS = 'results'
const PANEL = {
  MAPS,
  LEGEND,
  FILTERS,
  RESULTS,
  STATE_CLOSED
}

const usePanel = () => {
  const dispatch = useDispatch()
  const [panel, setPanel] = useState(PANEL.STATE_CLOSED)

  const shouldOpenPanel = (configs) => {
    if (_.has(configs, 'when')) {
      return configs.type === panel && configs.when
    }

    return configs.type === panel
  }

  const handleClosePanel = () => {
    setPanel(STATE_CLOSED)
    dispatch(clearFeatureSelection())
  }

  return {
    panel,
    changePanel: setPanel,
    shouldOpenPanel,
    handleClosePanel
  }
}

export function Tools() {
  // state
  const {
    value: isTooltipOpen,
    setTrue: handleTooltipOpen,
    setFalse: handleTooltipClose
  } = useBoolean(false)

  // store
  const layerStyle = useLayerStyle()
  const filters = useFilters()
  const filterResults = useFilterResults()

  // hooks
  const { panel, changePanel, shouldOpenPanel, handleClosePanel } = usePanel()

  // handlers
  const handleOnChange = (value) => {
    if (_.isEqual(value, PANEL.RESULTS) && _.size(filters) < 1) {
      handleTooltipOpen()
    } else {
      changePanel(value)
      handleTooltipClose()
    }
  }

  const disabledLegend = _.isEmpty(layerStyle) || layerStyle.type === 'default'
  const classBreaks =
    _.isEmpty(layerStyle) &&
    layerStyle.type !== 'default' &&
    layerStyle.intervals

  return (
    <>
      <MapSelection
        open={shouldOpenPanel({
          type: PANEL.MAPS
        })}
        onClose={handleClosePanel}
      />
      {/* SUGGEST FEATURE: for consistent user experience, legend drawer shouldn't render based on results BottomTapAction, and should show tooltip saying something like `No map selection, first apply map select option. ` */}
      <Legend
        open={shouldOpenPanel({
          type: PANEL.LEGEND
        })}
        onClose={handleClosePanel}
      />
      <Filter
        open={shouldOpenPanel({
          type: PANEL.FILTERS
        })}
        onClose={handleClosePanel}
      />
      <FilterResults
        open={shouldOpenPanel({
          type: PANEL.RESULTS,
          when: !_.isEmpty(filterResults)
        })}
        onClose={handleClosePanel}
      />
      <View
        value={panel}
        disabledLegend={disabledLegend}
        classBreaks={classBreaks}
        filterResults={filterResults}
        filters={filters}
        onChange={handleOnChange}
        isTooltipOpen={isTooltipOpen}
        onTooltipClose={handleTooltipClose}
      />
    </>
  )
}
