import React from 'react'
import { utils } from 'utils'
import CloseIcon from '@mui/icons-material/Close'
import { Drawer, Box, Typography } from 'ui-components'
import { QUANTITY_SELECTION_MAP_COLORS } from 'assets/color/Colors'

function Item(props) {
  const { sx, ...rest } = props

  return (
    <Box
      sx={{
        p: '0px 15px 15px 15px',
        borderRadius: 1,
        fontSize: '1rem',
        fontWeight: '700',
        ...sx
      }}
      {...rest}
    />
  )
}

export const View = (props) => {
  // Props
  const { open, data, title } = props

  // Callback
  const { onClose } = props

  return (
    <>
      <Drawer open={open}>
        <Box
          style={{
            top: 0,
            right: 0,
            height: '100%',
            display: 'flex',
            flexDirection: 'column',
            p: 1,
            zIndex: 3
          }}
        >
          <Box sx={{ display: 'flex', flexDirection: 'row' }}>
            <Box sx={{ position: 'absolute' }}>
              <CloseIcon
                sx={{ m: '10px', cursor: 'pointer' }}
                onClick={onClose}
              />
            </Box>
            <Item
              data-testid='clear-filter'
              sx={{
                flexGrow: '1',
                textAlign: 'center',
                pt: '15px',
                fontSize: '1rem',
                fontWeight: '700'
              }}
            >
              {title}
            </Item>
          </Box>
          <Box
            sx={{
              paddingLeft: '20px',
              width: '100%',
              flex: 1,
              display: 'flex',
              flexDirection: 'column',
              overflowY: 'hidden'
            }}
          >
            {data &&
              data.map((ramp, i) => {
                if (i === data.length - 1) return null
                return (
                  <Box
                    key={`ramp-${i}`}
                    sx={{
                      display: 'flex',
                      alignItems: 'center',
                      marginBottom: '10px'
                    }}
                  >
                    <Box>
                      <div
                        style={{
                          width: '30px',
                          height: '30px',
                          borderRadius: '3px',
                          marginRight: '10px',
                          background: `${QUANTITY_SELECTION_MAP_COLORS[i]}`
                        }}
                      />
                    </Box>
                    <Box>
                      <Typography style={{ margin: 'auto' }}>
                        {utils.prettifyNumber(ramp.toFixed(2))} to{' '}
                        {utils.prettifyNumber(data[i + 1].toFixed(2))}
                      </Typography>
                    </Box>
                  </Box>
                )
              })}
          </Box>
        </Box>
      </Drawer>
    </>
  )
}
