import React, { useEffect, useState } from 'react'
import { layerStyle } from 'state/layerStyle/layerStyleSlice'
import { useSelector } from 'react-redux'
import { View } from './view'

export function Legend(props) {
  const layerStyleData = useSelector(layerStyle)
  const [intervals, setIntervals] = useState(null)
  const { open, onClose } = props

  useEffect(() => {
    if (layerStyleData && layerStyleData.type === 'quantity')
      setIntervals(layerStyleData.intervals)
  }, [layerStyleData])

  return (
    <View
      title={layerStyleData.value}
      data={intervals}
      open={open}
      onClose={onClose}
    />
  )
}
