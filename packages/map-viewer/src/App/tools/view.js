import React from 'react'
import {
  BottomNavigation,
  Badge,
  IconList,
  IconMap,
  IconFilterAlt,
  IconTableView,
  Tooltip
} from 'ui-components'
import { utils } from 'utils'

export function View(props) {
  // props
  const {
    value,
    filters,
    filterResults,
    isTooltipOpen,
    disabledLegend,
    classBreaks
  } = props

  // callbacks
  const { onChange, onTooltipClose } = props

  const actions = [
    { value: 'maps', icon: <IconMap />, label: 'Maps' },
    {
      value: 'legend',
      icon: classBreaks ? (
        <Badge color='primary' badgeContent={classBreaks}>
          <IconList />
        </Badge>
      ) : (
        <IconList />
      ),
      label: 'Legend',
      disabled: disabledLegend
    },
    {
      value: 'filters',
      icon: filters.length ? (
        <Badge badgeContent={utils.thousandFormatter(filters.length)}>
          <IconFilterAlt />
        </Badge>
      ) : (
        <IconFilterAlt />
      ),
      label: 'Filters'
    },
    {
      value: 'results',
      icon: filterResults.length ? (
        <Badge badgeContent={filterResults.length}>
          <IconTableView />
        </Badge>
      ) : (
        <IconTableView />
      ),
      label: 'Results',
      id: 'filter-result-button',
      showLabel: true,
      containerComponent: () => (
        <Tooltip
          isClickAwayListener
          open={isTooltipOpen}
          onClose={onTooltipClose}
          title='No results, first apply a filter!'
        >
          {filters.length > 0 && filterResults.length > 0 ? (
            <Badge badgeContent={filterResults.length}>
              <IconTableView />
            </Badge>
          ) : (
            <IconTableView />
          )}
        </Tooltip>
      )
    }
  ]

  return <BottomNavigation {...{ value, actions, onChange }} />
}
