import React from 'react'
import PropTypes from 'prop-types'

import { Drawer, Tabs, Box, SideBarHeader } from 'ui-components'
import { TableResult } from './Table'
import { Total } from './Total'
import { Avg } from './Avg'
import { Count } from './Count'

function TabPanel(props) {
  const { sx, children, value, index, ...rest } = props

  return (
    <>
      {value === index ? (
        <Box
          role='tabpanel'
          hidden={value !== index}
          // id={`simple-tabpanel-${index}`}
          aria-labelledby={`simple-tab-${index}`}
          sx={{
            ...sx,
            p: 3
          }}
          {...rest}
        >
          {children}
        </Box>
      ) : null}
    </>
  )
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.number.isRequired,
  value: PropTypes.number.isRequired
}

export const View = (props) => {
  // Props
  const { open, tab } = props

  // Callback
  const { onClose, handleChangeTab } = props

  const tabItems = ['Totals', 'Avg.', 'Count', 'Table'].map((label, index) => ({
    label,
    id: `simple-tabpanel-${index}`,
    'data-testid': `tab-${index}`,
    'aria-controls': `simple-tabpanel-${index}`
  }))

  return (
    <Drawer open={open} onClose={onClose}>
      <SideBarHeader
        onClose={onClose}
        title={`${props.results.length} Results`}
      />
      <Box
        sx={{
          width: '100%',
          flex: 1,
          display: 'flex',
          flexDirection: 'column',
          overflowY: 'hidden'
        }}
      >
        <Box
          sx={{
            width: '100%',
            flex: 1,
            display: 'flex',
            flexDirection: 'column',
            overflowY: 'hidden'
          }}
        >
          <Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
            <Tabs value={tab} onChange={handleChangeTab} items={tabItems} />
          </Box>
          <Box sx={{ flex: 1, overflowY: 'auto' }}>
            <TabPanel value={tab} index={0}>
              <Total />
            </TabPanel>
            <TabPanel value={tab} index={1} sx={{ height: '100%' }}>
              <Avg />
            </TabPanel>
            <TabPanel value={tab} index={2}>
              <Count />
            </TabPanel>
            <TabPanel value={tab} index={3} sx={{ height: '100%' }}>
              <TableResult />
            </TabPanel>
          </Box>
        </Box>
      </Box>
    </Drawer>
  )
}
