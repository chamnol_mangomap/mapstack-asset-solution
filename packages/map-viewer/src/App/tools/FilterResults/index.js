import React, { useState, useEffect } from 'react'
import { get, set } from 'idb-keyval'

import { View } from './view'
import { useMetaData } from 'state/meta/metaSlice'
import { useFilterResults } from 'state/filter/filterSlice'

export function FilterResults(props) {
  // state
  const [tab, setTab] = useState(0)

  // props
  const { open } = props

  // callback
  const { onClose } = props

  // store
  const filterResults = useFilterResults()
  const metaData = useMetaData()

  useEffect(() => {
    ;(async function getSavedTab() {
      const savedTab = await get('last-used-filters-tab')
      savedTab && setTab(savedTab)
    })()
  }, [])

  const handleChangeTab = async (_, newValue) => {
    await set('last-used-filters-tab', newValue)
    setTab(newValue)
  }

  return (
    <View
      results={filterResults}
      metaDataData={metaData}
      tab={tab}
      open={open}
      onClose={onClose}
      handleChangeTab={handleChangeTab}
    />
  )
}
