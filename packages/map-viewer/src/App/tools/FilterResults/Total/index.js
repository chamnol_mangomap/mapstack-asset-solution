import React from 'react'
import { View } from './view'
import { useMetaData } from 'state/meta/metaSlice'
import { useFilterResults } from 'state/filter/filterSlice'
import { chain } from 'lodash'

export const useTotal = (filterResults = [], metaData = {}) => {
  const summaries = chain(metaData.schema)
    .pickBy({
      summary: true
    })
    .keys()
    .value()

  const totals = chain(summaries)
    .map((item) => {
      return {
        key: item,
        value: filterResults.reduce(
          (previous, current) => previous + current.properties[`${item}`],
          0
        )
      }
    })
    .value()

  return {
    totals
  }
}

export function Total() {
  const metaData = useMetaData()
  const filterResults = useFilterResults()

  // REFACTOR metaData -> useSummaries
  const { totals } = useTotal(filterResults, metaData)

  return <View totals={totals} />
}
