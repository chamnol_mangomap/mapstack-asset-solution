import React from 'react'
import { Typography, Box } from 'ui-components'
import { utils } from 'utils'

export function View(props) {
  const { totals } = props

  return (
    <Box sx={{ width: '100%', maxWidth: 500 }}>
      {totals.map((total, key) => (
        <div key={key}>
          <Typography overline bold>
            {utils.prettifyNumber(total.key)}
          </Typography>
          <Typography>{utils.prettifyNumber(total.value)}</Typography>
        </div>
      ))}
    </Box>
  )
}
