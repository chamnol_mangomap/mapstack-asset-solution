import React from 'react'
import { mount } from 'enzyme'
import { Provider } from 'react-redux'
import { store } from 'state/store'
import { Total, useTotal } from './index'
import { View } from './view'
import given from 'utils/testing/steps/given'
import { setFilterResults } from 'state/filter/filterSlice'
import { setMetaData } from 'state/meta/metaSlice'

describe('Total View', () => {
  let totalComponent
  const setUp = () => {
    store.dispatch(setFilterResults(given.filterResults))
    store.dispatch(setMetaData(given.metaData))
    totalComponent = mount(
      <Provider store={store}>
        <Total />
      </Provider>
    )
  }

  it('should show the sum of all the values in the results', () => {
    setUp()
    const { totals } = useTotal(given.filterResults, given.metaData)
    const { totals: actualTotals } = totalComponent.find(View).props()
    expect(actualTotals).toEqual(totals)
  })
})
