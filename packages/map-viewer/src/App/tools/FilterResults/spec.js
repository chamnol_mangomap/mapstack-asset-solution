import React from 'react'
import { mount } from 'enzyme'
import { Provider } from 'react-redux'
import { act } from 'react-dom/test-utils'
import '@testing-library/jest-dom'
import { set } from 'idb-keyval'

import { store } from 'state/store'
import { FilterResults } from './index'
import { View } from './view'
import given from 'utils/testing/steps/given'
import { setFilterResults } from 'state/filter/filterSlice'
import { setMetaData } from 'state/meta/metaSlice'

describe('Filter Results', () => {
  let filterResultsComponent
  const setup = async () => {
    filterResultsComponent = mount(
      <Provider store={store}>
        <FilterResults open />
      </Provider>
    )
  }

  it('should show number of results', async () => {
    await act(async () => {
      // setup state for FilterResult component
      store.dispatch(setFilterResults(given.filterResults))
      store.dispatch(setMetaData(given.metaData))

      // mounting
      await setup()

      // update the component
      filterResultsComponent.update()

      // check if the prop is passed to the view
      const viewProps = filterResultsComponent.find(View).props()
      expect(viewProps.results).toEqual(given.filterResults)
      expect(viewProps.metaDataData).toEqual(given.metaData)
    })
  })

  // TODO: pass this test
  it('should remember last used tab', async () => {
    await act(async () => {
      const index = 2
      await set('last-used-filters-tab', 2)

      store.dispatch(setFilterResults(given.filterResults))
      store.dispatch(setMetaData(given.metaData))
      await setup()

      await new Promise((r) => setTimeout(r, 100))

      filterResultsComponent
        .find(`#simple-tabpanel-${index}`)
        .hostNodes()
        .simulate('click')

      await new Promise((r) => setTimeout(r, 200))
      expect(filterResultsComponent.find(View).props().tab).toEqual(index)
    })
  })

  afterEach(() => {
    filterResultsComponent.unmount()
  })
})
