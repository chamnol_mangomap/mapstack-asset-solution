import React from 'react'
import { DataGrid, Paper } from 'ui-components'

const defaultColumnConfig = {
  width: 150,
  editable: false,
  sortable: false,
  resizable: true
}
export function View(props) {
  const { columns: _columns, rows } = props

  const columns = _columns.map((column) => ({
    ...column,
    ...defaultColumnConfig
  }))

  return (
    <Paper
      sx={{
        overflow: 'hidden',
        width: '100%',
        height: '100%'
      }}
    >
      <DataGrid
        data-testid='test-average-datagrid'
        rows={rows}
        columns={columns}
        hideFooter
        disableColumnMenu
        disableVirtualization
      />
    </Paper>
  )
}
