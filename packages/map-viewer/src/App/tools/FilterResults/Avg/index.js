import React from 'react'
import { chain, map, ceil } from 'lodash'
import { View } from './view'
import { useMetaData } from 'state/meta/metaSlice'
import { useFilterResults } from 'state/filter/filterSlice'
import { hasTextIncluded } from 'utils/helpers/strings'
import { min, max, mean, median } from 'utils/helpers/maths'

const columns = [
  { field: 'name', headerName: '' },
  { field: 'mean', headerName: 'Mean' },
  { field: 'median', headerName: 'Median' },
  { field: 'min', headerName: 'Min' },
  { field: 'max', headerName: 'Max' }
]

export function Avg() {
  const metaData = useMetaData()
  const filterResults = useFilterResults()

  const _keys = chain(metaData.schema)
    .pickBy({ type: 'number' })
    .entries()
    .map(([key]) => key)
    .value()

  const rows = map(_keys, (key, index) => {
    const results = map(filterResults, `properties.[${key}]`)
    const isValid =
      !hasTextIncluded(key, 'Median') && !hasTextIncluded(key, 'Rate')

    return {
      id: index,
      name: key,
      mean: isValid ? ceil(mean(results), 2) : 'n/a',
      median: isValid ? ceil(median(results), 2) : 'n/a ',
      min: min(results),
      max: max(results)
    }
  })

  return <View columns={columns} rows={rows} />
}

/**
 * Reference:
 * [Lodash Usage]: https://stackoverflow.com/questions/28354725/lodash-get-an-array-of-values-from-an-array-of-object-properties
 */
