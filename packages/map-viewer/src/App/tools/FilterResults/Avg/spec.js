import React from 'react'
import { mount } from 'enzyme'
import { Provider } from 'react-redux'
import { store } from 'state/store'
import { setFilterResults } from 'state/filter/filterSlice'
import given from 'utils/testing/steps/given'
import { setMetaData } from 'state/meta/metaSlice'
import { Avg } from './index'

describe('Average View', () => {
  let averageComponent
  const setup = () => {
    store.dispatch(setFilterResults(given.filterResults))
    store.dispatch(setMetaData(given.metaData))

    averageComponent = mount(
      <Provider store={store}>
        <Avg />
      </Provider>
    )
  }

  beforeEach(() => {
    setup()
  })

  afterEach(() => {
    jest.resetModules()
    jest.clearAllMocks()
  })

  it('should display DataGrid component inside the view component', () => {
    expect(
      averageComponent.find("[data-testid='test-average-datagrid']").exists()
    ).toBeTruthy()
  })

  test('it should include the column header', () => {
    const columnHeaderComponents = averageComponent.find(
      '.MuiDataGrid-columnHeaders'
    )
    expect(columnHeaderComponents.exists()).toBeTruthy()
  })

  test('it should have length of 5 ["", mean, median, min, max]', () => {
    const columnHeaderComponentArray = averageComponent.find(
      '.MuiDataGrid-columnHeader'
    )
    expect(columnHeaderComponentArray).toHaveLength(5)
  })

  describe('Should display required column headers', () => {
    test("1st row 1st column to be ''", () => {
      const column = averageComponent
        .find(
          '.MuiDataGrid-columnHeader[data-field="name"] .MuiDataGrid-columnHeaderTitle'
        )
        .first()
        .text()
      expect(column).toBe('')
    })

    test("1st row 2nd column to be 'Mean'", () => {
      const column = averageComponent
        .find(
          '.MuiDataGrid-columnHeader[data-field="mean"] .MuiDataGrid-columnHeaderTitle'
        )
        .first()
        .text()

      expect(column).toBe('Mean')
    })

    test("1st row 3rd column to be 'Median'", () => {
      const column = averageComponent
        .find(
          '.MuiDataGrid-columnHeader[data-field="median"] .MuiDataGrid-columnHeaderTitle'
        )
        .first()
        .text()

      expect(column).toBe('Median')
    })

    test("1st row 4th column to be 'Min'", () => {
      const column = averageComponent
        .find(
          '.MuiDataGrid-columnHeader[data-field="min"] .MuiDataGrid-columnHeaderTitle'
        )
        .first()
        .text()

      expect(column).toBe('Min')
    })

    test("1st row 5th column to be 'Max'", () => {
      const column = averageComponent
        .find(
          '.MuiDataGrid-columnHeader[data-field="max"] .MuiDataGrid-columnHeaderTitle'
        )
        .first()
        .text()

      expect(column).toBe('Max')
    })
  })
})

/**
 * Reference:
 * [Testing Issue with Datagrid Header]: https://mui.com/components/data-grid/virtualization/
 */
