import React from 'react'
import { chain } from 'lodash'
import { View } from './view'
import { useMetaData } from 'state/meta/metaSlice'
import { useFilterResults } from 'state/filter/filterSlice'

export const useCount = (array = [], metaData = {}) => {
  const categories = chain(metaData.schema)
    .pickBy({ category: true })
    .keys()
    .value()

  const countBy = (key) => {
    return chain(array)
      .map('properties')
      .countBy(key)
      .mapValues((value, key) => ({ key, value }))
      .orderBy(['value'], ['desc'])
      .value()
  }

  const countsByGroup = categories.map((group) => ({
    group,
    counts: countBy(group)
  }))

  return {
    countsByGroup
  }
}

export function Count() {
  const metaData = useMetaData()
  const filterResults = useFilterResults()

  // REFACTOR metaData -> useCategories
  const { countsByGroup } = useCount(filterResults, metaData)

  return <View countsByGroup={countsByGroup} />
}
