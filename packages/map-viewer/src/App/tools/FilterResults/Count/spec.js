import React from 'react'
import { mount } from 'enzyme'
import { Provider } from 'react-redux'
import _ from 'lodash'

import { store } from 'state/store'
import { Count, useCount } from './index'
import { View } from './view'
import given from 'utils/testing/steps/given'
import { setFilterResults } from 'state/filter/filterSlice'
import { setMetaData } from 'state/meta/metaSlice'

describe('Count View', () => {
  let countComponent
  const setup = () => {
    store.dispatch(setFilterResults(given.filterResults))
    store.dispatch(setMetaData(given.metaData))
    countComponent = mount(
      <Provider store={store}>
        <Count />
      </Provider>
    )
  }

  it('should passed the correct props', () => {
    // setup store
    store.dispatch(setFilterResults(given.filterResults))
    store.dispatch(setMetaData(given.metaData))

    // mounting

    setup()

    // counting the properties by given key
    const { countsByGroup } = useCount(given.filterResults, given.metaData)

    const viewComponentProps = countComponent.find(View).props()

    expect(viewComponentProps.countsByGroup).toEqual(countsByGroup)
  })
})
