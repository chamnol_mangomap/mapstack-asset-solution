import React from 'react'
import PropTypes from 'prop-types'
import { Typography, Box } from 'ui-components'

export function View(props) {
  const { countsByGroup } = props

  return (
    <Box sx={{ width: '100%', maxWidth: 500 }}>
      {countsByGroup.map(({ group, counts }, key) => (
        <div key={key}>
          <Typography overline bold>
            {group}
          </Typography>
          {counts.map((pair, key) => (
            <Typography key={key}>{`${pair.key}: ${pair.value}`}</Typography>
          ))}
        </div>
      ))}
    </Box>
  )
}

View.propTypes = {
  /**
   *  List of data that have been filtered and counted by categories
   */
  countsByGroup: PropTypes.array
}
