import React from 'react'
import { FilterTable } from 'ui-components'

export function View(props) {
  const { columns, rows, onFeatureClick } = props

  const handleRowClick = (rowData) => onFeatureClick(rowData.row)

  return (
    <FilterTable rows={rows} columns={columns} onRowClick={handleRowClick} />
  )
}
