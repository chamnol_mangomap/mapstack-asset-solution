import React from 'react'
import { mount } from 'enzyme'
import { Provider } from 'react-redux'
import turf from '@turf/bbox'

import { store } from 'state/store'
import { TableResult, useTableResults } from './index'
import { View } from './view'
import given from 'utils/testing/steps/given'
import { setFilterResults } from 'state/filter/filterSlice'

describe('Table Result', () => {
  let tableComponent
  const setUp = () => {
    store.dispatch(setFilterResults(given.filterResults))
    tableComponent = mount(
      <Provider store={store}>
        <TableResult />
      </Provider>
    )
  }
  it('Should show a table of all filter result', () => {
    setUp()

    const { rows, columns } = useTableResults(given.filterResults)

    const { rows: actualRows, columns: actualColumns } = tableComponent
      .find(View)
      .props()

    expect(actualRows).toEqual(rows)
    expect(actualColumns).toEqual(columns)
  })

  it('Should pan to the location and best fit zoom when the feature is clicked', () => {
    setUp()

    const firstData = given.filterResults[2]
    const bounds = turf(firstData.geometry)

    const firstRowId = `[data-id='feature-row-${firstData.id}']`

    tableComponent.find(firstRowId).hostNodes().simulate('click')

    const { map } = store.getState()

    expect(map.fitBounds).toEqual(expect.arrayContaining(bounds))
  })
})
