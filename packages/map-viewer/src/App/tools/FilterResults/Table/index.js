import React from 'react'
import { chain, isEmpty } from 'lodash'
import { useDispatch } from 'react-redux'
import turf from '@turf/bbox'
import { isMobile } from 'react-device-detect'
import { View } from './view'
import { useFilterResults } from 'state/filter/filterSlice'
import { setFitBounds } from 'state/map/mapSlice'
import { setPanel } from 'state/control/controlSlice'
import { setFeatureSelection } from 'state/featureSelection/featureSelectionSlice'

const defaultColumnConfig = {
  width: 150,
  editable: false,
  sortable: true,
  resizable: true
}

const CountyType = {
  County: {
    type: 'string'
  },
  'County id': {
    type: 'string'
  },
  Population: {
    type: 'number'
  },
  'Median age': {
    type: 'number'
  },
  Households: {
    type: 'number'
  },
  'Median income': {
    type: 'number'
  },
  'Unemployment rate': {
    type: 'number'
  },
  'Median household value': {
    type: 'number'
  },
  'State code': {
    type: 'string'
  },
  State: {
    type: 'string'
  },
  uuid: {
    type: 'string'
  }
}

export const useTableResults = (filterResults = []) => {
  const prepareColumns = (type) => {
    return chain(type)
      .entries()
      .sort((_, [__, value]) => (value.type === 'number' ? -1 : 1)) // put columns of type 'number' at the end of table
      .map(([key, value]) => {
        let columnConfig = {}
        if (key === 'uuid') columnConfig = { hide: true }
        if (value.type === 'number')
          columnConfig = { ...columnConfig, align: 'right' }

        return {
          ...defaultColumnConfig,
          ...columnConfig,
          field: key,
          headerName: key
        }
      })
      .value()
  }

  const prepareRows = (array = []) => {
    return chain(array)
      .map((item) => ({
        ...item,
        ...item.properties,
        htmlId: `feature-row-${item.id}`
      }))
      .value()
  }

  return {
    columns: !isEmpty(filterResults) ? prepareColumns(CountyType) : [],
    rows: prepareRows(filterResults)
  }
}

export const TableResult = () => {
  // store
  const dispatch = useDispatch()
  const filterResults = useFilterResults()

  // hooks
  const { rows, columns } = useTableResults(filterResults)

  const handleFeatureClick = (value) => {
    dispatch(setFeatureSelection(value))
    const bounds = turf(value.geometry)
    dispatch(setFitBounds([bounds[2], bounds[3], bounds[0], bounds[1]]))
    if (isMobile) dispatch(setPanel(''))
  }

  return (
    <View rows={rows} columns={columns} onFeatureClick={handleFeatureClick} />
  )
}
