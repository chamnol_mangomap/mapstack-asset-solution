import React from 'react'
import { mount } from 'enzyme'
import { Provider } from 'react-redux'
import { Tools } from './index'
import { View } from './view'
import { FilterResults } from './FilterResults'
import { store } from 'state/store'
import { setFilterResults, addFilters } from 'state/filter/filterSlice'
import { setMetaData } from 'state/meta/metaSlice'
import given from 'utils/testing/steps/given'

jest.mock('./MapSelections', () => ({
  MapSelection: () => {
    return <div></div>
  }
}))

describe('Filter Results', () => {
  let toolsComponent
  const setUp = () => {
    toolsComponent = mount(
      <Provider store={store}>
        <Tools />
      </Provider>
    )
  }

  beforeEach(() => {
    setUp()
  })

  it('should passed the correct prop to view', () => {
    // dispatch setFilterResults so we can assert the value of filterResults
    store.dispatch(setFilterResults(given.filterResults))

    // Called update, so the child component of the toolsComponent is rerender
    toolsComponent.update()

    // Getting props of the of child component
    const viewProps = toolsComponent.find(View).props()

    expect(viewProps.filterResults).toEqual(given.filterResults)
  })

  // FIXED:
  it('should show the result panel when the result button is pressed', () => {
    // dispatch setFilterResults so we can assert the value of filterResults
    store.dispatch(addFilters(given.filters))
    store.dispatch(setFilterResults(given.filterResults))
    store.dispatch(setMetaData(given.metaData))

    toolsComponent.find('#filter-result-button').hostNodes().simulate('click')

    // After 'filter-result-button' is pressed, result component should have open set to true
    const filterResultsProps = toolsComponent.find(FilterResults).props()
    expect(filterResultsProps.open).toBeTruthy()
  })

  afterEach(() => {
    jest.resetModules()
  })
})
