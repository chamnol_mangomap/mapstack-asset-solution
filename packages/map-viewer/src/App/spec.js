import React from 'react'
import { mount } from 'enzyme'
import { App } from './index'
import { act } from 'react-dom/test-utils'
import { get, clear } from 'idb-keyval'
import { Provider } from 'react-redux'
import { store } from 'state/store'
import given from 'utils/testing/steps/given'
import axios from 'axios'
import { Tools } from '../App/tools'
jest.mock('axios')
jest.mock('../App/tools')

describe('App', () => {
  const setUp = async () => {
    Tools.mockImplementation(() => {
      return <div />
    })
    axios.get.mockResolvedValueOnce({ data: given.metaData })
    axios.get.mockResolvedValueOnce({ data: given.geojson })
    await act(async () =>
      mount(
        <Provider store={store}>
          <App viewport={given.viewport} />
        </Provider>
      )
    )
  }

  const updateDataAndRerender = async () => {
    axios.get.mockResolvedValueOnce({ data: given.newVersionMetaData })
    axios.get.mockResolvedValueOnce({ data: given.geojson })
    await act(async () =>
      mount(
        <Provider store={store}>
          <App viewport={given.viewport} />
        </Provider>
      )
    )
  }

  beforeAll(() => {
    setUp()
  })

  it('Fetch geojson', async () => {
    await new Promise((r) => setTimeout(r, 100))
    expect(axios.get).toHaveBeenCalledWith(
      expect.stringContaining('/data/meta.json')
    )
    expect(axios.get).toHaveBeenCalledWith(
      '/data/757b64e5-bb80-4316-baeb-334e1684344f.geojson'
    )
  })

  it('Store geojson into local storage', async () => {
    const data = await get(given.metaData.dataSet)
    expect(data).toEqual(given.geojson)
    const metaData = await get(`${given.metaData.dataSet}:metaData`)
    expect(metaData.version).toEqual(given.metaData.version)
  })

  it('Update geojson version and refetch geojson', async () => {
    updateDataAndRerender()
    await new Promise((r) => setTimeout(r, 500))
    const metaDataFromStore = await get(
      `${given.newVersionMetaData.dataSet}:metaData`
    )
    expect(metaDataFromStore.version).toEqual(given.newVersionMetaData.version)
  })

  afterAll(() => {
    clear()
    jest.resetModules()
  })
})
