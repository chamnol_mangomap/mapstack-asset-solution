import axios from 'axios'
import React, { useEffect } from 'react'
import { Map } from 'App/Map'
import { set, get, del } from 'idb-keyval'
import { addDataset } from 'state/map/mapSlice'
import { setMetaData } from 'state/meta/metaSlice'
import { useDispatch } from 'react-redux'
import { ViewportContext } from 'contexts/ViewportContext'
import { Tools } from './tools'
import { CssBaseline, ThemeProvider as MUIThemeProvider } from '@mui/material'
import { ThemeProvider } from 'emotion-theming'
import { MapStackTheme } from 'ui-components'

// transform asset path to identical pattern using import.meta.url which does work in the browser
const getMetaData = (filePath) => {
  return new URL(`../assets/${filePath}`, import.meta.url).pathname
}

const icon = getMetaData('image/currency.png')
const metaDataJson = getMetaData('data/meta.json')
const geosonData = getMetaData(
  'data/757b64e5-bb80-4316-baeb-334e1684344f.geojson'
)

export const App = (props) => {
  const dispatch = useDispatch()

  useEffect(async () => {
    // Fetching meta data
    const response = await axios.get(metaDataJson)

    console.log('metaDataJson: ', response)

    const dataSetId = response.data.dataSet
    const version = response.data.version
    const hadDataSet = await get(dataSetId)
    const metaData = response.data

    if (hadDataSet) {
      // Data is changed when the version is meta data is changed
      const isDataSetChanged =
        (await get(`${dataSetId}:metaData`)).version !== version

      if (isDataSetChanged) {
        // Refetching data once data is changed
        refetchData(dataSetId, (data) => {
          storeData(data, dataSetId, metaData)
          // Trigger a redux action 'addDataset',
          // Any react component that are listening to 'datasets' will receive an update
          dispatchActions(metaData, dataSetId)
        })
      } else dispatchActions(metaData, dataSetId)
    } else {
      // Freshly fetching data for the first time
      fetchData(dataSetId, (data) => {
        storeData(data, dataSetId, metaData)
        dispatchActions(metaData, dataSetId)
      })
    }
  })

  const dispatchActions = (metaData, dataSetId) => {
    dispatch(addDataset(dataSetId))
    dispatch(setMetaData(metaData))
  }

  /**
   * Store data into local storage, indexedDB
   */
  const storeData = async (data, dataSetId, metaData) => {
    await set(dataSetId, data)
    await set(`${dataSetId}:metaData`, metaData)
  }

  /*
   * Refetch geojson data
   */
  const refetchData = async (dataSetId, callback) => {
    await del(dataSetId)
    fetchData(dataSetId, callback)
  }

  /*
   * Fetch geojson data
   */
  const fetchData = async (dataSetId, callback) => {
    // environment variable 'process.env.REACT_APP_CLOUDFRONT_URL' wil resolve to '<DEVELOPER_SUBDOMAIN>.mapstack.io'
    // when running app from example folder, else resolve to 'localhost' when running from
    // Nextjs or in cloudfront
    const response = await axios.get(geosonData)
    console.log('geosonData: ', response)
    callback(response.data)
  }

  /**
   * Public Methods
   */
  return (
    <MUIThemeProvider theme={MapStackTheme}>
      <ThemeProvider theme={MapStackTheme}>
        <ViewportContext.Provider value={props.viewport}>
          <CssBaseline />
          <Map />
          <img
            src={icon}
            style={{ position: 'absolute', zIndex: 99, top: 10, left: 20 }}
          />
          <Tools />
        </ViewportContext.Provider>
      </ThemeProvider>
    </MUIThemeProvider>
  )
}
