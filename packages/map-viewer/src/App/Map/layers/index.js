import React, { memo } from 'react'
import { datasets } from 'state/map/mapSlice'
import { useSelector } from 'react-redux'
import { GeojsonLayer } from './Geojson'
export const Layers = memo((props) => {
  const datasetsData = useSelector(datasets)
  return (
    <>
      {datasetsData.map((dataset, key) => (
        <div key={key}>
          <GeojsonLayer data={dataset} layerStyle={props.layerStyle} />
        </div>
      ))}
    </>
  )
})
