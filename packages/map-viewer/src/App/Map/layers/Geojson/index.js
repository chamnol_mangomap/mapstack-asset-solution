import React, { useEffect, useState } from 'react'
import { setFitBounds } from 'state/map/mapSlice'
import { addLayerSource } from 'state/layer/layerSlice'
import { useDispatch, useSelector } from 'react-redux'
import { get } from 'idb-keyval'
import turf from '@turf/bbox'
import _ from 'lodash'
import layerUtil from './layerUtil'
import { layerStyle } from 'state/layerStyle/layerStyleSlice'
import { View } from './view'

export const GeojsonLayer = (props) => {
  const layerStyleData = useSelector(layerStyle)
  const [style, setStyle] = useState()
  const dispatch = useDispatch()
  const source = 'geojson-layer'
  const [data, setData] = useState(null)
  useEffect(async () => {
    get(props.data).then((value) => {
      setData(value)
      dispatch(setFitBounds(turf(value)))
      dispatch(addLayerSource(source))
    })
  }, [props])

  useEffect(() => {
    if (!_.isEmpty(layerStyleData)) {
      if (layerStyleData.type === 'quantity') {
        setStyle(
          layerUtil.getQuantitiesStyle(
            layerStyleData.value /* feature property */,
            layerStyleData.intervals /* Equal intervals */
          )
        )
      }
      if (layerStyleData.type === 'default') {
        setStyle(layerUtil.getDefaultStyle())
      }
    }
  }, [layerStyleData])

  const tmpProps = { ...props }
  if (style) tmpProps.layerStyle = style

  return <>{data && <View {...tmpProps} data={data} />}</>
}

GeojsonLayer.defaultProps = {
  type: 'geojson',
  layerStyle: layerUtil.getDefaultStyle(),
  filterLayerStyle: layerUtil.getFilterLayerStyle()
}
