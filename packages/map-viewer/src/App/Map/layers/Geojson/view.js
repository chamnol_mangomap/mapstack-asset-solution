import React from 'react'
import { Source, Layer } from 'react-map-gl'

export const View = (props) => {
  const { data, layerStyle, filterLayerStyle, type } = props
  return (
    <Source id='geojson-layer' type={type} data={data} generateId>
      <Layer {...layerStyle} />
      <Layer {...filterLayerStyle} />
    </Source>
  )
}
