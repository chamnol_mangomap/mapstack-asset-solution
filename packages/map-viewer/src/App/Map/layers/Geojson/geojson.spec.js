import React from 'react'
import { mount } from 'enzyme'
import { act } from 'react-dom/test-utils'
import { Provider } from 'react-redux'
import { GeojsonLayer } from '.'
import { store } from 'state/store'
import { set, del } from 'idb-keyval'
import given from 'utils/testing/steps/given'

//Mock react-map-gl, webgl won't render in jest
jest.mock('react-map-gl')
import { Source } from 'react-map-gl'

describe('Geojson Layer', () => {
  const id = 'some-random-uuid'

  const setUp = async () => {
    await set(id, given.geojson)
    await act(async () => {
      mount(
        <Provider store={store}>
          <GeojsonLayer data={id} />
        </Provider>
      )
    })
  }
  const tearDown = async () => {
    await del(id)
    jest.clearAllMocks()
  }

  it('Props contains geojson data', async () => {
    Source.mockImplementation((props) => {
      expect(props.data).toEqual(given.geojson)
      return <div></div>
    })
    await setUp()
    expect(Source).toHaveBeenCalled()
  })

  it('Dispatch setFitbound', () => {
    expect(store.getState().map.fitBounds).toEqual(given.fitBounds)
  })

  afterAll(() => {
    tearDown()
  })
})
