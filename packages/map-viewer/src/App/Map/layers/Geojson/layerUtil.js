import {
  LAYER_FILL_COLORS,
  LAYER_OUTLINE_COLORS,
  LAYER_OUTLINE_HIGHLIGHT_COLORS,
  LAYER_FILL_OPACITY,
  LAYER_FILL_HIGHLIGHT_COLORS,
  QUANTITY_SELECTION_MAP_COLORS,
  LAYER_FILL_SELECTION_COLORS,
  LAYER_OUTLINE_SELECTION_COLORS
} from 'assets/color/Colors'

function getDefaultStyle() {
  return {
    type: 'fill',
    paint: {
      'fill-color': LAYER_FILL_COLORS[0],
      'fill-outline-color': LAYER_OUTLINE_COLORS[0],
      'fill-opacity': LAYER_FILL_OPACITY[0]
    }
  }
}

function getFilterLayerStyle() {
  return {
    type: 'fill',
    paint: {
      'fill-color': [
        'case',
        ['boolean', ['feature-state', 'feature-selected'], false],
        LAYER_FILL_SELECTION_COLORS[0],
        ['boolean', ['feature-state', 'highlight'], false],
        LAYER_FILL_HIGHLIGHT_COLORS[0],
        'transparent'
      ],
      'fill-outline-color': [
        'case',
        ['boolean', ['feature-state', 'feature-selected'], false],
        LAYER_OUTLINE_SELECTION_COLORS[0],
        ['boolean', ['feature-state', 'highlight'], false],
        LAYER_OUTLINE_HIGHLIGHT_COLORS[0],
        'transparent'
      ],
      'fill-opacity': LAYER_FILL_OPACITY[0]
    }
  }
}

function getColorRampsAndValue(intervals) {
  const values = []
  for (let i = 0; i < intervals.length - 1; i++) {
    values.push(intervals[i])
    values.push(QUANTITY_SELECTION_MAP_COLORS[i])
  }
  return values
}

function getQuantitiesStyle(property, intervals) {
  return {
    type: 'fill',
    paint: {
      'fill-color': [
        'interpolate',
        ['linear'],
        ['get', property],
        ...getColorRampsAndValue(intervals)
      ],
      'fill-outline-color': [
        'case',
        ['boolean', ['feature-state', 'highlight'], false],
        LAYER_OUTLINE_HIGHLIGHT_COLORS[0],
        LAYER_OUTLINE_COLORS[0]
      ],
      'fill-opacity': LAYER_FILL_OPACITY[0]
    }
  }
}

export default {
  getDefaultStyle,
  getQuantitiesStyle,
  getColorRampsAndValue,
  getFilterLayerStyle
}
