import { useState, useEffect } from 'react'
import { useFeatureSelection } from 'state/featureSelection/featureSelectionSlice'
export const handleFeatureSelection = () => {
  const featureSelection = useFeatureSelection()
  const [selectedFeature, setSelectedFeature] = useState(null)
  const [map, setMap] = useState(null)

  function highlightFeatureSelection(map, selection) {
    const { source, id } = selection
    map.setFeatureState(
      {
        source,
        id
      },
      {
        'feature-selected': true
      }
    )
  }

  function clearHighlightFeatureSelection(map, selectedFeature) {
    const { source, id } = selectedFeature
    map.setFeatureState(
      {
        source,
        id
      },
      {
        'feature-selected': false
      }
    )
  }

  useEffect(() => {
    if (map && featureSelection) {
      if (selectedFeature) clearHighlightFeatureSelection(map, selectedFeature)
      highlightFeatureSelection(map, featureSelection)
      setSelectedFeature(featureSelection)
    } else {
      if (selectedFeature) clearHighlightFeatureSelection(map, selectedFeature)
    }
  }, [featureSelection, map])
  return [setMap]
}
