import React from 'react'
import { mount } from 'enzyme'
import { Map } from './index'
import { addLayerSource } from 'state/layer/layerSlice'
import { ViewportContext } from 'contexts/ViewportContext'
import { act } from 'react-dom/test-utils'
import { Provider } from 'react-redux'
import { store } from 'state/store'
import given from 'utils/testing/steps/given'
import { setSelectionSet } from 'state/map/mapSlice'
import { setFeatureSelection } from 'state/featureSelection/featureSelectionSlice'
import { waitFor } from '@testing-library/react'
import { View } from './view'

jest.mock('./view')

// Creating map object, to be spied on methods
const map = {
  on() {},
  off() {},
  getCenter() {},
  getZoom() {},
  fitBounds() {},
  setFeatureState() {},
  removeFeatureState() {},
  querySourceFeatures() {
    return given.features
  }
}

describe('Map', () => {
  jest.useFakeTimers()
  const setUp = async () => {
    // Mock View so we will only focus on testing the functionalities of controller
    View.mockImplementation(() => {
      return <div />
    })

    await act(async () => {
      // Mock View so we will only focus on testing the functionalities of controller
      View.mockImplementation((props) => {
        props.onMapInstance(map)
        return <div />
      })
      mount(
        <Provider store={store}>
          <ViewportContext.Provider value={given.viewPort}>
            <Map />
          </ViewportContext.Provider>
        </Provider>
      )
    })
    jest.advanceTimersByTime(3000)
  }

  it('Should highlight a feature selection', async () => {
    await setUp()
    store.dispatch(setFeatureSelection(given.featureSelection))

    const spyOnRemoveFeatureState = jest.spyOn(map, 'removeFeatureState')
    const spyOnSetFeatureState = jest.spyOn(map, 'setFeatureState')

    // Using waitFor as a delay, since when using jest.useFakeTimers(), setting delay with setTimeout won't work
    await waitFor(() => {}, { timeout: 25, interval: 10 })

    expect(spyOnRemoveFeatureState).not.toHaveBeenCalledWith()

    const arg1 = { id: 252, source: given.selectionSet.source }
    const arg2 = { 'feature-selected': true }
    expect(spyOnSetFeatureState).toHaveBeenCalledWith(arg1, arg2)
  })

  it('Show selectionSet where state is ohio', async () => {
    store.dispatch(addLayerSource('geojson-layer'))
    await setUp()

    const spyOnRemoveFeatureState = jest.spyOn(map, 'removeFeatureState')
    const spyOnSetFeatureState = jest.spyOn(map, 'setFeatureState')
    store.dispatch(setSelectionSet(given.selectionSet))

    // Using waitFor as a delay, since when using jest.useFakeTimers(), setting delay with setTimeout won't work
    await waitFor(() => {}, { timeout: 25, interval: 10 })

    const arg0 = { source: 'geojson-layer' }
    expect(spyOnRemoveFeatureState).toHaveBeenCalledWith(arg0)

    const arg1 = { id: 1, source: given.selectionSet.source }
    const arg2 = { highlight: true }
    expect(spyOnSetFeatureState).toHaveBeenCalledWith(arg1, arg2)

    expect(store.getState().map.fitBounds).toEqual([
      -88.0279541015625, 30.557530797259176, -87.989501953125, 30.75127777625781
    ])
    const matchResult = given.features.find((feature) => feature.id === 1)
    expect(store.getState().filter.filterResults[0].id).toEqual(matchResult.id)
  })

  it('Show selectionSet where state is Ohio or state is Alabama', async () => {
    store.dispatch(addLayerSource('geojson-layer'))
    await setUp()
    const spyOnSetFeatureState = jest.spyOn(map, 'setFeatureState')

    const selectionSet = {
      source: 'geojson-layer',
      filters: [
        {
          id: 'd1fb0ae7-3a82-5120-b523-96568f66cc1e',
          filter: {
            property: 'State',
            operator: '==',
            value: 'Ohio'
          }
        },
        {
          id: '3ec1a08c-dbf2-50fb-b481-8a3a3cf32149',
          filter: {
            property: 'State',
            operator: '==',
            value: 'Alabama'
          }
        }
      ]
    }
    store.dispatch(setSelectionSet(selectionSet))
    await waitFor(() => {}, { timeout: 25, interval: 10 })

    let arg1 = { id: 0, source: given.selectionSet.source }
    let arg2 = { highlight: true }
    expect(spyOnSetFeatureState).toHaveBeenCalledWith(arg1, arg2)

    arg1 = { id: 3, source: given.selectionSet.source }
    arg2 = { highlight: true }
    expect(spyOnSetFeatureState).toHaveBeenCalledWith(arg1, arg2)

    arg1 = { id: 1, source: given.selectionSet.source }
    arg2 = { highlight: true }
    expect(spyOnSetFeatureState).toHaveBeenCalledWith(arg1, arg2)

    arg1 = { id: 4, source: given.selectionSet.source }
    arg2 = { highlight: true }
    expect(spyOnSetFeatureState).toHaveBeenCalledWith(arg1, arg2)
  })

  it('Show selectionSet where Population > 10000 and Population <= 50000', async () => {
    store.dispatch(addLayerSource('geojson-layer'))
    await setUp()
    // We spy on map.setFeatureState, this method will be called with expected arguments
    const spy = jest.spyOn(map, 'setFeatureState')

    const selectionSet = {
      source: 'geojson-layer',
      filters: [
        {
          id: 'd1fb0ae7-3a82-5120-b523-96568f66cc1e',
          filter: {
            property: 'Population',
            operator: '>',
            value: '100000'
          }
        },
        {
          id: '3ec1a08c-dbf2-50fb-b481-8a3a3cf32149',
          filter: {
            property: 'Population',
            operator: '<=',
            value: '500000'
          }
        }
      ]
    }

    store.dispatch(setSelectionSet(selectionSet))

    // Using waitFor as a delay, since when using jest.useFakeTimers(), setting delay with setTimeout won't work
    await waitFor(() => {}, { timeout: 25, interval: 10 })

    let arg1 = { id: 1, source: given.selectionSet.source }
    let arg2 = { highlight: true }
    expect(spy).toHaveBeenCalledWith(arg1, arg2)

    arg1 = { id: 4, source: given.selectionSet.source }
    arg2 = { highlight: true }
    expect(spy).toHaveBeenCalledWith(arg1, arg2)
  })

  it('Show selectionSet where Households > 8000 and Median age > 40', async () => {
    store.dispatch(addLayerSource('geojson-layer'))
    await setUp()
    // We spy on map.setFeatureState, this method will be called with expected arguments
    const spy = jest.spyOn(map, 'setFeatureState')

    const selectionSet = {
      source: 'geojson-layer',
      filters: [
        {
          id: 'd1fb0ae7-3a82-5120-b523-96568f66cc1e',
          filter: {
            property: 'Households',
            operator: '>',
            value: '6600'
          }
        },
        {
          id: '3ec1a08c-dbf2-50fb-b481-8a3a3cf32149',
          filter: {
            property: 'Median age',
            operator: '>',
            value: '40'
          }
        }
      ]
    }

    store.dispatch(setSelectionSet(selectionSet))

    // Using waitFor as a delay, since when using jest.useFakeTimers(), setting delay with setTimeout won't work
    await waitFor(() => {}, { timeout: 25, interval: 10 })

    let arg1 = { id: 1, source: given.selectionSet.source }
    let arg2 = { highlight: true }
    expect(spy).toHaveBeenCalledWith(arg1, arg2)

    arg1 = { id: 4, source: given.selectionSet.source }
    arg2 = { highlight: true }
    expect(spy).toHaveBeenCalledWith(arg1, arg2)

    arg1 = { id: 3, source: given.selectionSet.source }
    arg2 = { highlight: true }
    expect(spy).toHaveBeenCalledWith(arg1, arg2)
  })

  afterEach(() => {
    jest.resetModules()
    jest.clearAllMocks()
  })
})
