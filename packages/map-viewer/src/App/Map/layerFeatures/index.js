import { useState, useEffect } from 'react'
import { addLayerFeatures } from 'state/map/mapSlice'
import { layerSources } from 'state/layer/layerSlice'
import { useSelector, useDispatch } from 'react-redux'
export const handleLayerFeatures = () => {
  const dispatch = useDispatch()
  const layerSourcesData = useSelector(layerSources)

  const [layerSourceMap, setLayerSourceMap] = useState(null)

  useEffect(async () => {
    const hasLayerSource = layerSourcesData.length && layerSourceMap
    if (hasLayerSource) getVisibleFeatureInFullExtend()
  }, [layerSourcesData, layerSourceMap])

  /**
   * Retrieve features from all visible tiles and store it for other query usage.
   * Once the map is fully loaded with the max extent, then we could retrieve all the features.
   * for detail https://docs.mapbox.com/mapbox-gl-js/api/map/#map#querysourcefeatures
   */
  const getVisibleFeatureInFullExtend = () => {
    const intervalId = setInterval(() => {
      const features = layerSourceMap.querySourceFeatures(layerSourcesData[0])
      if (features.length) {
        dispatch(
          addLayerFeatures({
            source: layerSourcesData[0],
            features: features.map((result) => {
              return {
                id: result.id,
                geometry: result.geometry,
                properties: result.properties
              }
            })
          })
        )
        clearInterval(intervalId)
      }
    }, 500)
  }

  return [setLayerSourceMap]
}
