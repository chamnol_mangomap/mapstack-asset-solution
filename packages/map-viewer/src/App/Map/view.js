import React from 'react'
import PropTypes from 'prop-types'
import ReactMapGL from 'react-map-gl'
import { Layers } from './layers'

export const View = (props) => {
  const { viewport, onMapInstance, onViewportChange } = props
  return (
    <ReactMapGL
      ref={(ref) => ref && onMapInstance(ref.getMap())}
      {...viewport}
      height={`calc(100vh - 56px)`}
      mapStyle='https://api.maptiler.com/maps/toner/style.json?key=i96ELe4hrhWZ0qG6O2GY'
      onViewportChange={onViewportChange}
      pitchWithRotate={false}
      dragRotate={false}
      touchZoomRotate={false}
    >
      <Layers />
    </ReactMapGL>
  )
}

View.propTypes = {
  viewport: PropTypes.shape({
    width: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    height: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    latitude: PropTypes.number,
    longitude: PropTypes.number,
    zoom: PropTypes.number
  })
}

View.defaultProps = {
  viewport: {
    width: '100wh',
    height: 'calc(100vh - 56px)',
    latitude: 38.84609720557644,
    longitude: -100.09397174691415,
    zoom: 3.703849286134473
  }
}
