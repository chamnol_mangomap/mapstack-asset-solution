import { useState, useEffect } from 'react'
import { highlightSelectionSet } from '../highlightSelectionSet'
import { useSelector } from 'react-redux'
import { selectionSet } from 'state/map/mapSlice'
export const handleFilterSelection = () => {
  const selectionSetData = useSelector(selectionSet)
  const [filterSelectionMap, setFilterSelectionMap] = useState(null)

  // highlight features on the map once selectionSetData is updated by filter tools
  useEffect(() => {
    if (selectionSetData && filterSelectionMap)
      highlightSelectionSet(filterSelectionMap, selectionSetData)
  }, [selectionSetData, filterSelectionMap])

  return [setFilterSelectionMap]
}
