import React from 'react'
import { mount } from 'enzyme'
import { Map } from './index'
import { View } from './view'
import { ViewportContext } from 'contexts/ViewportContext'
import { act } from 'react-dom/test-utils'
import { Provider } from 'react-redux'
import { store } from 'state/store'
import given from 'utils/testing/steps/given'
import { setFitBounds } from 'state/map/mapSlice'

describe('Map', () => {
  let mapComponent

  beforeAll(async () => {
    await act(async () => {
      mapComponent = mount(
        <Provider store={store}>
          <ViewportContext.Provider value={given.viewPort}>
            <Map />
          </ViewportContext.Provider>
        </Provider>
      )
    })
  })

  it('Map should have viewport from context', () => {
    expect(mapComponent.find(View).props().viewport).toEqual(given.viewPort)
  })

  it('Map should zoom and move to position that best fits the layer', async () => {
    // Dispatch setFitBounds which make the map best fit the layer (lat, lng, zoom)
    store.dispatch(setFitBounds(given.fitBounds))
    await new Promise((r) => setTimeout(r, 100))

    // Enzyme require mounted mapComponent to call .update() to make component and its children re-render
    mapComponent.update()
    const expectedMapViewport = {
      longitude: 125.60000000000002,
      latitude: 10.099999999999957,
      zoom: 24
    }

    const viewPort = mapComponent.find(View).props().viewport
    expect(viewPort.longitude).toEqual(expectedMapViewport.longitude)
    expect(viewPort.latitude).toEqual(expectedMapViewport.latitude)
    expect(viewPort.zoom).toEqual(expectedMapViewport.zoom)
  })

  afterAll(() => {
    jest.resetModules()
    jest.clearAllMocks()
  })
})
