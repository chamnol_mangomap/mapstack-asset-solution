import jmespath from 'jmespath'
import { store } from 'state/store'
import turf from '@turf/bbox'
import { setFitBounds } from 'state/map/mapSlice'
import { setFilterResults } from 'state/filter/filterSlice'
export function highlightSelectionSet(map, selectionSetData) {
  clearHighlight(map, selectionSetData)
  const hasSelectionSet = selectionSetData.filters.length
  if (!hasSelectionSet) return

  const source = selectionSetData.source
  const features = getMapFeaturesBySource(source)
  const filterResults = getFilterResult(features, selectionSetData)

  dispatchFitBounds(filterResults)

  store.dispatch(setFilterResults(filterResults))

  highlight(map, filterResults, source)
}

export function highlightFeatureSelection(map, selection) {
  const { source, id } = selection
  map.setFeatureState(
    {
      source,
      id
    },
    {
      'feature-selected': true
    }
  )
}

export function clearHighlightFeatureSelection(map, selectedFeature) {
  const { source, id } = selectedFeature
  map.setFeatureState(
    {
      source,
      id
    },
    {
      'feature-selected': false
    }
  )
}

const dispatchFitBounds = (results) => {
  const bounds = turf({
    type: 'FeatureCollection',
    features: results
  })

  store.dispatch(setFitBounds(bounds))
}

const highlight = (map, results, source) => {
  results.map((result) => {
    map.setFeatureState(
      {
        source: source,
        id: result.id
      },
      {
        highlight: true
      }
    )
  })
}

const clearHighlight = (map, selectionSetData) => {
  map.removeFeatureState({ source: selectionSetData.source })
}

const getMapFeaturesBySource = (source) => {
  return store
    .getState()
    .map.layersFeatures.find((layerFeatures) => layerFeatures.source === source)
    .features
}

const getFilterResult = (features, selectionSetData) => {
  // The filter string follow this format jmespath.search(geojson.features, '[?properties."Population"<`1000000`&&properties."Population">`800000`]')
  return (
    jmespath
      .search(features, createFilterString(selectionSetData))
      // Remove duplicated value
      .reduce((acc, current) => {
        const x = acc.find((item) => item.id === current.id)
        if (!x) {
          return acc.concat([current])
        } else {
          return acc
        }
      }, [])
      .map((result) => {
        return {
          source: selectionSetData.source,
          id: result.id,
          geometry: result.geometry,
          properties: result.properties
        }
      })
  )
}

const createFilterString = (selectionSetData) => {
  const noneNumericFilterString = createNoneNumericFilterString(
    selectionSetData.filters
  )

  const numericFilterString = createNumericFilterString(
    selectionSetData.filters
  )

  return (
    '[?' +
    (numericFilterString
      ? numericFilterString + (noneNumericFilterString ? '&&' : '')
      : '') +
    (noneNumericFilterString ? '(' + noneNumericFilterString + ')' : '') +
    ']'
  )
}

const createNumericFilterString = (filters) => {
  const numericEquations = []
  filters
    .filter(
      (selection) =>
        selection.filter.operator !== '==' && selection.filter.operator !== '!='
    )
    .map((filter) => {
      const filterProps = filter.filter
      numericEquations.push(
        'properties."' +
          filterProps.property +
          '"' +
          filterProps.operator +
          '`' +
          filterProps.value +
          '`'
      )
    })

  return numericEquations.join('&&')
}

const createNoneNumericFilterString = (filters) => {
  const noneNumericEquations = []

  filters
    .filter(
      (selection) =>
        selection.filter.operator === '==' || selection.filter.operator === '!='
    )
    .map((filter) => {
      const filterProps = filter.filter
      noneNumericEquations.push(
        'properties."' +
          filterProps.property +
          '"' +
          filterProps.operator +
          '`' +
          filterProps.value +
          '`'
      )
    })

  return noneNumericEquations.join('||')
}
