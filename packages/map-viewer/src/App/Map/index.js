import React, { useState, memo, useEffect, useContext } from 'react'
import { View } from './view.js'
import { fitBounds } from 'state/map/mapSlice'
import { useSelector } from 'react-redux'
import { ViewportContext } from 'contexts/ViewportContext'
import { useWindowSize } from 'utils/hooks/windowSize'
import { WebMercatorViewport } from 'react-map-gl'
import { handleFeatureSelection } from './featureSelection'
import { handleFilterSelection } from './filterSelection'
import { handleLayerFeatures } from './layerFeatures'

/**
 * A React map component contains base map, layers and other map control.
 */
export const Map = memo(() => {
  let map

  const [setMap] = handleFeatureSelection()
  const [setFilterSelectionMap] = handleFilterSelection()
  const [setLayerSourceMap] = handleLayerFeatures()

  // fitBoundsData a Redux event listener that will receive fitBound information triggered by layers, once a layer is added and its bounding box extent is calculated
  const fitBoundsData = useSelector(fitBounds)

  const [viewport, setViewport] = useState(useContext(ViewportContext))
  const { width, height } = useWindowSize()

  useEffect(() => {
    const hasFitbounds = fitBoundsData.length && width && height
    if (hasFitbounds) handleFitbounds()
  }, [fitBoundsData, width, height])

  useEffect(() => {
    if (map) {
      setLayerSourceMap(map)
      setFilterSelectionMap(map)
      setMap(map)
    }
  }, [map])

  // Recalibrate viewport size once window size is changed( portrait to landscape)
  useEffect(() => {
    if (width) setViewportResponsive()
  }, [width])

  /**
   * Recenter and best fit zoom when setFitbounds is dispatched
   */
  const handleFitbounds = () => {
    const isVertical = width < height

    const value = new WebMercatorViewport({
      width,
      height
    }).fitBounds(
      [
        [fitBoundsData[0], fitBoundsData[1]],
        [fitBoundsData[2], fitBoundsData[3]]
      ],
      { padding: isVertical ? 20 : 40 }
    )
    const { zoom, longitude, latitude } = value
    const temp = {
      ...viewport,
      zoom,
      longitude,
      latitude,
      transitionDuration: 1000
    }
    setViewport(temp)
  }

  /**
   * Making viewport responsive to window size changed, (portrait to landscape)
   */
  const setViewportResponsive = () => {
    const tmpViewport = { ...viewport, width, height }
    setViewport(tmpViewport)
  }

  /**
   * Storing map-gl instance, helpful when needs to interact with map through its API
   */
  const handleMapInstance = (mapInstance) => {
    map = mapInstance
    //window.map = map
  }

  /**
   * Recalibrate every time viewport changed. Fired every map moved or zoomend.
   */
  const handleViewportChange = (value) => {
    setViewport(value)
  }

  return (
    <View
      fitBounds={fitBoundsData}
      viewport={viewport}
      onViewportChange={handleViewportChange}
      onMapInstance={handleMapInstance}
    />
  )
})
