import { createContext } from 'react'

const viewportProps = {
  width: '100wh',
  height: '100vh',
  latitude: 40.111688665595956,
  longitude: -95.66894531250001,
  zoom: 4
}

export const ViewportContext = createContext(viewportProps)
