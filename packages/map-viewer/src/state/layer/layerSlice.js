import { createSlice } from '@reduxjs/toolkit'

const initialState = {
  layerSources: []
}

export const layerSlice = createSlice({
  name: 'layer',
  initialState,
  reducers: {
    addLayerSource: (state, action) => {
      state.layerSources.push(action.payload)
    }
  }
})

export const layerSources = (state) => state.layer.layerSources
export const { addLayerSource } = layerSlice.actions

export default layerSlice.reducer
