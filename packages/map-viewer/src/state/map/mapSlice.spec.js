import mapSliceReducer, {
  addDataset,
  setFitBounds,
  setSelectionSet,
  removeFilterFromSelectionSet
} from './mapSlice'
import given from 'utils/testing/steps/given'

// TODO: update spec after add layerFeatures field
describe('map reducer', () => {
  const initialState = {
    datasets: [],
    fitBounds: [],
    layersFeatures: [],
    selectionSet: null
  }

  it('should handle initial state', () => {
    expect(mapSliceReducer(undefined, { type: 'unknown' })).toEqual(
      initialState
    )
  })

  it('should handle addDataset', () => {
    const dataset = 'some-random-uuid'
    const actual = mapSliceReducer(initialState, addDataset(dataset))
    expect(actual.datasets[0]).toEqual(dataset)
  })

  it('should handle setFitBounds', () => {
    const actual = mapSliceReducer(initialState, setFitBounds(given.fitBounds))
    expect(actual.fitBounds).toEqual(given.fitBounds)
  })

  it('should handle setSelectionSet', () => {
    const actual = mapSliceReducer(
      initialState,
      setSelectionSet(given.selectionSet)
    )
    expect(actual.selectionSet).toEqual(given.selectionSet)
  })

  it('should removeFilterFromSelectionSet', () => {
    const newState = mapSliceReducer(
      initialState,
      setSelectionSet(given.selectionSet)
    )

    const actual = mapSliceReducer(
      newState,
      removeFilterFromSelectionSet(given.selectionSet.filters[0].id)
    )
    expect(actual.selectionSet.filters.length).toEqual(0)
  })
})
