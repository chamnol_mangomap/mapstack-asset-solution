import { createSlice } from '@reduxjs/toolkit'

const initialState = {
  datasets: [],
  fitBounds: [],
  selectionSet: null,
  layersFeatures: []
}

export const mapSlice = createSlice({
  name: 'map',
  initialState,
  reducers: {
    addLayerFeatures: (state, action) => {
      state.layersFeatures.push(action.payload)
    },
    removeFilterFromSelectionSet: (state, action) => {
      if (!state.selectionSet) return
      state.selectionSet.filters = state.selectionSet.filters.filter(
        (filter) => filter.id !== action.payload
      )
    },
    setSelectionSet: (state, action) => {
      state.selectionSet = action.payload
    },
    addDataset: (state, action) => {
      state.datasets.push(action.payload)
    },
    setFitBounds: (state, action) => {
      state.fitBounds = action.payload
    }
  }
})

export const {
  addDataset,
  setFitBounds,
  setSelectionSet,
  addLayerFeatures,
  removeFilterFromSelectionSet
} = mapSlice.actions

export const datasets = (state) => state.map.datasets
export const fitBounds = (state) => state.map.fitBounds
export const selectionSet = (state) => state.map.selectionSet
export const layersFeatures = (state) => state.map.layersFeatures

export default mapSlice.reducer
