import metaSliceReducer, { setMetaData } from './metaSlice'
import given from 'utils/testing/steps/given'

describe('meta reducer', () => {
  const initialState = {
    metaData: {}
  }

  it('should handle initial state', () => {
    expect(metaSliceReducer(undefined, { type: 'unknown' })).toEqual(
      initialState
    )
  })

  it('should handle setMetaData', () => {
    const actual = metaSliceReducer(initialState, setMetaData(given.metaData))
    expect(actual.metaData).toEqual(given.metaData)
  })
})
