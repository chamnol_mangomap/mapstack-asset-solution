import { useSelector } from 'react-redux'
import { createSlice } from '@reduxjs/toolkit'
import _ from 'lodash'
import { filterObjectByMatchProperty } from 'utils/functions'

const initialState = {
  metaData: {}
}

export const metaSlice = createSlice({
  name: 'meta',
  initialState,
  reducers: {
    setMetaData: (state, action) => {
      return {
        ...state,
        metaData: action.payload
      }
    }
  }
})

export const { setMetaData } = metaSlice.actions

export const metaData = (state) => state.meta.metaData
export const dataSet = (state) => _.get(state, ['meta', 'metaData', 'dataSet'])
export const filterSchema = (state) =>
  filterObjectByMatchProperty(state.meta.metaData.schema, { filter: true })
export const quantitySchema = (state) =>
  filterObjectByMatchProperty(state.meta.metaData.schema, { quantity: true })
export const summarySchema = (state) =>
  filterObjectByMatchProperty(state.meta.metaData.schema, { summary: true })
export const categorySchema = (state) =>
  filterObjectByMatchProperty(state.meta.metaData.schema, { category: true })

/**
 * Hook for retrieving all the meta data
 * @returns {Array} filters data
 */
export const useMetaData = () => {
  const state = useSelector(metaData)
  return state
}

export default metaSlice.reducer
