import { configureStore } from '@reduxjs/toolkit'
import mapSliceReducer from 'state/map/mapSlice'
import filterSliceReducer from 'state/filter/filterSlice'
import metaDataReducer from 'state/meta/metaSlice'
import controlSliceReducer from 'state/control/controlSlice'
import layerSliceReducer from 'state/layer/layerSlice'
import featureSelectionSliceReducer from 'state/featureSelection/featureSelectionSlice'
import layerStyleSliceReducer from 'state/layerStyle/layerStyleSlice'

export const store = configureStore({
  reducer: {
    map: mapSliceReducer,
    filter: filterSliceReducer,
    meta: metaDataReducer,
    control: controlSliceReducer,
    layer: layerSliceReducer,
    layerStyle: layerStyleSliceReducer,
    featureSelection: featureSelectionSliceReducer
  }
})
