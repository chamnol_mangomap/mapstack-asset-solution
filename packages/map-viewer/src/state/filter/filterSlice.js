import { useSelector } from 'react-redux'
import { createSlice } from '@reduxjs/toolkit'
import { findIndex } from 'lodash'

const initialState = {
  filters: [],
  filterResults: []
}

export const filterSlice = createSlice({
  name: 'filter',
  initialState,
  reducers: {
    setFilterResults: (state, action) => {
      state.filterResults = action.payload
    },
    addFilter: (state, action) => {
      state.filters.push(action.payload)
    },
    removeFilter: (state, action) => {
      state.filters = state.filters.filter(
        (filter) => filter.id !== action.payload
      )
    },
    removeFilters: (state, action) => {
      let filters = [...state.filters]
      action.payload.map((payloadFilter) => {
        filters = filters.filter((filter) => filter.id !== payloadFilter.id)
      })
      state.filters = filters
    },
    clearAllFilters: (state) => {
      state.filters = []
    },
    addFilters: (state, action) => {
      state.filters.push(...action.payload)
    },
    updateFilter: (state, action) => {
      const index = findIndex(state.filters, { id: action.payload.id })
      state.filters.splice(index, 1, action.payload)
    }
  }
})

export const filters = (state) => state.filter.filters

export const filterResults = (state) => state.filter.filterResults

/**
 * Hook for retrieving all the filters data
 * @returns {Array} filters data
 */
export const useFilters = () => {
  const state = useSelector(filters)
  return state
}

/**
 * Hook for retrieving filter results data
 * @returns {Array} filter results data
 */
export const useFilterResults = () => {
  const state = useSelector(filterResults)
  return state
}

export const {
  setFilterResults,
  addFilter,
  addFilters,
  removeFilter,
  removeFilters,
  clearAllFilters,
  updateFilter
} = filterSlice.actions

export default filterSlice.reducer
