import { store } from '../store'
import filterSliceReducer, {
  setFilterResults,
  addFilter,
  removeFilter,
  removeFilters,
  clearAllFilters,
  addFilters,
  updateFilter
} from './filterSlice'
import given from 'utils/testing/steps/given'

describe('filter reducer', () => {
  afterEach(() => {
    store.dispatch(clearAllFilters())
  })

  const initialState = {
    filters: [],
    filterResults: []
  }

  it('should set initial state correctly', () => {
    expect(filterSliceReducer(undefined, { type: 'unknown' })).toEqual(
      initialState
    )
  })

  it('should set filter result correctly', () => {
    const actual = filterSliceReducer(
      initialState,
      setFilterResults(given.filterResults)
    )
    expect(actual.filterResults).toBe(given.filterResults)
  })

  it('should add a filter to the filter list', () => {
    const actual = filterSliceReducer(initialState, addFilter(given.filters[0]))
    expect(actual.filters[0]).toEqual(given.filters[0])
    expect(actual.filters).toHaveLength(1)
  })

  it('should remove a filter from the filter list', () => {
    store.dispatch(clearAllFilters())
    store.dispatch(addFilters(given.filters))
    let actual = store.getState().filter
    expect(actual.filters).toHaveLength(3)
    store.dispatch(removeFilter(given.filters[0].id))

    actual = store.getState().filter
    expect(actual.filters).toHaveLength(2)
    expect(actual.filters[0]).toEqual(given.filters[1])
  })

  it('should remove multiple filters from the filter list', () => {
    store.dispatch(clearAllFilters())
    store.dispatch(addFilters(given.filters))
    let actual = store.getState().filter
    expect(actual.filters).toHaveLength(3)

    store.dispatch(removeFilters(given.filters))
    actual = store.getState().filter
    expect(actual.filters).toHaveLength(0)
  })

  it('should clear all the filters from the list', () => {
    store.dispatch(clearAllFilters())
    store.dispatch(addFilters(given.filters))
    let actual = store.getState().filter
    expect(actual.filters).toHaveLength(3)

    store.dispatch(clearAllFilters())
    actual = store.getState().filter
    expect(actual.filters).toHaveLength(0)
  })

  it('should add multiple filters to the list', () => {
    let actual = filterSliceReducer(initialState, {})
    expect(actual.filters).toHaveLength(0)

    store.dispatch(addFilters(given.filters))
    actual = store.getState().filter
    expect(actual.filters).toHaveLength(3)
    expect(actual.filters).toEqual(given.filters)
  })

  it('should update the filter by their index', () => {
    const updateItem = {
      id: '48f54bef-56cb-5f5c-9c43-ad31ce643085',
      filter: {
        property: 'State',
        operator: '==',
        value: 'Arizona'
      }
    }

    store.dispatch(clearAllFilters())
    store.dispatch(addFilters(given.filters))
    store.dispatch(updateFilter(updateItem))

    const actual = store.getState().filter
    const updatedItem = actual.filters.filter(
      (item) => item.id === updateItem.id
    )[0]

    expect(updatedItem).toEqual(updateItem)
  })
})
