import { createSlice } from '@reduxjs/toolkit'
import { useSelector } from 'react-redux'

const INITIAL_STATE = { layerStyle: {} }

export const layerStyleSlice = createSlice({
  name: 'layerStyle',
  initialState: INITIAL_STATE,
  reducers: {
    setLayerStyle: (state, action) => {
      state.layerStyle = action.payload
    }
  }
})

export const layerStyle = (state) => state.layerStyle.layerStyle

/**
 * Hook for retrieving layer style
 * @returns {object}
 */
export const useLayerStyle = () => {
  const state = useSelector(layerStyle)
  return state
}

export const { setLayerStyle } = layerStyleSlice.actions

export default layerStyleSlice.reducer
