import { createSlice } from '@reduxjs/toolkit'

const INITIAL_STATE = { selections: {} }

export const selectionSlice = createSlice({
  name: 'selection',
  initialState: INITIAL_STATE,
  reducers: {
    addSelection: (state, action) => {
      state.selections.push(action.payload)
    },
    clearSelections: (state) => {
      state.selections = {}
    }
  }
})

export const selections = (state) => state.selection.selections

export const { addSelection, clearSelections } = selectionSlice.actions

export default selectionSlice.reducer
