import { useSelector } from 'react-redux'
import { createSlice } from '@reduxjs/toolkit'

const initialState = {
  featureSelection: null
}

export const featureSelectionSlice = createSlice({
  name: 'featureSelection',
  initialState,
  reducers: {
    setFeatureSelection: (state, action) => {
      state.featureSelection = action.payload
    },
    clearFeatureSelection: (state) => {
      state.featureSelection = null
    }
  }
})

export const featureSelection = (state) =>
  state.featureSelection.featureSelection

/**
 * Hook for retrieving featureSelection
 * @returns {object} feature
 */
export const useFeatureSelection = () => {
  return useSelector(featureSelection)
}

export const { setFeatureSelection, clearFeatureSelection } =
  featureSelectionSlice.actions

export default featureSelectionSlice.reducer
