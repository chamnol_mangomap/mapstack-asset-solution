import { createSlice } from '@reduxjs/toolkit'

const initialState = {
  panel: ''
}

export const controlSlice = createSlice({
  name: 'control',
  initialState,
  reducers: {
    setPanel: (state, action) => {
      state.panel = action.payload
    }
  }
})

export const panel = (state) => state.control.panel
export const { setPanel } = controlSlice.actions

export default controlSlice.reducer
