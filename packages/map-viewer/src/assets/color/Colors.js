export const LAYER_FILL_COLORS = ['#DE3C4B']
export const LAYER_FILL_HIGHLIGHT_COLORS = ['#21FA90']
export const LAYER_FILL_SELECTION_COLORS = ['#FF00FF']
export const LAYER_OUTLINE_COLORS = ['#666']
export const LAYER_OUTLINE_HIGHLIGHT_COLORS = ['#000 4px']
export const LAYER_OUTLINE_SELECTION_COLORS = ['#F0F 4px']
export const LAYER_FILL_OPACITY = [0.7]
export const QUANTITY_SELECTION_MAP_COLORS = [
  '#fef0d9',
  '#fdd49e',
  '#fdbb84',
  '#fc8d59',
  '#ef6548',
  '#d7301f',
  '#990000'
]
