import { createMuiTheme } from '@material-ui/core/styles'

// Create a theme instance.
export const theme = createMuiTheme({
  palette: {
    type: 'light',
    primary: {
      main: '#11a670',
      contrastText: '#ffffff'
    },
    secondary: {
      main: '#343423'
    },
    success: {
      main: '#11a670'
    },
    error: {
      main: '#cc451a'
    },
    background: {
      default: '#f5f5f5'
    }
  },
  typography: {
    fontSize: 16,
    fontFamily: '"Inter", "Roboto", sans-serif',
    overline: {
      fontSize: '0.75rem',
      fontWeight: 600,
      letterSpacing: '-0.003em'
    },
    button: {
      fontWeight: 600,
      fontSize: '1rem',
      letterSpacing: '-0.013rem'
    },
    body1: {
      fontWeight: 400,
      fontSize: '1rem',
      lineHeight: 1.25,
      letterSpacing: '-0.013rem'
    },
    body2: {
      fontWeight: 400,
      fontSize: '0.9rem',
      lineHeight: 1.25,
      letterSpacing: '-0.013rem'
    },
    h1: {
      fontSize: '2.5rem',
      lineHeight: 1.4
    },
    h2: {
      fontSize: '1.8rem',
      lineHeight: 1.12,
      fontWeight: 500
    },
    h3: {
      fontSize: '1.4rem',
      lineHeight: 1.5
    },
    h4: {
      fontSize: '1.2rem',
      lineHeight: 1.45
    },
    h5: {
      fontSize: '1.15rem',
      lineHeight: 1.35
    },
    h6: {
      fontWeight: 600,
      fontSize: '1.15rem',
      lineHeight: 1.2
    },
    caption: {
      fontSize: '.75rem',
      fontWeight: 500,
      lineHeight: '1.3',
      opacity: 0.87
    },
    subtitle2: {
      fontWeight: 300
    },
    subtitle1: {
      fontWeight: 300
    }
  },
  shape: {
    borderRadius: 6
  },
  breakpoints: {
    values: {
      xxs: 0,
      xs: 300,
      sm: 600,
      md: 960,
      lg: 1280,
      xl: 1920
    }
  },
  /**
   * Override the style of the component
   */
  overrides: {
    MuiPaper: {
      root: {
        padding: 8
      }
    },

    MuiTypography: {
      /*   textTransform: 'none',
      margin: '0 0 1rem', 
      h1: {
        fontSize: 24
      },
      h3: {
        fontSize: '2rem',
        fontWeight: '700',
        lineHeight: '1.75rem'
      }*/
    },
    MuiButton: {
      sizeSmall: {
        fontSize: 12,
        marginLeft: 24
      },
      textSecondary: {
        opacity: 0.8
      }
    },
    MuiOutlinedInput: {
      root: {
        background: 'white'
      },
      input: {
        padding: '10.5px 10px'
      }
    },
    MuiInputLabel: {
      outlined: {
        transform: 'translate(14px, 12px) scale(1)'
      }
    }
  },
  /**
   * Predefined props of compoment
   */
  props: {
    MuiButton: {
      variant: 'text',
      color: 'primary'
    }
    /*
    MuiAppBar: {
      color: 'transparent'
    } */
  }
})
