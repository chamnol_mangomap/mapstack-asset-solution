import { makeStyles } from '@material-ui/core'

export const styles = makeStyles((theme) => ({
  nestedList: {
    paddingLeft: theme.spacing(4)
  },
  gradientButton: {
    background: 'linear-gradient(45deg, #FE6B8B 30%, #FF8E53 90%)',
    border: 0,
    borderRadius: 3,
    boxShadow: '0 3px 5px 2px rgba(255, 105, 135, .3)',
    color: 'white'
  },
  marginLarge: {
    margin: 15
  },
  marginMedium: {
    margin: 10
  },
  marginSmall: {
    margin: 5
  },
  marginXSmall: {
    margin: 2
  },
  marginTopLarge: {
    marginTop: 15
  },
  marginTopMedium: {
    marginTop: 10
  },
  marginTopSmall: {
    marginTop: 5
  },
  marginTopXSmall: {
    marginTop: 2
  },
  marginBottomLarge: {
    marginBottom: 15
  },
  marginBottomMedium: {
    marginBottom: 10
  },
  marginBottomSmall: {
    marginBottom: 5
  },
  marginBottomXSmall: {
    marginBottom: 2
  },
  paddingLarge: {
    padding: 15
  },
  paddingMedium: {
    padding: 10
  },
  paddingSmall: {
    padding: 5
  },
  paddingXSmall: {
    padding: 2
  }
}))
