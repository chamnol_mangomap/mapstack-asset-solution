import React from 'react'
import ReactDOM from 'react-dom'
import ExampleApp from './App'
import './index.css'

ReactDOM.render(<ExampleApp />, document.getElementById('app'))
