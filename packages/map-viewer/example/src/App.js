import 'core-js/stable'
import 'regenerator-runtime/runtime'
import React from 'react'
import { Provider } from 'react-redux'
import { App, store } from 'map-viewer'

export default function ExampleApp() {
  const viewport = {
    width: '100wh',
    height: '100vh',
    latitude: 38.84609720557644,
    longitude: -100.09397174691415,
    zoom: 3.703849286134473
  }

  return (
    <div>
      <Provider store={store}>
        <App viewport={viewport} />
      </Provider>
    </div>
  )
}
