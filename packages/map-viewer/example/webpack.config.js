const path = require('path')
const webpack = require('webpack')
const HtmlWebPackPlugin = require('html-webpack-plugin')
const dotenv = require('dotenv')
const { log } = require('console')

// call dotenv and it will return an Object with a parsed key
const env = process.env

// reduce it to a nice object, the same as before
const envKeys = Object.keys(env).reduce((prev, next) => {
  prev[`process.env.${next}`] = JSON.stringify(env[next])
  return prev
}, {})

module.exports = {
  entry: '/src/index.js',
  output: {
    path: path.resolve(__dirname, 'dist')
  },
  devServer: {
    publicPath: '/',
    contentBase: './dist',
    hot: true,
    open: true,
    watchOptions: {
      ignored: /node_modules/,
      poll: 1000
    },
    port: 9000,
    historyApiFallback: true
  },
  resolve: {
    alias: {
      'mapbox-gl': 'maplibre-gl'
    }
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['@babel/preset-env', '@babel/preset-react']
          }
        }
      },
      {
        test: /\.css$/,
        include: path.resolve(__dirname, 'src'),
        use: ['style-loader', 'css-loader']
      }
    ]
  },
  plugins: [
    new webpack.DefinePlugin(envKeys),
    // fix "process is not defined" error:
    // (do "npm install process" before running the build)
    new webpack.ProvidePlugin({
      process: 'process/browser'
    }),
    new HtmlWebPackPlugin({
      template: './src/index.html'
    })
  ]
}
