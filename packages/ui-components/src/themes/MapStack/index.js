import { createTheme } from '@mui/material/styles'
import { green, blue, grey } from '@mui/material/colors'

export const MapStackTheme = createTheme({
  palette: {
    background: {
      default: '#333',
      paper: '#333'
    },
    text: {
      primary: '#ffffff'
    },
    primary: {
      main: green[500],
      contrastText: '#fff'
    },
    secondary: {
      main: grey[700],
      contrastText: '#fff'
    },
    light: {
      main: '#fff',
      contrastText: '#333'
    },
    success: {
      main: blue[500],
      contrastText: '#fff'
    }
  },
  components: {
    MuiCssBaseline: {
      styleOverrides: {
        body: {
          '&::-webkit-scrollbar, & *::-webkit-scrollbar': {
            width: '16px'
          },
          '&::-webkit-scrollbar-thumb, & *::-webkit-scrollbar-thumb': {
            border: '4px solid rgba(255, 255, 255, 0)',
            backgroundClip: 'padding-box',
            borderRadius: 99,
            backgroundColor: 'rgba(255, 255, 255, 0.3)'
          },
          '&::-webkit-scrollbar-thumb:focus, & *::-webkit-scrollbar-thumb:focus':
            {
              backgroundColor: 'rgba(255, 255, 255, 0.5)'
            },
          '&::-webkit-scrollbar-thumb:hover, & *::-webkit-scrollbar-thumb:hover':
            {
              backgroundColor: 'rgba(255, 255, 255, 0.8)'
            },
          '&::-webkit-scrollbar-thumb:active, & *::-webkit-scrollbar-thumb:active':
            {
              backgroundColor: 'rgba(255, 255, 255, 1)'
            },
          '&::-webkit-scrollbar-corner, & *::-webkit-scrollbar-corner': {
            backgroundColor: '#333'
          }
        }
      }
    }
  }
})

export const textFieldTheme = ({ theme }) => ({
  '& .MuiOutlinedInput-root': {
    backgroundColor: '#fff',
    color: '#333'
  }
})

export const NavigationButtonTheme = ({ theme }) => ({
  color: '#fff',
  '&.Mui-selected': {
    color: blue[500]
  }
})

/**
 * CHANGE LOG:
 * - added scroll style in MuiCssBaseline
 */
