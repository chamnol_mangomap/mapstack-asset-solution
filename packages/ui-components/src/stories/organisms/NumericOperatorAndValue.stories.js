import React from 'react'
import { NumericOperators } from 'molecules/NumericOperator'
import { TextInput } from 'atoms/TextInput'
import { Range } from 'atoms/Range'
import { NumericOperatorAndValue } from 'organisms/NumericOperatorAndValue'

export default {
  title: 'Organisms/NumericOperatorAndValue',
  component: NumericOperatorAndValue
}

const Template = (args) => <NumericOperatorAndValue {...args} />

export const Default = Template.bind({})
Default.args = {
  operator: <NumericOperators operator='>=' onOperatorSelected={() => {}} />,
  value: (
    <TextInput
      color='light'
      error={false}
      data-testid='number-value-input'
      type='text'
      name='value'
      onChange={() => {}}
    />
  ),
  range: <Range min={2} max={20} />
}
