import React from 'react'
import { StringOperators } from 'molecules/StringOperators'
import { ChipInput } from 'atoms/ChipInput'
import { StringOperatorAndValue } from 'organisms/StringOperatorAndValue'

export default {
  title: 'Organisms/StringOperatorAndValue',
  component: StringOperatorAndValue
}

const Template = (args) => <StringOperatorAndValue {...args} />

export const Default = Template.bind({})
Default.args = {
  operator: <StringOperators operator='==' onOperatorSelected={() => {}} />,
  value: (
    <ChipInput
      options={[
        'Alabama',
        'Arizona',
        'Arkansas',
        'California',
        'Colorado',
        'Connecticut',
        'Delaware',
        'District of Columbia',
        'Florida'
      ]}
      onChange={() => {}}
    />
  )
}
