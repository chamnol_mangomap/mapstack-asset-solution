import React from 'react'
import { FilterProps } from 'organisms/FilterProps'
import { PropChips } from 'molecules/PropChips'

export default {
  title: 'Organisms/FilterProps',
  component: FilterProps
}

const Template = (args) => <FilterProps {...args} />

export const Default = Template.bind({})
Default.args = {
  children: (
    <PropChips
      onPropertySelected={() => {}}
      property='Median age'
      schema={{
        County: {
          type: 'string'
        },
        'County id': {
          type: 'string'
        },
        Households: {
          type: 'number'
        },
        'Median age': {
          type: 'number'
        },
        'Median household value': {
          type: 'number'
        },
        'Median income': {
          type: 'number'
        },
        Population: {
          type: 'number'
        },
        State: {
          type: 'string'
        },
        'State code': {
          type: 'string'
        },
        'Unemployment rate': {
          type: 'number'
        }
      }}
    />
  )
}
