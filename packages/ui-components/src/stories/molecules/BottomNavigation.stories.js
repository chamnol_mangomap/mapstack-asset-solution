import React from 'react'
import { IconMap, IconList } from 'atoms/Icons'
import { BottomNavigation } from 'molecules/BottomNavigation'

export default {
  title: 'Molecules/BottomNavigation',
  component: BottomNavigation
}

const Template = (args) => {
  return <BottomNavigation {...args} />
}

export const Default = Template.bind({})
Default.args = {
  value: '100%',
  actions: [
    { value: 'maps', icon: <IconMap />, label: 'Maps' },
    { value: 'legend', icon: <IconList />, label: 'Legend' }
  ],
  onChange: console.log
}
