import React from 'react'
import { NumericOperators } from 'molecules/NumericOperator'

export default {
  title: 'Molecules/NumericOperator',
  component: NumericOperators
}

const Template = (args) => <NumericOperators {...args} />

export const Default = Template.bind({})
Default.args = {
  operator: '>'
}
