import React from 'react'
import { PropChips } from 'molecules/PropChips'

export default {
  title: 'Molecules/PropChips',
  component: PropChips
}

const Template = (args) => <PropChips {...args} />

export const Default = Template.bind({})
Default.args = {
  property: 'Median age',
  schema: {
    County: {
      type: 'string'
    },
    'County id': {
      type: 'string'
    },
    Population: {
      type: 'number'
    },
    'Median age': {
      type: 'number'
    },
    Households: {
      type: 'number'
    },
    'Median income': {
      type: 'number'
    },
    'Unemployment rate': {
      type: 'number'
    },
    'Median household value': {
      type: 'number'
    },
    'State code': {
      type: 'string'
    },
    State: {
      type: 'string'
    }
  }
}
