import React from 'react'
import { StringOperators } from 'molecules/StringOperators'

export default {
  title: 'Molecules/StringOperators',
  component: StringOperators
}

const Template = (args) => <StringOperators {...args} />

export const Default = Template.bind({})
Default.args = {
  operator: '=='
}
