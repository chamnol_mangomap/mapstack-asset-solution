import React from 'react'
import { Tabs } from 'molecules/Tabs'
import { Box } from 'atoms/Box'

export default {
  title: 'Molecules/Tabs',
  component: Tabs
}

const Template = (args) => {
  return (
    <Box sx={{ bgcolor: 'black' }}>
      <Tabs {...args} />
    </Box>
  )
}

export const Default = Template.bind({})
Default.args = {
  value: false,
  items: ['Totals', 'Avg.', 'Count', 'Table'].map((label, index) => ({
    label,
    id: `simple-tabpanel-${index}`,
    'aria-controls': `simple-tabpanel-${index}`
  })),
  onChange: () => {}
}
