import React from 'react'
import { FilterTable } from 'molecules/FilterTable'

export default {
  title: 'Molecules/FilterTable',
  component: FilterTable
}

const Template = (args) => {
  return <FilterTable {...args} />
}

export const Default = Template.bind({})
Default.args = {
  rows: [
    {
      id: 67,
      properties: {
        County: 'Santa Cruz County',
        'County id': '0500000US04023',
        Population: 46480,
        'Median age': 36.7,
        Households: 15853,
        'Median income': 41259,
        'Unemployment rate': 7.7,
        'Median household value': 151200,
        'State code': 'AZ',
        State: 'Arizona',
        uuid: 'de02aa61-5e7b-4b52-af20-9830a50b'
      }
    },
    {
      id: 68,
      properties: {
        County: 'Pinal County',
        'County id': '0500000US04021',
        Population: 432793,
        'Median age': 39.3,
        Households: 141300,
        'Median income': 58174,
        'Unemployment rate': 7.1,
        'Median household value': 183100,
        'State code': 'AZ',
        State: 'Arizona',
        uuid: '6b256310-2f4e-4eba-83bf-bf09204e'
      }
    },
    {
      id: 69,
      properties: {
        County: 'Navajo County',
        'County id': '0500000US04017',
        Population: 109270,
        'Median age': 37.3,
        Households: 34990,
        'Median income': 40067,
        'Unemployment rate': 12.7,
        'Median household value': 126100,
        'State code': 'AZ',
        State: 'Arizona',
        uuid: '9c45db7b-b27d-4ae6-90df-f040e9ac'
      }
    },
    {
      id: 70,
      properties: {
        County: 'Graham County',
        'County id': '0500000US04009',
        Population: 37996,
        'Median age': 33.6,
        Households: 11017,
        'Median income': 51353,
        'Unemployment rate': 8.3,
        'Median household value': 138700,
        'State code': 'AZ',
        State: 'Arizona',
        uuid: 'e00c7931-bfbc-43b2-beac-98de2068'
      }
    },
    {
      id: 71,
      properties: {
        County: 'Greenlee County',
        'County id': '0500000US04011',
        Population: 9522,
        'Median age': 34.5,
        Households: 3132,
        'Median income': 63473,
        'Unemployment rate': 5.7,
        'Median household value': 83800,
        'State code': 'AZ',
        State: 'Arizona',
        uuid: 'f3aa4ba6-0e66-4eca-8e1e-8c32554b'
      }
    },
    {
      id: 72,
      properties: {
        County: 'Coconino County',
        'County id': '0500000US04005',
        Population: 141274,
        'Median age': 30.7,
        Households: 47447,
        'Median income': 59460,
        'Unemployment rate': 7.6,
        'Median household value': 280900,
        'State code': 'AZ',
        State: 'Arizona',
        uuid: '730b5886-9759-44c3-b95e-7b6fc2c6'
      }
    },
    {
      id: 73,
      properties: {
        County: 'Maricopa County',
        'County id': '0500000US04013',
        Population: 4328810,
        'Median age': 36.4,
        Households: 1552096,
        'Median income': 64468,
        'Unemployment rate': 5,
        'Median household value': 260200,
        'State code': 'AZ',
        State: 'Arizona',
        uuid: '7c05a05e-9536-4460-899f-7ea3238e'
      }
    },
    {
      id: 74,
      properties: {
        County: 'Gila County',
        'County id': '0500000US04007',
        Population: 53546,
        'Median age': 50.2,
        Households: 21945,
        'Median income': 43524,
        'Unemployment rate': 8.8,
        'Median household value': 165800,
        'State code': 'AZ',
        State: 'Arizona',
        uuid: '6c9ca873-fd24-4a86-a985-f84b1190'
      }
    },
    {
      id: 75,
      properties: {
        County: 'Apache County',
        'County id': '0500000US04001',
        Population: 71511,
        'Median age': 35,
        Households: 20867,
        'Median income': 32508,
        'Unemployment rate': 10.5,
        'Median household value': 59900,
        'State code': 'AZ',
        State: 'Arizona',
        uuid: '15989fed-5587-4132-9735-c6208417'
      }
    },
    {
      id: 76,
      properties: {
        County: 'Mohave County',
        'County id': '0500000US04015',
        Population: 207695,
        'Median age': 51.6,
        Households: 86889,
        'Median income': 45587,
        'Unemployment rate': 8,
        'Median household value': 160500,
        'State code': 'AZ',
        State: 'Arizona',
        uuid: '07f5aef3-4cdf-4dbd-b92e-ab0fd668'
      }
    },
    {
      id: 77,
      properties: {
        County: 'Yuma County',
        'County id': '0500000US04027',
        Population: 209468,
        'Median age': 34.6,
        Households: 73098,
        'Median income': 45243,
        'Unemployment rate': 10,
        'Median household value': 127900,
        'State code': 'AZ',
        State: 'Arizona',
        uuid: '71ac0888-5adc-4b7a-b2be-256dcbfd'
      }
    }
  ].map((county) => ({
    ...county.properties,
    htmlId: `feature-row-${county.id}`
  })),
  columns: [
    'County',
    'County id',
    'Population',
    'Median age',
    'Households',
    'Median income',
    'Unemployment rate',
    'Median household value',
    'State code',
    'State',
    'uuid'
  ]
}

Default.decorators = [
  (Story) => (
    <div style={{ backgroundColor: 'rgb(51, 51, 51, 0.5)', padding: '30px' }}>
      <Story />
    </div>
  )
]
