import React from 'react'
import { SideBarHeader } from 'molecules/SideBarHeader'

export default {
  title: 'Molecules/SideBarHeader',
  component: SideBarHeader
}

const Template = (args) => (
  <div>
    <SideBarHeader {...args} />
  </div>
)

export const Default = Template.bind({})
Default.args = {}
