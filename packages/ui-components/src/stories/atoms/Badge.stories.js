import React from 'react'
import { Badge } from 'atoms/Badge'
import { IconAddCircleOutlined } from 'atoms/Icons'
import { ThemeProvider } from '@mui/styles'
import { MapStackTheme } from 'themes/MapStack'

export default {
  title: 'Atoms/Badge',
  component: Badge
}

const Template = (args) => (
  <ThemeProvider theme={MapStackTheme}>
    <Badge {...args}>
      <IconAddCircleOutlined color='secondary' />
    </Badge>
  </ThemeProvider>
)

export const Default = Template.bind({})
Default.args = {
  badgeContent: '100',
  color: 'primary'
}
