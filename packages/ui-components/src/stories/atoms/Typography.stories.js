import React from 'react'

import { Typography } from 'atoms/Typography'

export default {
  title: 'Atoms/Typography',
  component: Typography
}

const Template = (args) => <Typography {...args} />

/**
 * @type {{args: import('@mui/material/Typography/index').TypographyProps}}
 */
export const Default = Template.bind({})
Default.args = {
  size: 'medium',
  children: 'Hello Mango'
}
