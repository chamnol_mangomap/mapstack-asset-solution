import React from 'react'
import { AlertMsg } from 'atoms/AlertMsg'

export default {
  title: 'Atoms/AlertMsg',
  component: AlertMsg
}

const Template = (args) => <AlertMsg {...args} />

export const Default = Template.bind({})
Default.args = {
  severity: 'error',
  msg: 'This is error alert'
}

export const Warning = Template.bind({})
Warning.args = {
  severity: 'warning',
  msg: 'This is warning message'
}

export const Info = Template.bind({})
Info.args = {
  severity: 'info',
  msg: 'This is information message'
}

export const Success = Template.bind({})
Success.args = {
  severity: 'success',
  msg: 'This is success message'
}
