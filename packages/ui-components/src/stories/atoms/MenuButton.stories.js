import React from 'react'
import { MenuButton } from 'atoms/MenuButton'

export default {
  title: 'Atoms/MenuButton',
  component: MenuButton
}

const Template = (args) => <MenuButton {...args} />

export const Default = Template.bind({})
Default.args = {}
