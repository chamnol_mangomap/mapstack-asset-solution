import React from 'react'
import { Drawer, Box } from 'atoms'

export default {
  title: 'Atoms/Drawer',
  component: Drawer
}

const Template = (args) => {
  return (
    <Drawer {...args}>
      <Box>Hey there i'm a drawer</Box>
    </Drawer>
  )
}

export const Default = Template.bind({})
Default.args = {
  onOpen: () => {},
  onClose: () => {},
  open: true,
  anchor: 'right'
}
