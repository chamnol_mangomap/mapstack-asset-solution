import React from 'react';
import { IconAddCircleOutlined } from 'ui-components';

export default {
  title: 'Atoms/Icons',
  component: IconAddCircleOutlined,
};

const Template = (args) => <IconAddCircleOutlined {...args} />;

export const Default = Template.bind({});
Default.args = {};
