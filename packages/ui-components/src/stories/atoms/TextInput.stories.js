import React from 'react'

import { TextInput } from 'atoms/TextInput'

export default {
  title: 'Atoms/TextInput',
  component: TextInput
}

const Template = (args) => <TextInput {...args} />

export const Default = Template.bind({})
Default.args = {
  size: 'medium'
}
