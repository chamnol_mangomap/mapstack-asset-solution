import React from 'react'

import { RegularButton } from 'atoms/RegularButton'

export default {
  title: 'Atoms/RegularButton',
  component: RegularButton
}

const Template = (args) => (
  <RegularButton {...args}>Regular Button</RegularButton>
)

export const Default = Template.bind({})
Default.args = {
  variant: 'contained',
  size: 'medium'
}
