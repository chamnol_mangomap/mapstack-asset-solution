import React from 'react'
import { Chip } from 'atoms/Chip'
import { IconAddCircleOutlined } from 'atoms/Icons'

export default {
  title: 'Atoms/Chip',
  component: Chip
}

const Template = (args) => <Chip {...args} />

export const Default = Template.bind({})
Default.args = {
  label: 'Chip',
  color: 'primary'
}

export const AddChip = Template.bind({})
AddChip.args = {
  variant: 'contained',
  color: 'secondary',
  label: 'Add Chip',
  onDelete: () => {},
  deleteIcon: <IconAddCircleOutlined />
}
