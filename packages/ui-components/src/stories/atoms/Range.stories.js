import React from 'react'
import { Range } from 'atoms/Range'

export default {
  title: 'Atoms/Range',
  component: Range
}

const Template = (args) => <Range {...args} />

export const Default = Template.bind({})
Default.args = {
  min: 10,
  max: 100
}
