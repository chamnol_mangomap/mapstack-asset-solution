import React from 'react'

import { Box } from 'atoms/Box'

export default {
  title: 'Atoms/Box',
  component: Box
}

const Template = (args) => <Box {...args} />

/**
 * @type {{args: import('@mui/material/Box').BoxProps}}
 */
export const Default = Template.bind({})
Default.args = {
  sx: {
    width: 500,
    height: 500,
    borderColor: 'lightgrey',
    borderWidth: 1,
    borderStyle: 'dashed'
  }
}
