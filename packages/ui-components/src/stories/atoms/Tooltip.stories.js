import React from 'react'

import { Tooltip, Typography, Box } from 'atoms'

export default {
  title: 'Atoms/Tooltip',
  component: Tooltip
}

const Template = (args) => {
  const { text, tooltip } = args
  return (
    <Tooltip {...tooltip}>
      <Box
        sx={{
          p: 2,
          width: 'fit-content',
          borderStyle: 'solid',
          borderColor: 'lightgrey',
          borderWidth: 2,
          borderRadius: 8,
          cursor: 'pointer',
          userSelect: 'none'
        }}
      >
        <Typography
          variant='button'
          sx={{
            fontWeight: 'bold'
          }}
        >
          {text}
        </Typography>
      </Box>
    </Tooltip>
  )
}

export const Default = Template.bind({})

/**
 * @type {{args: {tooltip: import('@mui/material/Tooltip').TooltipProps}}}
 */
Default.args = {
  ref: Object
}
