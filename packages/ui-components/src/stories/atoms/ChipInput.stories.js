import React from 'react'
import { ChipInput } from 'atoms/ChipInput'

export default {
  title: 'Atoms/ChipInput',
  component: ChipInput
}

const Template = (args) => <ChipInput {...args} />

export const Default = Template.bind({})
Default.args = {
  controlledState: false,
  onChange: () => {},
  options: [
    'Alabama',
    'Arizona',
    'Arkansas',
    'California',
    'Colorado',
    'Connecticut',
    'Delaware',
    'District of Columbia',
    'Florida'
  ]
}

export const Secondary = Template.bind({})
Secondary.args = {
  controlledState: true,
  value: ['Alabama'],
  onChange: () => {},
  options: [
    'Alabama',
    'Arizona',
    'Arkansas',
    'California',
    'Colorado',
    'Connecticut',
    'Delaware',
    'District of Columbia',
    'Florida'
  ]
}
