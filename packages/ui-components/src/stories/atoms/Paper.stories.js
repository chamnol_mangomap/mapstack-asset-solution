import React from 'react'

import { Paper } from 'atoms/Paper'

export default {
  title: 'Atoms/Paper',
  component: Paper
}

const Template = (args) => <Paper {...args} />

/**
 * @type {{args: import('@mui/material/Paper').PaperProps}}
 */
export const Default = Template.bind({})
Default.args = {}
