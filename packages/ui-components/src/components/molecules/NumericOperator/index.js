import React from 'react'
import PropTypes from 'prop-types'
import { Chip } from 'atoms/Chip'

const operators = [
  {
    display: 'Greater than',
    value: '>'
  },
  { display: 'Greater than or equal to', value: '>=' },
  {
    display: 'Less than',
    value: '<'
  },
  {
    display: 'Less than or equal to',
    value: '<='
  },
  {
    display: 'Equal to',
    value: '=='
  }
]

/**
 * Show list of numeric operators
 */
export const NumericOperators = (props) => {
  const { operator, onOperatorSelected } = props
  return (
    <React.Fragment>
      {operators.map((operatorObj, key) => (
        <div key={key} style={{ margin: 5, display: 'inline-block' }}>
          <Chip
            data-testid={`number-operator-${operatorObj.display}`}
            color={operatorObj.value === operator ? 'success' : 'secondary'}
            label={operatorObj.display}
            onClick={() => onOperatorSelected(operatorObj.value)}
          />
        </div>
      ))}
    </React.Fragment>
  )
}

NumericOperators.propTypes = {
  /**
   * An operator one of the followings
   */
  operator: PropTypes.oneOf(['>', '<', '<=', '>=', '==', '']),
  /**
   * A callback function when an operator is selected
   */
  onOperatorSelected: PropTypes.func
}

NumericOperators.defaultProps = {
  operator: '>'
}
