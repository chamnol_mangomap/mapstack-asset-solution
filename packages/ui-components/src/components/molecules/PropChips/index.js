import React from 'react'
import { Chip, Box } from 'atoms'
import PropTypes from 'prop-types'
import _ from 'lodash'

/**
 * Show a list of property chips
 */
export const PropChips = (props) => {
  // Props
  const { schema, property, direction } = props

  // Callbacks
  const { onPropertySelected } = props
  return (
    <Box
      sx={{
        display: direction === 'column' ? 'flex' : 'block',
        flexDirection: direction
      }}
    >
      {_.map(schema, (val, key) => {
        return (
          <div key={key} style={{ margin: 5, display: 'inline-block' }}>
            <Chip
              data-testid={`property-${key}`}
              color={key === property ? 'success' : 'secondary'}
              label={key}
              onClick={() => onPropertySelected(val.type, key)}
            />
          </div>
        )
      })}
    </Box>
  )
}

PropChips.propTypes = {
  /**
   * A schema contains list of properties and it's type
   */
  schema: PropTypes.object,
  /**
   * A property to be selected
   */
  property: PropTypes.string,
  /**
   * A function for handling when property is selected
   */
  onPropertySelected: PropTypes.func,
  /**
   * Direction for display chip options
   */
  direction: PropTypes.oneOf(['row', 'column'])
}

PropChips.defaultProps = {
  direction: 'row'
}
