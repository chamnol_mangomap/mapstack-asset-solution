import React from 'react'
import PropTypes from 'prop-types'
import { Chip } from 'atoms/Chip'

const operators = [
  {
    display: 'Is',
    value: '=='
  },
  {
    display: 'Is not',
    value: '!='
  }
]

/**
 * Show list of string operators
 */
export const StringOperators = (props) => {
  // Props
  const { operator } = props

  // Callbacks
  const { onOperatorSelected } = props

  return (
    <React.Fragment>
      {operators.map((operatorObj, key) => (
        <div key={key} style={{ margin: 5, display: 'inline-block' }}>
          <Chip
            data-testid={`string-operator-${operatorObj.display}`}
            color={operatorObj.value === operator ? 'success' : 'secondary'}
            label={operatorObj.display}
            onClick={() => onOperatorSelected(operatorObj.value)}
          />
        </div>
      ))}
    </React.Fragment>
  )
}

StringOperators.propTypes = {
  /**
   * An operator is one of the followings
   */
  operator: PropTypes.oneOf(['!=', '==', '']),
  /**
   * A callback function when an operator is selected
   */
  onOperatorSelected: PropTypes.func
}
