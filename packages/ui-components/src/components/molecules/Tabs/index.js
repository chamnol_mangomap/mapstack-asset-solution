import React from 'react'
import PropTypes from 'prop-types'
import { Tabs as MUITabs, Tab } from '@mui/material'
import { styled } from '@mui/styles'
import { NavigationButtonTheme } from 'themes/MapStack'

/**
 * Tabs organize and allow navigation between groups of content that are related and at the same level of hierarchy.
 */

const StyledTab = styled((props) => {
  return <Tab {...props} />
})(NavigationButtonTheme)

export const Tabs = ({ items, ...props }) => {
  return (
    <MUITabs {...props}>
      {items.map((i, index) => (
        <StyledTab key={index} {...i} />
      ))}
    </MUITabs>
  )
}

Tabs.propTypes = {
  /**
   * Define the active index of tab (0,1,..)
   */
  value: PropTypes.any,
  /**
   * Array of tab children
   */
  items: PropTypes.array,
  /**
   * Callback when the tab value is changed
   */
  onChange: PropTypes.func
}

Tabs.defaultProps = {
  value: false
}
