import React from 'react'
import PropTypes from 'prop-types'
import {
  BottomNavigation as MuiBottomNavigation,
  BottomNavigationAction,
  Paper
} from '@mui/material'
import { styled } from '@mui/material/styles'
import { NavigationButtonTheme } from 'themes/MapStack'

const StyledBottomNavigationAction = styled(
  ({ icon, containerComponent: Container, ...rest }) => {
    if (Container)
      return <BottomNavigationAction icon={<Container />} {...rest} />

    return <BottomNavigationAction {...rest} icon={icon} />
  }
)(NavigationButtonTheme)

export const BottomNavigation = (props) => {
  const { value, actions } = props
  const { onChange } = props

  const _handleOnChange = (_, newValue) => {
    onChange(newValue)
  }

  return (
    <Paper
      sx={{ position: 'fixed', bottom: 0, left: 0, right: 0 }}
      elevation={3}
    >
      <MuiBottomNavigation
        color='light'
        showLabels
        value={value}
        onChange={_handleOnChange}
      >
        {actions.map((props) => {
          return (
            <StyledBottomNavigationAction
              key={props.label}
              {...props}
              style={{ color: `${props.disabled ? 'hsl(240deg 8% 46%)' : ''}` }}
            />
          )
        })}
      </MuiBottomNavigation>
    </Paper>
  )
}

BottomNavigation.propTypes = {
  /**
   * The width of the table, default to '100%'
   */
  value: PropTypes.string,
  /**
   * Array of actions items
   */
  actions: PropTypes.array,
  /**
   * Callback when action is changed
   */
  onChange: PropTypes.func
}

/**
 * Warning Issue: Warning: Function components cannot be given refs.
 * Read more: https://mui.com/guides/composition/#caveat-with-refs
 */
