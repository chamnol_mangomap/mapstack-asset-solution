import React from 'react'
import PropTypes from 'prop-types'
import { Paper } from '@mui/material'
import { DataGridPro as MuiDataGrid } from '@mui/x-data-grid-pro'

export const FilterTable = (props) => {
  const [page, setPage] = React.useState(25)
  // props
  const { rows, columns, sx } = props

  // callback
  const { onRowClick } = props

  return (
    <Paper sx={{ ...sx, overflow: 'hidden', width: '100%', height: '100%' }}>
      {rows.length > 0 ? (
        <MuiDataGrid
          onPageSizeChange={(val) => setPage(val)}
          pageSize={page}
          rowsPerPageOptions={[25, 50, 100]}
          pagination
          disableColumnMenu
          sx={(theme) => ({
            height: '100%', // fill the parent container
            '& .MuiDataGrid-sortIcon': {
              color: theme.palette.getContrastText(
                theme.palette.background.paper
              )
            }
          })}
          getRowId={(row) => row.htmlId}
          rows={rows}
          columns={columns}
          aria-label='Filter results table'
          onRowClick={onRowClick}
          componentsProps={{
            pagination: {
              labelRowsPerPage: 'Rows',
              sx: {
                overflow: 'hidden'
              }
            }
          }}
        />
      ) : (
        <div>No data to show</div>
      )}
    </Paper>
  )
}

FilterTable.propTypes = {
  /**
   * The system prop that allows defining system overrides as well as
   * additional CSS styles.
   */
  sx: PropTypes.object,
  /**
   * The array of data to show
   */
  rows: PropTypes.array,
  /**
   * The array of headers to show
   */
  columns: PropTypes.arrayOf(PropTypes.object),
  /**
   * A callback function when a feature (row) is clicked
   */
  onRowClick: PropTypes.func
}

FilterTable.defaultProps = {
  sx: {
    width: '100%',
    height: '100%'
  }
}

/**
 * Reference
 * [Custom Label `Row per page`]: https://github.com/mui/mui-x/issues/1854
 */
