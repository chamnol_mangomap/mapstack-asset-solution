import React from 'react'
import PropTypes from 'prop-types'
import Box from '@mui/material/Box'
import CloseIcon from '@mui/icons-material/Close'

function Item(props) {
  const { sx, ...rest } = props
  return (
    <Box
      sx={{
        p: '0px 15px 15px 15px',
        borderRadius: 1,
        fontSize: '1rem',
        fontWeight: '700',
        ...sx
      }}
      {...rest}
    />
  )
}

/**
 * Header of side bar which contains a close icon and clear filter text button
 */
export const SideBarHeader = (props) => {
  // Props
  const { title } = props

  // Callbacks
  const { onClearAllFilter, onClose } = props

  return (
    <Box sx={{ display: 'flex' }}>
      <Box sx={{ top: 10, position: title ? 'absolute' : 'static' }}>
        <CloseIcon sx={{ m: '10px', cursor: 'pointer' }} onClick={onClose} />
      </Box>
      {title && (
        <Box
          sx={{
            flexGrow: '1',
            textAlign: 'center',
            p: '15px',
            mt: '10px',
            fontSize: '1rem',
            fontWeight: '700'
          }}
        >
          {title}
        </Box>
      )}
      {onClearAllFilter && (
        <Item
          data-testid='clear-filter'
          onClick={onClearAllFilter}
          sx={{
            flexGrow: '1',
            textAlign: 'right',
            pt: '15px',
            fontSize: '1rem',
            fontWeight: '700'
          }}
        >
          Clear All
        </Item>
      )}
    </Box>
  )
}

SideBarHeader.propTypes = {
  /**
   * A callback fired when close icon is clicked
   */
  onClose: PropTypes.func,
  /**
   * A callback fired when clease filter text is clicked
   */
  onClearAllFilter: PropTypes.func,
  /**
   * A title for header (optional)
   */
  title: PropTypes.string
}
