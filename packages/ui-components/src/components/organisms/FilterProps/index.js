import React from 'react'
import PropTypes from 'prop-types'
/**
 * An organism that contains a header and a molucule of PropChips
 */
export const FilterProps = (props) => {
  return (
    <React.Fragment>
      <h4 style={{ marginTop: 0 }}>Property</h4>
      {props.children}
    </React.Fragment>
  )
}

FilterProps.propTypes = {
  /**
   * A PropChips component (ie. PropChips)
   */
  children: PropTypes.element
}
