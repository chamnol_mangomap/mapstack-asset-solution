import React from 'react'
import PropTypes from 'prop-types'

/**
 * A organism that contains a Header, a StringOperator molecule and a TextInput atom
 */
export const StringOperatorAndValue = (props) => {
  const { operator, value } = props
  return (
    <React.Fragment>
      <h4>Operator</h4>
      {operator}
      <h4>Value:</h4>
      <div style={{ padding: '5px' }}>{value}</div>
    </React.Fragment>
  )
}

StringOperatorAndValue.propTypes = {
  /**
   * A StringOperator type molecule
   */
  operator: PropTypes.element,
  /**
   * A ChipInput type atom
   */
  value: PropTypes.element
}
