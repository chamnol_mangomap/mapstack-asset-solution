import React from 'react'
import PropTypes from 'prop-types'

/**
 * A organism that contains a Header, NumericOperators molecule, TextInput atom and a Range atom
 */
export const NumericOperatorAndValue = (props) => {
  const { operator, value, range } = props
  return (
    <React.Fragment>
      <h4>Operator</h4>
      {operator}
      <h4>Value:</h4>
      <div style={{ padding: '0px 5px' }}>
        {value}
        {range}
      </div>
    </React.Fragment>
  )
}

NumericOperatorAndValue.propTypes = {
  /**
   * A NumericOperators type molecule
   */
  operator: PropTypes.element,
  /**
   * A TextInput type atom
   */
  value: PropTypes.element,
  /**
   * A Range type atom
   */
  range: PropTypes.element
}
