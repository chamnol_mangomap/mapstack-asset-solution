import React from 'react'
import { Box as MuiBox } from '@mui/material'

/**
 * A wrapper that you can group other component
 * reference: https://mui.com/components/box/
 */
export function Box(props) {
  return <MuiBox {...props} />
}
