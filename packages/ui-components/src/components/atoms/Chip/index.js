import React from 'react'
import PropTypes from 'prop-types'
import { Chip as MuiChip } from '@mui/material'

/**
 * Chips are compact elements that represent an input, attribute, or action.
 * This component is a wrapper of muiChip, for details check out at https://mui.com/api/chip/
 */
export function Chip(props) {
  return <MuiChip {...props} />
}

Chip.propTypes = {
  /**
   * Support color one of the following:
   * 'default' | 'primary' | 'secondary' | 'error' | 'info' | 'success' | 'warning' | string
   */
  color: PropTypes.oneOf([
    'default',
    'primary',
    'secondary',
    'error',
    'info',
    'success',
    'warning'
  ]),
  /**
   * Support one of the following:
   * 'filled' | 'outlined' | string
   */
  variant: PropTypes.oneOf(['default', 'filled', 'outlined', 'contained'])
}

Chip.defaultProps = {
  color: 'default',
  variant: 'contained'
}
