import React from 'react'
import { Tooltip as MuiTooltip, ClickAwayListener } from '@mui/material'

/**
 * Tooltips display informative text when users hover over, focus on, or tap an element.<br/>
 * Reference: https://mui.com/components/tooltips/
 */
export const Tooltip = (props) => {
  const { children, isClickAwayListener, onClose, ...rest } = props
  if (isClickAwayListener) {
    return (
      <ClickAwayListener onClickAway={onClose}>
        <div>
          <Tooltip
            onClose={onClose}
            disableFocusListener
            disableHoverListener
            disableTouchListener
            {...rest}
          >
            <div>{children}</div>
          </Tooltip>
        </div>
      </ClickAwayListener>
    )
  }
  return (
    <MuiTooltip onClose={onClose} {...rest}>
      {children}
    </MuiTooltip>
  )
}

Tooltip.defaultProps = {
  PopperProps: {
    disablePortal: true
  },
  placement: 'top',
  arrow: true
}
