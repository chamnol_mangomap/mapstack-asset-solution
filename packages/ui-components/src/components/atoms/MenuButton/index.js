import React from 'react'
import Fab from '@mui/material/Fab'
import MenuIcon from '@mui/icons-material/Menu'
import PropTypes from 'prop-types'

/**
 * A floating action button (FAB) performs the primary, or most common, action on a screen.
 * For detail props check https://mui.com/components/floating-action-button/
 */
export const MenuButton = (props) => {
  return (
    <Fab {...props}>
      <MenuIcon />
    </Fab>
  )
}

MenuButton.propTypes = {
  /**
   * The color of the component. It supports those theme colors that make sense for this component.
   */
  color: PropTypes.oneOf(['primary', 'secondary', 'light']),
  /**
   * A callback when the button is clicked
   */
  onClick: PropTypes.func
}

MenuButton.defaultProps = {
  color: 'primary'
}
