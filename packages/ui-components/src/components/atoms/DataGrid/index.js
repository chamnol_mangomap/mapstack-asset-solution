import React from 'react'
import PropTypes from 'prop-types'
import { DataGridPro as MuiDataGrid, LicenseInfo } from '@mui/x-data-grid-pro'

// FIXME: add the key to different file or env variable
LicenseInfo.setLicenseKey(
  '54b15a57982fcfd357f997b69b2e34f7T1JERVI6Mzk0NzQsRVhQSVJZPTE2Nzg2MTEzNDAwMDAsS0VZVkVSU0lPTj0x'
)

/**
 * Two dimensional, row and column table for data viewing.
 * Documentation: https://mui.com/api/data-grid/data-grid/
 */
export const DataGrid = (props) => {
  return <MuiDataGrid {...props} />
}

DataGrid.propTypes = {
  /**
   * Set of columns of type GridColumns.
   */
  rows: PropTypes.array.isRequired,
  /**
   * Set of rows of type GridRowsProp.
   */
  columns: PropTypes.array.isRequired
}

const columns = [
  {
    field: 'Column 1',
    headerName: 'Column 1'
  },
  {
    field: 'Column 2',
    headerName: 'Column 2'
  },
  {
    field: 'Column 3',
    headerName: 'Column 3'
  }
]

const rows = [
  {
    id: 1,
    'Column 1': 'Row 1',
    'Column 2': 'Row 1',
    'Column 3': 'Row 1'
  },
  {
    id: 2,
    'Column 1': 'Row 2',
    'Column 2': 'Row 2',
    'Column 3': 'Row 2'
  },
  {
    id: 3,
    'Column 1': 'Row 3',
    'Column 2': 'Row 3',
    'Column 3': 'Row 3'
  }
]

DataGrid.defaultProps = {
  rows,
  columns
}
