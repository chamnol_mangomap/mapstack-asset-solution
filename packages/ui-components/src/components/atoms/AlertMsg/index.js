import React from 'react'
import PropTypes from 'prop-types'
import Alert from '@mui/material/Alert'

/**
 * AlertMsg displays a short, important message in a way that attracts the user's attention without interrupting the user's task.
 */
export const AlertMsg = (props) => {
  const { msg } = props
  return (
    <Alert style={{ marginTop: 15 }} {...props}>
      {msg}
    </Alert>
  )
}

AlertMsg.propTypes = {
  /**
   * Select one of the following to indicate the important of the message
   * 'error'| 'info'| 'success'| 'warning'
   */
  severity: PropTypes.oneOf(['error', 'warning', 'info', 'success']),
  /**
   * The message
   */
  msg: PropTypes.string.isRequired
}

AlertMsg.defaultProps = {
  severity: 'success'
}
