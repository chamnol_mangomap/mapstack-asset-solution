import * as React from 'react'
import Chip from '@mui/material/Chip'
import Autocomplete from '@mui/material/Autocomplete'
import PropTypes from 'prop-types'
import { TextInput } from 'atoms/TextInput'

/**
 * This component provides an autocomplete chip input field, It is an extended of MUI Autocomplete component.
 * For details of of MUI Autocomplete checking https://mui.com/components/autocomplete/
 */
export const ChipInput = (props) => {
  const autocompleteProps =
    props.value && props.controlledState ? { value: props.value } : {}
  return (
    <div style={{ minWidth: '150px' }}>
      <Autocomplete
        {...autocompleteProps}
        data-testid='auto-complete-chip'
        color='light'
        multiple
        onChange={(_, newValue) => {
          props.onChange(newValue.filter((value) => value))
        }}
        options={props.options}
        getOptionLabel={(option) => option}
        renderTags={(value, getTagProps) =>
          value.map(
            (option, index) =>
              option && (
                <Chip
                  key={index}
                  data-testid={`chip-value-${index}`}
                  color='success'
                  label={option}
                  {...getTagProps({ index })}
                />
              )
          )
        }
        renderInput={(params) => (
          <TextInput color='primary' {...params} variant='outlined' />
        )}
      />
    </div>
  )
}

ChipInput.propTypes = {
  /**
   * Resolve to true, the user of this component has to control the state of prop 'value'
   * and if resolve to false, the component will control its own state of prop 'value'
   */
  controlledState: PropTypes.bool,
  /**
   * A array of default values, only applicable if prop 'controlledState' resolve to true
   */
  value: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.string),
    PropTypes.string
  ]),
  /**
   * Array of options. Mainly array of strings
   */
  options: PropTypes.arrayOf(PropTypes.string),
  /**
	 * Callback fired when the value changes.
			Signature:
			function(event, value).
			event: The event source of the callback.
			value: The new value of the component.
	 */
  onChange: PropTypes.func
}

ChipInput.defaultProps = {
  controlledState: false,
  options: [],
  onChange: function () {}
}
