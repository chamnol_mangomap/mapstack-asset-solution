import React from 'react'
import {
  AddCircleOutlined,
  List,
  Map,
  FilterAlt,
  TableView,
  Close,
  Share
} from '@mui/icons-material'

const IconAddCircleOutlined = (props) => {
  return <AddCircleOutlined {...props} />
}

const IconMap = (props) => {
  return <Map {...props} />
}

const IconList = (props) => {
  return <List {...props} />
}

const IconFilterAlt = (props) => {
  return <FilterAlt {...props} />
}

const IconTableView = (props) => {
  return <TableView {...props} />
}

const IconClose = (props) => {
  return <Close {...props} />
}

const IconShare = (props) => {
  return <Share {...props} />
}

export {
  IconAddCircleOutlined,
  IconMap,
  IconList,
  IconFilterAlt,
  IconTableView,
  IconClose,
  IconShare
}
