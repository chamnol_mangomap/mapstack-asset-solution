import React from 'react'
import TextField from '@mui/material/TextField'
import { styled } from '@mui/material/styles'
import PropTypes from 'prop-types'
import { textFieldTheme } from 'themes/MapStack'

const StyledInput = styled((props) => {
  return <TextField {...props} />
})(textFieldTheme)

/**
 * A text input. This component is a wrapper of mui TextField. For details props check https://mui.com/components/text-fields/
 */
export const TextInput = (props) => <StyledInput {...props} />

TextInput.propTypes = {
  /**
   * The size of textInput varies base on the follow value 'medium'| 'small'| string
   */
  size: PropTypes.oneOf(['small', 'medium'])
}

TextInput.defaultProps = {
  size: 'small'
}
