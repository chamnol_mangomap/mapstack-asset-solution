import React from 'react'
import PropTypes from 'prop-types'
import { makeStyles } from '@mui/styles'
import classNames from 'classnames'

const useStyles = makeStyles({
  typography: {
    margin: '0px 0px 0.35em',
    fontWeight: '400',
    lineHeight: '1.57'
  },
  medium: {
    fontSize: '0.8125rem'
  },
  small: {
    fontSize: '0.75rem'
  },
  large: {
    fontSize: '1rem'
  },
  bold: {
    fontWeight: '700'
  },
  overline: {
    textTransform: 'uppercase'
  }
})

/**
 * Basic Typography for mapstack
 */
export const Typography = (props) => {
  const classes = useStyles()
  const { children, size, bold, overline, ...rest } = props
  const btnClasses = classNames({
    [classes.typography]: true,
    [classes[size]]: size,
    [classes.bold]: bold,
    [classes.overline]: overline
  })
  return (
    <div className={btnClasses} {...rest}>
      {children}
    </div>
  )
}

Typography.propTypes = {
  /**
   * Typography size consists 'small' , 'medium' and 'large'
   */
  size: PropTypes.oneOf(['small', 'medium', 'large']),
  /**
   * Typography boolean to make text bolder
   */
  bold: PropTypes.bool,
  /**
   * Typography boolean to make text uppercase
   */
  overline: PropTypes.bool
}

Typography.defaultProps = {
  size: 'medium'
}
