import React from 'react'
import PropTypes from 'prop-types'
import { Badge as MUIBadge } from '@mui/material'

/**
 * Badge attached itself to a child element to display information
 */

export const Badge = ({ children, ...props }) => {
  return <MUIBadge {...props}>{children}</MUIBadge>
}

Badge.propTypes = {
  /**
   * Define the color for the badge
   */
  color: PropTypes.oneOf([
    'primary',
    'secondary',
    'error',
    'warning',
    'info',
    'success'
  ]),
  /**
   * Content to display inside the badge
   */
  badgeContent: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  /**
   * Children to attached the badge to
   */
  children: PropTypes.node.isRequired
}

Badge.defaultProps = {
  color: 'primary'
}
