import React, { useCallback } from 'react'
import MUIDrawer from '@mui/material/SwipeableDrawer'
import PropTypes from 'prop-types'
import { get, set } from 'idb-keyval'

import { Box } from '../Box'

const minDrawerWidth = 480

export function Drawer({ children, ...props }) {
  const [drawerWidth, setDrawerWidth] = React.useState(minDrawerWidth)

  React.useEffect(() => {
    get('drawer-width').then((val) => setDrawerWidth(val || minDrawerWidth))
  }, [])

  const handleMouseDown = () => {
    document.addEventListener('touchmove', handleMouseMove, true)
    document.addEventListener('touchend', handleMouseUp, true)

    document.addEventListener('mouseup', handleMouseUp, true)
    document.addEventListener('mousemove', handleMouseMove, true)
  }

  const handleMouseUp = () => {
    document.removeEventListener('mouseup', handleMouseUp, true)
    document.removeEventListener('mousemove', handleMouseMove, true)

    document.removeEventListener('touchend', handleMouseUp, true)
    document.removeEventListener('touchmove', handleMouseMove, true)
  }

  const handleMouseMove = useCallback((e) => {
    const { clientX } = (e.touches?.length && e.touches[0]) || e
    const newWidth =
      document.body.offsetWidth - (clientX - document.body.offsetLeft)

    if (newWidth > minDrawerWidth) {
      setDrawerWidth(newWidth)
      set('drawer-width', newWidth)
    }
  }, [])

  return (
    <MUIDrawer
      {...props}
      PaperProps={{ sx: { width: drawerWidth, maxWidth: { xs: 1, sm: 0.9 } } }}
    >
      <Box
        onTouchStart={handleMouseDown}
        onMouseDown={handleMouseDown}
        sx={(theme) => ({
          width: '4px',
          cursor: 'ew-resize',
          padding: '4px 0 0',
          position: 'absolute',
          top: 0,
          left: 0,
          bottom: 0,
          zIndex: 100,
          transition: 'all 0.5s',
          bgcolor: theme.palette.text.disabled,
          '&:hover': {
            bgcolor: theme.palette.grey['A100']
          }
        })}
      />
      {children}
    </MUIDrawer>
  )
}

Drawer.propTypes = {
  /**
   * from which direction to open up the drawer
   * 'right'| 'left'| 'bottom'| 'top'
   */
  anchor: PropTypes.oneOf(['right', 'left', 'bottom', 'top']),
  /**
   * Elements to render inside the drawer
   */
  children: PropTypes.node.isRequired,
  /**
   * Callback fired when drawer is open
   */
  onOpen: PropTypes.func,
  /**
   * Callback fired when drawer is closed
   */
  onClose: PropTypes.func
}

Drawer.defaultProps = {
  onOpen: () => {},
  onClose: () => {},
  open: false,
  anchor: 'right'
}
