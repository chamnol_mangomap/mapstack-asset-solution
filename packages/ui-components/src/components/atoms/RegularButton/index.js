import React from 'react'
import Button from '@mui/material/Button'
import PropTypes from 'prop-types'
import { makeStyles } from '@mui/styles'
import classNames from 'classnames'

const useStyles = makeStyles({
  button: {
    minHeight: '2.5rem'
  },
  block: {
    width: '100% !important'
  },
  round: {
    borderRadius: '30px'
  }
})

/**
 * Basic button, this component is a wrapper of mui button.
 * For details doc check https://mui.com/components/buttons/
 */
export const RegularButton = (props) => {
  const classes = useStyles()
  const { children, block, round, ...rest } = props
  const btnClasses = classNames({
    [classes.button]: true,
    [classes.block]: block,
    [classes.round]: round
  })
  return (
    <Button className={btnClasses} {...rest}>
      {children}
    </Button>
  )
}

RegularButton.propTypes = {
  /**
   * The Button comes with three variants: text (default), contained, and outlined.
   *
   */
  variant: PropTypes.oneOf(['default', 'contained', 'outlined', 'text']),
  /**
   * Button size consists 'small' , 'medium' and 'large'
   */
  size: PropTypes.oneOf(['small', 'medium', 'large'])
}

RegularButton.defaultProps = {
  variant: 'contained',
  size: 'medium'
}
