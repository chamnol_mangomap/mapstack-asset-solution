import React from 'react'
import PropTypes from 'prop-types'

/**
 * Display the range of a minimum and maximum
 */
export const Range = (props) => {
  const { min, max } = props
  return (
    <div style={{ marginTop: 10 }} data-testid='number-value-range'>
      Range: {min} - {max}
    </div>
  )
}

Range.propTypes = {
  /**
   * The minimum of range
   */
  min: PropTypes.number,
  /**
   * The maximum of range
   */
  max: PropTypes.number
}
