import React from 'react'
import PropTypes from 'prop-types'
import { Paper as MuiPaper } from '@mui/material'

/**
 * A wrapper that you can group other component and
 * apply elevation to establish a hierarchy between other content.<br/>
 * Reference: https://mui.com/components/paper/
 */
export function Paper(props) {
  return <MuiPaper {...props} />
}

Paper.propTypes = {
  /**
   * The variant of the paper
   */
  variant: PropTypes.oneOf(['elevation', 'outlined', PropTypes.string]),
  /**
   * Shadow depth, corresponds to dp in the spec. It accepts values between 0
   * and 24 inclusive.
   */
  elevation: PropTypes.number
}

Paper.defaultProps = {
  elevation: 1,
  variant: 'elevation'
}
