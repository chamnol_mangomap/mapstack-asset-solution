module.exports = {
  plugins: [
    [
      'module-resolver',
      {
        root: ['./src/'],
        alias: {
          atoms: './src/components/atoms',
          molecules: './src/components/molecules',
          organisms: './src/components/organisms',
          themes: './src/themes'
        }
      }
    ]
  ]
}
