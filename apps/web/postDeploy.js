'use strict';

let jsonData = require('./.serverless/Template.MapStackNextApplication.CloudFront.json');

const fs = require('fs');
const os = require('os');
const findUp = require('find-up');

/**
 * setEnvValue.
 *
 * Take values from the Serverless deployment (.serverless) and add them
 * to the environment variables in config.env in the root of the monorepo.
 *
 * If the environment value doesn't exist it will be created, if it already
 * exists then the value will be updated.
 *
 * @param {string} key the ENV VAR to be used in config.env
 * @param {string} value the value to be applied to the ENV VAR
 */
async function setEnvValue(key, value) {
  const envFile = await findUp('config.env');

  // read file from hdd & split if from a linebreak to a array
  const ENV_VARS = fs.readFileSync(envFile, 'utf8').split(os.EOL);

  // find the env we want based on the key
  const target = ENV_VARS.indexOf(
    ENV_VARS.find((line) => {
      return line.match(new RegExp(key));
    })
  );

  // replace the key/value with the new value
  ENV_VARS.splice(target, 1, `${key}=${value}`);

  // write everything back to the file system
  fs.writeFileSync(envFile, ENV_VARS.join(os.EOL));
}

/**
 * Set the cloudfront URL of the deployment
 */
setEnvValue('NEXT_APP_CLOUDFRONT_URL', jsonData.url);
setEnvValue('REACT_APP_CLOUDFRONT_URL', jsonData.url);
