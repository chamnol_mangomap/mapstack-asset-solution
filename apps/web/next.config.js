const withPWA = require('next-pwa');

if (process.env.NODE_ENV === 'development') {
  module.exports = {
    target: 'serverless',
    webpack: (config, { buildId, dev, isServer, defaultLoaders, webpack }) => {
      config.resolve.alias['mapbox-gl'] = 'maplibre-gl';
      return config;
    },
  };
} else {
  module.exports = withPWA({
    target: 'serverless',
    pwa: {
      dest: 'public',
      swSrc: 'service-worker.js',
      disable: process.env.NODE_ENV === 'development',
      buildExcludes: [/middleware-manifest.json$/],
    },
    webpack: (config, { buildId, dev, isServer, defaultLoaders, webpack }) => {
      config.resolve.alias['mapbox-gl'] = 'maplibre-gl';
      return config;
    },
    //avoiding CORS error, more here: https://vercel.com/support/articles/how-to-enable-cors
    async headers() {
      return [
        {
          // matching all API routes
          source: '/data/:path*',
          headers: [
            { key: 'Access-Control-Allow-Credentials', value: 'true' },
            { key: 'Access-Control-Allow-Origin', value: '*' },
            {
              key: 'Access-Control-Allow-Methods',
              value: 'GET,OPTIONS,PATCH,DELETE,POST,PUT',
            },
            {
              key: 'Access-Control-Allow-Headers',
              value:
                'X-CSRF-Token, X-Requested-With, Accept, Accept-Version, Content-Length, Content-MD5, Content-Type, Date, X-Api-Version',
            },
          ],
        },
      ];
    },
  });
}
