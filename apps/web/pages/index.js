import { App } from 'map-viewer';

export default function Home() {
  const viewport = {
    width: '100vw',
    height: '100vh',
    latitude: 38.84609720557644,
    longitude: -100.09397174691415,
    zoom: 3.703849286134473,
  };

  return <App viewport={viewport} />;
}
