import { useRouter } from 'next/router'
import React from 'react'

const Example = () => {
  const router = useRouter()
  const { pid } = router.query

  return ( 
    <React.Fragment>
      <p>Dynamic Route ID: {pid}</p>
      <p>NEXT_PUBLIC_API_URL: {process.env.NEXT_PUBLIC_API_URL}</p>
      <p>NEXT_PUBLIC_API_KEY: {process.env.NEXT_PUBLIC_API_KEY}</p>
    </React.Fragment>
  )
}

export default Example
