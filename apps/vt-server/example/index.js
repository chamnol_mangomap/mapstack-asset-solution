const http = require('http');
const hostname = 'localhost';
const port = 8080;
const server = http.createServer((req, res) => {
  console.log(req.headers);
  res.statusCode = 200;
  res.end(`
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<title>VT Server Example</title>
<meta name="viewport" content="initial-scale=1,maximum-scale=1,user-scalable=no" />
<script src="https://unpkg.com/maplibre-gl@1.15.2/dist/maplibre-gl.js"></script>
<link href="https://unpkg.com/maplibre-gl@1.15.2/dist/maplibre-gl.css" rel="stylesheet" />
<style>
	body { margin: 0; padding: 0; }
	#map { position: absolute; top: 0; bottom: 0; width: 100%; }
</style>
</head>
<body>
<div id="map"></div>
<script>
    var map = new maplibregl.Map({
        container: 'map',
        style:
            'https://api.maptiler.com/maps/streets/style.json?key=get_your_own_OpIi9ZULNHzrESv6T2vL',
        zoom: 4,
        center: [-100.74908787951827, 39.10583672286391]
    });

    map.on('load', function () {
        map.addSource('us-counties', {
            type: 'vector',
						maxzoom: 8,
						tiles: [ "${process.env.NEXT_PUBLIC_VT_SERVER_URL}/us-counties/{z}/{x}/{y}.pbf" ]
        });
        map.addLayer({
            'id': 'us-counties',
            'type': 'line',
            'source': 'us-counties',
            'source-layer': 'counties',
            //'source-layer': 'all',
            layout: {
              'line-cap': 'round',
              'line-join': 'round',
            },
            paint: {
              'line-opacity': 0.6,
              'line-color': 'rgb(53, 175, 109)',
              'line-width': 2,
            },
        });
    });
</script>

</body>
</html>
	 `);
});
server.listen(port, hostname);
