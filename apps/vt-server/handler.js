const serverless = require('serverless-http');
const express = require('express');
const cors = require('cors');
const { tileIndex, getTile, getInfo } = require('./src/utils');
const app = express();

// Disable cors
app.use(cors());

app.get('/us-counties/:z/:x/:y.pbf', async (req, res) => {
  getTile(req, res);
});

app.get('/us-counties/info', async (req, res) => {
  getInfo(req, res);
});

app.get('/', (req, res) => {
  res.send(
    'Use the follow url stucture to access this service: /:layer/:z/:x/:y.pbf'
  );
});

//app.listen(3000, () => {
//console.log(`Example app listening on port 3000`);
//});

module.exports.handler = serverless(app, {
  binary: ['application/octet-stream'], // We need to manually set binary response types
});
