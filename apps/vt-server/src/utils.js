const fs = require('fs');
const MBTiles = require('@mapbox/mbtiles');
const { promisify } = require('util');

module.exports.tileIndex = async () => {
  const mbtiles = await new (promisify(MBTiles))('./data/us-counties.mbtiles');
  return mbtiles;
};

/**
 * Returns a PBF tile
 */
module.exports.getTile = async (req, res) => {
  // getTile only accepts integers
  const z = +req.params['z'];
  const x = +req.params['x'];
  const y = +req.params['y'];

  const mbtiles = await new (promisify(MBTiles))('./data/us-counties.mbtiles');

  mbtiles.getTile(z, x, y, (err, data, headers) => {
    if (!err) {
      res.set(headers);
      res.send(data);
    } else {
      console.error(err);
      if (err == 'Error: Tile does not exist') {
        res.status(404).send('Not found');
      } else {
        throw err;
      }
    }
  });
};

/**
 * Returns information about the vector layer
 */
module.exports.getInfo = async (req, res) => {
  const mbtiles = await new (promisify(MBTiles))('./data/us-counties.mbtiles');

  mbtiles.getInfo((err, info) => {
    if (!err) {
      res.send(info);
    } else {
      console.error(err);
      throw err;
    }
  });
};
