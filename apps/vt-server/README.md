# Getting Started

For vt-server, the example runs more quickly on serverless than it does locally. So for testing, we deploy first.

    yarn deploy
    yarn start

You can then run the example app on:

    localhost:8080

Or you can access the tileserver directly on:

    localhost:3000

When you deploy the location of the CloudFront endpoint are added to the monorepo config.env as:

    REACT_APP_VT_SERVER_URL=https://xxxxxxxxxx.cloudfront.net
    NEXT_PUBLIC_VT_SERVER_URL=https://xxxxxxxxxx.cloudfront.net

# Debugging

If we detect errors in PBF tiles in the browser, this is usually an error in converting the binary format to geojson. We can debug that locally with the following command:

    vt2geojson --layer all -z 12 -x 2109 -y 1350 /tmp/tiles/express.pbf

# Generate MBTiles

The layer name is very important as this must be passed as the 'source-layer' when creating a map layer in MapLibre 

	tippecanoe -zg -l counties -o counties.mbtiles --drop-densest-as-needed counties.json
