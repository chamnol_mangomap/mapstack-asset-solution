const editDotEnv = require('edit-dotenv');
const fs = require('fs');

/**
 * This file is used by the serverless stack-output-plugin
 * It take the out values from the Cloudformation resourse
 * and writes them the the global config.env file so that
 * other apps know the endpoint for vt-server
 */
async function handler(data) {
  const configPath = '../../config.env';
  const dotEnv = fs.readFileSync(configPath).toString();
  const changes = {
    REACT_APP_VT_SERVER_URL: `https://${data.VtServerEndpoint}`,
    NEXT_PUBLIC_VT_SERVER_URL: `https://${data.VtServerEndpoint}`,
  };
  const newEnv = editDotEnv(dotEnv, changes);
  fs.writeFileSync(configPath, newEnv);
}
module.exports = { handler };
