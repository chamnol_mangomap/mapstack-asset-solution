# Monorepo
We are using yarn workspaces, which builds a sigle environment for all packages. To install a dependency:

	cd packages/{my-package}
	yarn add {my-dependency}

For dev dependencies:

	cd packages/{my-package}
	yarn add -D {my-dependency}

# Getting Started

## AWS Config

### Account and Credentials

Create a new AWS account and create new credentials:

	https://docs.aws.amazon.com/IAM/latest/UserGuide/id_root-user.html#id_root-user_manage_add-key

Save the credentials to your machine:

	https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-files.html

### Developer Sub Domain

For development you should run a full build on your personal AWS account. 

As serveress is charged per request, and AWS free-tier includes 1,000,000 requests per month, you will not be charged.

To configure your personal sub-domain. E.g.

	chris.mapstack.io

You will need to create a new Hosted Zone in AWS:

	Run: Route 53 > Hosted Zones > Create Hosted Zones
	CREATE: mapstack.io DO NOT CREATE: chris.mapstack.io

Then you will need to copy the four NS records and send them to Chris.

Next you need to run the deployment:

	yarn deploy

This will use the AWS credentials created in the earlier step. The deployment will return an error:

	Error: Your newly validated AWS ACM Certificate is taking a while to register as valid.  Please try running this component again in a few minute

This is because it creates an SSL certificate and can't confirm that you are the owner of the domain mapstack.io.

To fix this error you need to confirm you are the owner of the domain by going to:

	AWS Certificate Manager > Certificates > mapstack.io

Then in the domains section, copy the "CNAME name" and "CNAME value" for mapstack.io and send it to Chris.

### DevOp Sub Domain Configuration 

Steps to be performed by Chris

#### Add NS record to mapstack.io

The NS records for mapstack.io are in the AWS Mapstack Prod account, we need to redirect NS lookups for sub domains to the AWS account for the sub-domain.

To do that go to:

	Route53 > Hosted Zones > mapstack.io

Then we need add the new NS details (replace "chris" with sub-domain value, and nameservers with those provided by dev):

	$TTL    3600
	chris     IN     NS     ns-545.awsdns-04.net. 
	chris     IN     NS     ns-1103.awsdns-09.org.
	chris     IN     NS     ns-1765.awsdns-28.co.uk.
	chris     IN     NS     ns-164.awsdns-20.com.

## Global Environment (.env)

1. Copy config-template.env to config.env (cp config-template.env config.env). 
2. Then complete the required fields

The config.env file contains configuration settings used locally in development. For production and staging these variables will be set in Bitbucket Pipelines environment variables.

In addition to values added by the you, the system will also populate this file with additional values during a deploy. For example the URL for the GraphQL endpoint.

### Using Global Environment (.env)

There are three places that we will need to access these variables:

- Example React apps (must start with REACT_APP_*)
- The Next JS apps (must start with NEXT_PUBLIC_*)
- Jest tests

For React and NextJS we inject the variables in at start and build time using the 'env-cmd' CLI tool in the script section of our package.json files, e.g:

	env-cmd --silent -f $( find-up config.env ) react-scripts start
	env-cmd --silent -f $( find-up config.env ) react-scripts build

As these are injected at build time rather than run time, if any environment variable changes, you will need to run the build or deployment again in order to update the value.

For tests with Jest we inject the environment variables by defining a setup file in jest.config.js and then having that file (setup-tests.js) load the global config.env file using the 'dot-env' package. 

NextJS when deployed to Serverless doesn't work with 'env-cmd' in the package.json deploy script and instead requires the environment variables to be contained in a file called '.env.local'. So for NextJS when the deploy script is run, we copy the global .env file to the NextJS .env.local:

    "deploy": "cp $( find-up config.env ) .env.local && next build && sls"

## Deployment

As the frontend and tests are accessing AWS serverless, no tests, apps or examples will run until the deployment has been run.

2. Run 'yarn install'
3. Run 'yarn deploy' // this will install it in your AWS account (providing .aws/credentials are set)
4. Run 'yarn workspaces run test' // this will install test fixtures and ensure everything is running

### Debugging Serverless Deployments

On Long's machine we had the following error during deployment:

	serverless nextjs NoSuchDistribution: The specified distribution does not exist.

This was resolved by deleting the /home/long/.serverless directory. This then produced a new error installing the the nextjs plugin. This was resolved by manually installing the nextjs plugin:

	npm install @sls-next/serverless-component@3.6.0 --prefix /home/long/.serverless/components/registry/npm/@sls-next/serverless-component@3.6.0

## Mango Monorepo Architecture

See: https://mangowiki.atlassian.net/wiki/spaces/HOME/pages/1121320961/Monorepo+Architecture
