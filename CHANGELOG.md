# [2.4.0](https://bitbucket.org/mangomap/mapstack/compare/v2.3.0...v2.4.0) (2022-03-15)


### Bug Fixes

* 🐛 close panel when click on feature table on mobile ([f8c221e](https://bitbucket.org/mangomap/mapstack/commits/f8c221efa07b8691943ef0702267ba120c6cd17c))
* 🐛 count ([06ec6c4](https://bitbucket.org/mangomap/mapstack/commits/06ec6c492a00936bb1a4a4ee604cb28aea025395))
* 🐛 filter table result page size changes ([9c7f52f](https://bitbucket.org/mangomap/mapstack/commits/9c7f52fc3a85c3736a5298c6eb062fa5023bf279))
* 🐛 fix FilterResults/Count to display results back ([30638db](https://bitbucket.org/mangomap/mapstack/commits/30638dbcc25b8b121773df552bff95d792ae7f00))
* 🐛 fix map selection with re defined meta data ([0089509](https://bitbucket.org/mangomap/mapstack/commits/008950966bd78134c71c532384387f3cf103e39a))
* 🐛 fix merge conflict from master ([1feae94](https://bitbucket.org/mangomap/mapstack/commits/1feae94b0e8024157246c10f127a06c6a8ec8827))
* 🐛 fix path alias in ui-components ([7201692](https://bitbucket.org/mangomap/mapstack/commits/7201692ad8fe7cc029ffd7b870e0841d4a9c4728))
* 🐛 fixed failing spec ([53481f9](https://bitbucket.org/mangomap/mapstack/commits/53481f9177ad95c73629ce0348161fa05b9be961))
* 🐛 fixed filter extend ([a21eb80](https://bitbucket.org/mangomap/mapstack/commits/a21eb807dc7420a0319b10740d166bba7a0dc1b2))
* 🐛 fixed highlightSelection failing spec ([fd62dfa](https://bitbucket.org/mangomap/mapstack/commits/fd62dfa7e3eeb5d92a44bf4fde6f394f27d71958))
* 🐛 fixed highlightSelection failing tests ([eaa2539](https://bitbucket.org/mangomap/mapstack/commits/eaa2539df7225ec7a2b5b9d669a0d23a80dbb846))
* 🐛 fixed map filter fit boung ([1ab9efe](https://bitbucket.org/mangomap/mapstack/commits/1ab9efe7df526bbaf70b0494292e206190d579a3))
* 🐛 fixed map highligh ([45a4d83](https://bitbucket.org/mangomap/mapstack/commits/45a4d8332d9e98a0511d1dd6cdfa66baf2e7be3b))
* 🐛 merged with master and resolved some conflict ([e101c50](https://bitbucket.org/mangomap/mapstack/commits/e101c50f5c28ee86242e638ba1fbb87f1784040f))
* 🐛 remove dispatch on set state function ([e31967b](https://bitbucket.org/mangomap/mapstack/commits/e31967bcddb4b29b8b63cacd528389271937cc3b))
* 🐛 remove duplicate function declaration ([c60afe0](https://bitbucket.org/mangomap/mapstack/commits/c60afe01380477359fe74d334e4589292a1117bf))
* 🐛 resolve merge conflict ([d06621b](https://bitbucket.org/mangomap/mapstack/commits/d06621ba715ca05fcb06df34b58c8b31cfacc557))
* 🐛 resolve pull conflict ([eaa7b60](https://bitbucket.org/mangomap/mapstack/commits/eaa7b60f6eb357e5a5fa8f7ba8c0558c7ebc01fb))


### Features

* 🎸 added feature selection ([615bbfa](https://bitbucket.org/mangomap/mapstack/commits/615bbfa14a314b721fca55b048fa6cedbcb27a4b))
* 🎸 apply [@mui](https://bitbucket.org/mui) license key ([f6b2e6b](https://bitbucket.org/mangomap/mapstack/commits/f6b2e6ba4d3b46e51537183f29e6ccb4c26b3a84))
* 🎸 handle feature selection ([c5cebd6](https://bitbucket.org/mangomap/mapstack/commits/c5cebd63867ebf21dea2883de8457b2b11fa367c))
* 🎸 remember last used filter result tab ([7f51618](https://bitbucket.org/mangomap/mapstack/commits/7f516185ff743fab6fca92ae0a73110272fec22b))
* 🎸 resizable drawer ([9b62b5f](https://bitbucket.org/mangomap/mapstack/commits/9b62b5f4ddf8a505e54c58ec9684bc54a4b13922))

# [2.3.0](https://bitbucket.org/mangomap/mapstack/compare/v2.2.0...v2.3.0) (2022-03-14)


### Bug Fixes

* 🐛 remove console log ([30bc859](https://bitbucket.org/mangomap/mapstack/commits/30bc8593eb354480cda9a09ee1f96e4221f89263))


### Features

* 🎸 change meta data fetching flow ([6b8aded](https://bitbucket.org/mangomap/mapstack/commits/6b8aded0991bd70a0b586635e2837c6853686f83))

# [2.2.0](https://bitbucket.org/mangomap/mapstack/compare/v2.1.0...v2.2.0) (2022-03-09)


### Bug Fixes

* 🐛 add map selection default select & default Typography ([ef002f8](https://bitbucket.org/mangomap/mapstack/commits/ef002f8ef55fd31625018f4c79feb98c30a940cb))
* 🐛 add Typography to legend ([cdb7174](https://bitbucket.org/mangomap/mapstack/commits/cdb71740e57451c07dae7c7d28442e37ceeffce8))
* 🐛 change chip from jsx to js ([f7d34d8](https://bitbucket.org/mangomap/mapstack/commits/f7d34d8f55c3a2a87a6ce33e928dcd09480031cb))
* 🐛 change navigation tooltip wrapper ([fb5270f](https://bitbucket.org/mangomap/mapstack/commits/fb5270f3ccc9a73f555fa8decb43d4958a91743e))
* 🐛 change to use PropChips for map selection ([5701c95](https://bitbucket.org/mangomap/mapstack/commits/5701c959bb54369cee6d3e11cf2099425ca3920f))
* 🐛 clean code ([07952fd](https://bitbucket.org/mangomap/mapstack/commits/07952fd88dabcc19e04b4cd04abd8416aeedcc3d))
* 🐛 fix tooltip with bottom navigation ([9576b26](https://bitbucket.org/mangomap/mapstack/commits/9576b26076fec8f83d76a1b338ac86be08217b82))
* 🐛 fixed some warning message when running test ([4841012](https://bitbucket.org/mangomap/mapstack/commits/48410121b01f8710d8190ccbd13ebb4aef63c66d))
* 🐛 forwardRef warning for tooltip and clickawaylistener ([2a8cc4d](https://bitbucket.org/mangomap/mapstack/commits/2a8cc4d74bbfb869ec96a67d453c841e3d455363))
* 🐛 make filter over map selection & fix map selection test ([e8f5217](https://bitbucket.org/mangomap/mapstack/commits/e8f5217f3492b0b973bb328ff8ff19fbf634165a))
* 🐛 modified update layer style logic ([c3a0a5d](https://bitbucket.org/mangomap/mapstack/commits/c3a0a5d1969bd6cc55e6cf6c023765b2e810e47c))
* 🐛 move clickawaylistener to tooltip ui-component ([543ce58](https://bitbucket.org/mangomap/mapstack/commits/543ce5894b5dc885a00bf0b7db6e53cd38390654))
* 🐛 remove category map selection logic ([cb1fa86](https://bitbucket.org/mangomap/mapstack/commits/cb1fa86bafb50a7129e4ad63c82a495a496f0129))
* 🐛 show legend number ([377b93e](https://bitbucket.org/mangomap/mapstack/commits/377b93ee522a32dbb1da2bee664e63c2f1b74ae9))
* 🐛 storybood doesn't show description and default props ([0f9e057](https://bitbucket.org/mangomap/mapstack/commits/0f9e0579cb507ad0f87dfdbc9a279fefabe213f9))


### Features

* 🎸 add maplayer update on selection ([aa0d529](https://bitbucket.org/mangomap/mapstack/commits/aa0d5299e8040f51c0760f9edfe9a5f3889f37a2))
* 🎸 add none option for map selection ([647f779](https://bitbucket.org/mangomap/mapstack/commits/647f77938a8b9d01b551d328e1c33d848420fe33))
* 🎸 add SelectionRange stories to storybook ([ded5857](https://bitbucket.org/mangomap/mapstack/commits/ded585717d0f2286834f9f4d9b93a5b7e2d69e28))
* 🎸 finish legend functionalities ([3cfc846](https://bitbucket.org/mangomap/mapstack/commits/3cfc846ff3c01a7c6443592abf6dca281ca59317))
* 🎸 implement map selection view ([048f455](https://bitbucket.org/mangomap/mapstack/commits/048f4555b5b70786eda1fe795d330eb5f35bd99b))
* 🎸 implement map selection with tools navigation ([565bc23](https://bitbucket.org/mangomap/mapstack/commits/565bc23f6f55706c21f4bfb15fd40243b5e5f437))
* 🎸 improving ui-component storybook ([12717e4](https://bitbucket.org/mangomap/mapstack/commits/12717e439ed974b873e2697fc1bdf381a002b7c5))
* 🎸 revert back to use update props for layerstyle ([2374edf](https://bitbucket.org/mangomap/mapstack/commits/2374edf0e5e583ccf8b5f0d59f1cc5b74b0b4192))
* 🎸 update map selection range ([ae29f9d](https://bitbucket.org/mangomap/mapstack/commits/ae29f9db43c0dcd1f086b0cd83c357cb12e805f1))

# [2.1.0](https://bitbucket.org/mangomap/mapstack/compare/v2.0.0...v2.1.0) (2022-03-09)


### Features

* **vt-server:** migrated from geojson-vt to mbtiles ([0ec8eb2](https://bitbucket.org/mangomap/mapstack/commits/0ec8eb24922b9e4676f488a04218f0e5dd006a77))

# [2.0.0](https://bitbucket.org/mangomap/mapstack/compare/v1.11.1...v2.0.0) (2022-03-04)


### Bug Fixes

* 🐛 change id for filter table row ([c4545c0](https://bitbucket.org/mangomap/mapstack/commits/c4545c06b7e51350bfb5587aad23b45181fdf413))
* 🐛 filter header alignment should be now center ([01acb6c](https://bitbucket.org/mangomap/mapstack/commits/01acb6cb880c51cd02cf38d443fc21f2015d1fb8))
* 🐛 fix overflow problem in global styles ([ab58dcb](https://bitbucket.org/mangomap/mapstack/commits/ab58dcb220c87db9ea9e3009884bf38e066179a0))
* 🐛 fix pagination problem on x-datagrid-pro ([57b9432](https://bitbucket.org/mangomap/mapstack/commits/57b9432fbf83af8717c8ff49ad86d8bdb9574600))
* 🐛 fix scrollbar visibility on datagrid ([d88c783](https://bitbucket.org/mangomap/mapstack/commits/d88c7834847ea4b16bf89e7ee700b46878b2a2db))
* 🐛 fix yarn.lock ([9f8f3a4](https://bitbucket.org/mangomap/mapstack/commits/9f8f3a465eb64a0a95d02883b3728c894c6c2bf1))
* 🐛 fixed row selected pan to location ([e7f23fa](https://bitbucket.org/mangomap/mapstack/commits/e7f23fa26a2c0761ec0898d3b8c28cd0579f8d5d))
* 🐛 hide some icons in bottom navigation that doesnt work ([426cc90](https://bitbucket.org/mangomap/mapstack/commits/426cc90949519f81c9a31a16142aeadd8887d616))
* 🐛 remove unused code ([ff29cbb](https://bitbucket.org/mangomap/mapstack/commits/ff29cbb9ca03c3eadf1f0c4df00d795adbde1595))
* 🐛 resolve merge conflict with mike ([e5ce723](https://bitbucket.org/mangomap/mapstack/commits/e5ce7231db9a3f22b9e306152d761c3fa50c6a3e))
* 🐛 revert navigation drawer width ([388310c](https://bitbucket.org/mangomap/mapstack/commits/388310c8608a5f3c03cbff33b15343d20e1804ca))
* 🐛 update yarn.lock ([f3cdd98](https://bitbucket.org/mangomap/mapstack/commits/f3cdd981b125f0a2ecf9dda1d0e2e61543b357b5))
* **map-viewer:** changed category list to only include states ([12a5a34](https://bitbucket.org/mangomap/mapstack/commits/12a5a341dc7411b1fc9a9ffdacf7e480ddda0859))


### Code Refactoring

* 💡 remove deprecated code in FilterTable ([3e881b3](https://bitbucket.org/mangomap/mapstack/commits/3e881b38612b94a15b7c919b5fe9b8f8567663b1))


### Features

* 🎸 add @mui/x-data-grid-pro to make ColumnHeader resizable ([f19fc5b](https://bitbucket.org/mangomap/mapstack/commits/f19fc5bdd104fcaa16ba9597ecec50dfded3ee1a))
* 🎸 add box and typography into ui-components/atom ([8b8e48d](https://bitbucket.org/mangomap/mapstack/commits/8b8e48d4b4c1f1c583317651bf7a550a24c0e6d0))
* 🎸 add datagrid library ([7c818f5](https://bitbucket.org/mangomap/mapstack/commits/7c818f5c7540b873df0d1070176ba8abd54a14a2))
* 🎸 add prop-types support ([7e51fe5](https://bitbucket.org/mangomap/mapstack/commits/7e51fe5de02044418864a89abd578b1d623254db))
* 🎸 add tooltip on result BottomNavigation when clicked ([a6cce42](https://bitbucket.org/mangomap/mapstack/commits/a6cce422af9de7d80464ec0a9a3de6d1e216eff4))
* 🎸 added jsconfig.js for absolute path ([a485cba](https://bitbucket.org/mangomap/mapstack/commits/a485cbacddd30d2e7fd6a0d5166605de3e926dc5))
* 🎸 added kformat for results ([24717a4](https://bitbucket.org/mangomap/mapstack/commits/24717a42de6c6b2c77087e2aa6b3a68019fd87e9))
* 🎸 added pagination to result table ([a0ee001](https://bitbucket.org/mangomap/mapstack/commits/a0ee001458ce9a08e3f6e433cd8a6301567ca048))
* 🎸 display table ([cd7dbd9](https://bitbucket.org/mangomap/mapstack/commits/cd7dbd9f7850dd88548640c5c799ba3f28e233b8))
* 🎸 ignore .vscode configuration ([bbe47f0](https://bitbucket.org/mangomap/mapstack/commits/bbe47f0130a0e1288e38dcd80f94502112496832))
* 🎸 paginated table ([65f6642](https://bitbucket.org/mangomap/mapstack/commits/65f664225a4bf03bf36cbc9bde23ef7c02257f34))
* 🎸 pan to feature when click feature in table result ([950e910](https://bitbucket.org/mangomap/mapstack/commits/950e9105ec5a3fa07f4b579e30381bd046e7dc02))
* 🎸 refactor and clean code ([7723fa3](https://bitbucket.org/mangomap/mapstack/commits/7723fa398cac1c1092f2f62602361eb009e5fb75))
* 🎸 results ([dd4a499](https://bitbucket.org/mangomap/mapstack/commits/dd4a49906345f56592e207b145c7ecdf23f5d486))
* 🎸 setFitbound one click on the feature in table ([42eabcf](https://bitbucket.org/mangomap/mapstack/commits/42eabcfe692e7aa56e8480c85e0c1c2491941638))
* 🎸 show count by category ([f749741](https://bitbucket.org/mangomap/mapstack/commits/f749741561ab4aa3bc1ecc0565a2f985760b0626))
* 🎸 show filter and filter results badge ([fb7cb0a](https://bitbucket.org/mangomap/mapstack/commits/fb7cb0a85b4edda56d30a94b6f864cec266dfb7f))
* 🎸 show scrollable in tab content ([e8cba3a](https://bitbucket.org/mangomap/mapstack/commits/e8cba3ad93b46014e6066e264062f18d7815e9a4))
* 🎸 update filter table scroll button ([8b6e778](https://bitbucket.org/mangomap/mapstack/commits/8b6e778f52e8d5edc6190ede3d39114dafdcebcd))


### BREAKING CHANGES

* 🧨 \

## [1.11.1](https://bitbucket.org/mangomap/mapstack/compare/v1.11.0...v1.11.1) (2022-03-01)


### Bug Fixes

* **serverless:** moved serverless from global install to package dependency ([97ff70d](https://bitbucket.org/mangomap/mapstack/commits/97ff70d81d7b0d36b1dcaff9c9dd8b9ffe20c0bd))

# [1.11.0](https://bitbucket.org/mangomap/mapstack/compare/v1.10.0...v1.11.0) (2022-02-10)


### Bug Fixes

* 🐛 disable pitchRotate, dragRoate and zoomRotate map ([4ae81c9](https://bitbucket.org/mangomap/mapstack/commits/4ae81c96ac215f89a0156c4fed3775f3713f5ecd))
* 🐛 fixed feature highlight off the visible ([4aa688b](https://bitbucket.org/mangomap/mapstack/commits/4aa688babffb01d7f396d1aa9326c7b58d10627d))
* 🐛 fixed map zoom after fitbounds and drag ([1880bdb](https://bitbucket.org/mangomap/mapstack/commits/1880bdb93ae9d464be9baacc653ed16b94aa4c4b))
* 🐛 remove filters from local storage persistence ([f1569db](https://bitbucket.org/mangomap/mapstack/commits/f1569db65c5530f4602d058cde7b0139c138d25d))
* 🐛 sort filter by property and operator ([ae4d1b3](https://bitbucket.org/mangomap/mapstack/commits/ae4d1b3884cdfcbcf77b518928bd07d325cd017c))


### Features

* 🎸 group filter by property name ([c468c8e](https://bitbucket.org/mangomap/mapstack/commits/c468c8e65ab95c7924f67ae66edb75c9f371d8ee))

# [1.10.0](https://bitbucket.org/mangomap/mapstack/compare/v1.9.0...v1.10.0) (2022-02-07)


### Features

* 🎸 change to absolute import for map-viewer ([d312376](https://bitbucket.org/mangomap/mapstack/commits/d312376ed83a0f140b4c26b2a3c3de98b7314ca4))
* 🎸 resolve to absolute import for ui-components ([5c4c513](https://bitbucket.org/mangomap/mapstack/commits/5c4c513a3ea542cc542b66b6b30df74d8bea4c11))

# [1.9.0](https://bitbucket.org/mangomap/mapstack/compare/v1.8.0...v1.9.0) (2022-02-04)


### Bug Fixes

* **map-viewer:** fixed broken test ([2cc9eb5](https://bitbucket.org/mangomap/mapstack/commits/2cc9eb5dcbfcf1b8083f050594a2db0f1284210f))
* **merge:** merged in master branch, required rerun of install ([d0daa2c](https://bitbucket.org/mangomap/mapstack/commits/d0daa2c594c9717c5527a4b803edf7933e608f41))


### Features

* **map-viewer:** updated style and base map ([6aaeb40](https://bitbucket.org/mangomap/mapstack/commits/6aaeb40ef9dbb05740268f60e0c6024d8e637792))

# [1.8.0](https://bitbucket.org/mangomap/mapstack/compare/v1.7.1...v1.8.0) (2022-02-04)

## [1.7.1](https://bitbucket.org/mangomap/mapstack/compare/v1.7.0...v1.7.1) (2022-01-17)


### Bug Fixes

* **vt-server:** updated index and package to use local when using dev script ([e8eedd2](https://bitbucket.org/mangomap/mapstack/commits/e8eedd2bb8b2fc32958d64c5cf24e39b7cce405e))

# [1.7.0](https://bitbucket.org/mangomap/mapstack/compare/v1.6.0...v1.7.0) (2022-01-17)


### Bug Fixes

* **package:** updated dependencies ([28d4aaf](https://bitbucket.org/mangomap/mapstack/commits/28d4aaff750358c6799e4fdd1e266dbc821d4684))
* **vt-server:** removated static path to api gateway ([785a18a](https://bitbucket.org/mangomap/mapstack/commits/785a18acc4815c96d25dda61a84898b1d7cc4c86))
* **vt-server:** updated pbf version to v2 ([d23695b](https://bitbucket.org/mangomap/mapstack/commits/d23695bfadb48f73b7fd0304dbfadf705b40bcc3))


### Features

* **vt-server:** able to deploy sample data to serverless ([d175f8d](https://bitbucket.org/mangomap/mapstack/commits/d175f8d4a0f43786bc7819d0c351d522eeae4080))
* **vt-server:** added cloudfront setup and configured export of cloudfront endpoints to config.env ([c440db8](https://bitbucket.org/mangomap/mapstack/commits/c440db8cc317ddfad4c782b7c3031a3a65a15784))
* **vt-server:** first commit ([c5afca7](https://bitbucket.org/mangomap/mapstack/commits/c5afca763f28718d96d2b8a6056881c19e489d37))

# [1.6.0](https://bitbucket.org/mangomap/mapstack/compare/v1.5.5...v1.6.0) (2022-01-04)


### Bug Fixes

* 🐛 fixed failing tests ([566e0bc](https://bitbucket.org/mangomap/mapstack/commits/566e0bc1d77b440c445873ad2f6d56f74f7679d1))
* 🐛 rename metaData.json to meta.json ([54c2cef](https://bitbucket.org/mangomap/mapstack/commits/54c2cef832c88f98a9d73b81f7f98078cc6462b3))


### Features

* 🎸 refresh data when new version available ([b1d7e5c](https://bitbucket.org/mangomap/mapstack/commits/b1d7e5c521c819fbed2d19a93b2fd1c77de62b73))
* 🎸 revalidate data once it's updated ([1f481ee](https://bitbucket.org/mangomap/mapstack/commits/1f481ee8aeaba69125f521e9ed9da2ff63c6b846))

## [1.5.5](https://bitbucket.org/mangomap/mapstack/compare/v1.5.4...v1.5.5) (2021-12-22)


### Bug Fixes

* **web-app:** added fix to serverless-dev to use env var for sub-domain ([75f18f5](https://bitbucket.org/mangomap/mapstack/commits/75f18f503a665448ed5d40eb615af907e921af64))

## [1.5.4](https://bitbucket.org/mangomap/mapstack/compare/v1.5.3...v1.5.4) (2021-12-21)


### Bug Fixes

* **pipeline:** changed s3 directory names used for cachine serverless config ([a336680](https://bitbucket.org/mangomap/mapstack/commits/a3366802009573de272915ecedb97637b375f7c7))

## [1.5.3](https://bitbucket.org/mangomap/mapstack/compare/v1.5.2...v1.5.3) (2021-12-20)


### Bug Fixes

* **package:** added a destroy setting turbo ([4444b50](https://bitbucket.org/mangomap/mapstack/commits/4444b501fafb5b7129470c72ff96eb26e40fa41e))

## [1.5.2](https://bitbucket.org/mangomap/mapstack/compare/v1.5.1...v1.5.2) (2021-12-20)


### Bug Fixes

* **pipeline:** removed cache as it slowed things down ([e275c48](https://bitbucket.org/mangomap/mapstack/commits/e275c4842724db82af6a5eafd28bccf5767f1359))

## [1.5.1](https://bitbucket.org/mangomap/mapstack/compare/v1.5.0...v1.5.1) (2021-12-20)


### Bug Fixes

* **package:** added config for deploy:staging and deploy:prod for turbo ([2521e6d](https://bitbucket.org/mangomap/mapstack/commits/2521e6d3977388e022f889d6dae6c6e602974f41))

# [1.5.0](https://bitbucket.org/mangomap/mapstack/compare/v1.4.2...v1.5.0) (2021-12-20)


### Bug Fixes

* **package:** fixed typo in script ([16ee24c](https://bitbucket.org/mangomap/mapstack/commits/16ee24c3f09b994d055e99bb842514ea23cd91a9))


### Features

* **pipeline:** added node caching ([45f533e](https://bitbucket.org/mangomap/mapstack/commits/45f533ebae7248e5bd6d1e0ccd71dc38171f5e27))
* **pipeline:** integrated turborepo ([30a3423](https://bitbucket.org/mangomap/mapstack/commits/30a3423a98efd0a36b822a370f9db181d0f281ca))

## [1.4.2](https://bitbucket.org/mangomap/mapstack/compare/v1.4.1...v1.4.2) (2021-12-15)


### Bug Fixes

* **pipeline:** broke the master merge into multiple steps ([c67fb8f](https://bitbucket.org/mangomap/mapstack/commits/c67fb8f0f3233b9b43525f421e72d416b8600d4b))
* **pipeline:** updated deployment tag that was duplicated ([f281ab1](https://bitbucket.org/mangomap/mapstack/commits/f281ab1339afc073a5f8288584790964aca6c56c))

## [1.4.1](https://bitbucket.org/mangomap/mapstack/compare/v1.4.0...v1.4.1) (2021-12-15)


### Bug Fixes

* **next-app:** fixed typo in deploy scripts in package.json ([1cbd2ec](https://bitbucket.org/mangomap/mapstack/commits/1cbd2ec5c9055318820aa306ce98a5d76b49032a))

# [1.4.0](https://bitbucket.org/mangomap/mapstack/compare/v1.3.0...v1.4.0) (2021-12-15)


### Features

* **serverless:** added support for deployment to custom sub-domains ([938d965](https://bitbucket.org/mangomap/mapstack/commits/938d965ebe8deeb0ac34bf0eef7a6f365f090a3a))

# [1.3.0](https://bitbucket.org/mangomap/mapstack/compare/v1.2.1...v1.3.0) (2021-12-15)


### Bug Fixes

* 🐛 fixed storybook issue with mui v5 ([fe1054d](https://bitbucket.org/mangomap/mapstack/commits/fe1054d438e35080af09d5ac02f400b4fa9a3ba5))


### Features

* 🎸 added storybook package ([189c2bb](https://bitbucket.org/mangomap/mapstack/commits/189c2bba0e257fbada61c426bb257216696f1116))

## [1.2.1](https://bitbucket.org/mangomap/mapstack/compare/v1.2.0...v1.2.1) (2021-12-14)


### Bug Fixes

* **pipeline:** fixed typo ([1c7f0f2](https://bitbucket.org/mangomap/mapstack/commits/1c7f0f21cd33dc7b5e9756fd780eb361f8342f9b))

# [1.2.0](https://bitbucket.org/mangomap/mapstack/compare/v1.1.6...v1.2.0) (2021-12-14)


### Features

* **pipeline:** added custom build step for production deployment ([0273019](https://bitbucket.org/mangomap/mapstack/commits/027301977505d0fed400cfd9ada5c19b9de64c48))

## [1.1.6](https://bitbucket.org/mangomap/mapstack/compare/v1.1.5...v1.1.6) (2021-12-14)


### Bug Fixes

* **pipeline:** fixed typo in aws s3 create call ([231d8ce](https://bitbucket.org/mangomap/mapstack/commits/231d8cea450d90c391bef4f4ac6d4000be4ced7c))

## [1.1.5](https://bitbucket.org/mangomap/mapstack/compare/v1.1.4...v1.1.5) (2021-12-14)


### Bug Fixes

* **pipeline:** moved awscli install into main step ([5e6d759](https://bitbucket.org/mangomap/mapstack/commits/5e6d759e0df93a9812646b2bc5f1b33ac0a200ec))

## [1.1.4](https://bitbucket.org/mangomap/mapstack/compare/v1.1.3...v1.1.4) (2021-12-14)


### Bug Fixes

* **pipeline:** added a line for debugging ([8648973](https://bitbucket.org/mangomap/mapstack/commits/8648973a92d00d539b5700b3de50e7d20c78e97a))
* **pipeline:** added apt-get update ([fbb48f2](https://bitbucket.org/mangomap/mapstack/commits/fbb48f26f34350ea4f9bfd3f6e2c9602370ddedf))
* **pipeline:** added install for aws-cli ([aabe391](https://bitbucket.org/mangomap/mapstack/commits/aabe3913d66ebfd1242d17db84f04c3931946ee1))
* **pipeline:** added manual python3 install back ([c52ca3b](https://bitbucket.org/mangomap/mapstack/commits/c52ca3b287f5cc5ede66f4bc9f9571d111a58972))
* **pipeline:** added python install to aws-cli install step ([bdb2336](https://bitbucket.org/mangomap/mapstack/commits/bdb2336e160766ecb98ba390f701c0fd389d19bc))
* **pipeline:** added step to checking python version (debugging build) ([85ca8ff](https://bitbucket.org/mangomap/mapstack/commits/85ca8ffe542a45987cf21f6126be224238277609))
* **pipeline:** changed python 3.6 install to unofficial source ([4a37b42](https://bitbucket.org/mangomap/mapstack/commits/4a37b42443f3024a3b2acce0c71fe3c1165b1a74))
* **pipeline:** fixed awscli install in docker and moved code here ([234bf0a](https://bitbucket.org/mangomap/mapstack/commits/234bf0aa382ab68dfbe1cb1dfa2de44f5e6681f1))
* **pipeline:** fixed issue with alias in yml ([15c38e0](https://bitbucket.org/mangomap/mapstack/commits/15c38e0506d6ed4685ab2bfbd9bad3dbea23ddf5))
* **pipeline:** fixed issue with apt install order for python (required for aws-cli) ([342f6d5](https://bitbucket.org/mangomap/mapstack/commits/342f6d5c01562f07eb86a2f27266623d5523f0a9))
* **pipeline:** install pip3 ([4b58148](https://bitbucket.org/mangomap/mapstack/commits/4b5814815794be810d7f952402a56cf010789b41))
* **pipeline:** moved aws-cli to seperate step ([3d81d6c](https://bitbucket.org/mangomap/mapstack/commits/3d81d6c58072e2d37c23825459ef9e428fb0e32f))
* **pipeline:** moved the pip3 install into a shared step to avoid merge ([0c0060e](https://bitbucket.org/mangomap/mapstack/commits/0c0060e6ee0639e4ef1eac2b0cf1e95caa2396c6))
* **pipeline:** removed comments to try and fix pipeline error ([20e7985](https://bitbucket.org/mangomap/mapstack/commits/20e798542dfaddeb8e358a03d6faab112421903c))
* **pipeline:** removed incorrect -y flag from pip3 install ([205301b](https://bitbucket.org/mangomap/mapstack/commits/205301bbf596a2e40da61eaaf7eb741548c8d71d))
* **pipeline:** removed sudo command that shouldn't have been there ([9442e28](https://bitbucket.org/mangomap/mapstack/commits/9442e28b22fcbadb741805e27a9e2c22a586f0aa))
* **pipeline:** try installing awscli with pip3 ([e297aff](https://bitbucket.org/mangomap/mapstack/commits/e297aff7bc7787595d1f49392d9cc722a3572d5a))

## [1.1.3](https://bitbucket.org/mangomap/mapstack/compare/v1.1.2...v1.1.3) (2021-12-14)


### Bug Fixes

* **pipeline:** build now stores/retrieved .serverless to s3 ([cadfc54](https://bitbucket.org/mangomap/mapstack/commits/cadfc54202010e04face412dca6b169a248298d6))

## [1.1.2](https://bitbucket.org/mangomap/mapstack/compare/v1.1.1...v1.1.2) (2021-12-13)


### Bug Fixes

* **pipeline:** added serverless install to pipeline ([85d2704](https://bitbucket.org/mangomap/mapstack/commits/85d2704cfe1a6b30556cb9dd11159da8403f9885))

## [1.1.1](https://bitbucket.org/mangomap/mapstack/compare/v1.1.0...v1.1.1) (2021-12-13)


### Bug Fixes

* **package:** added global deploy:staging and deploy:prod scripts ([f51bf39](https://bitbucket.org/mangomap/mapstack/commits/f51bf396b34504168e038acfedb6cd5b7f4f641a))

# [1.1.0](https://bitbucket.org/mangomap/mapstack/compare/v1.0.0...v1.1.0) (2021-12-13)


### Features

* **next-app:** added pipeline for staging and production ([e5c2d74](https://bitbucket.org/mangomap/mapstack/commits/e5c2d7490f01d9a267c8635f41306d3866b799d5))

# 1.0.0 (2021-12-13)


### Bug Fixes

* 🐛 enable pwa ([dc0d95f](https://bitbucket.org/mangomap/mapstack/commits/dc0d95f8b02577062ca8edbb9bb82475e04945ea))
* 🐛 fixed fallback page ([65ab8af](https://bitbucket.org/mangomap/mapstack/commits/65ab8af7279f6c32a2ce0e9a701612bccfd75f0c))
* 🐛 fixed map fitbound ([a564b2e](https://bitbucket.org/mangomap/mapstack/commits/a564b2e5e7b47d78739d3e09f6e04110f5dca946))
* 🐛 fixed pwa ([eac71be](https://bitbucket.org/mangomap/mapstack/commits/eac71be72a2469dd957b40d3e96ae534dde6f4e9))
* 🐛 map template ([6d3d550](https://bitbucket.org/mangomap/mapstack/commits/6d3d5509db6d57beca783ec0e63b61fd7212088f))
* 🐛 remove style from example ([beb2dfb](https://bitbucket.org/mangomap/mapstack/commits/beb2dfb0bcf8f66e392160b13d545ebdbf7a7bf6))

### Bug Fixes

* 🐛 filter ([0cd2815](https://bitbucket.org/mangomap/mapstack/commits/0cd28154f9d75595200431a8164915ca93d8a2ad))
* 🐛 fix filter edit ui ([93dbfc8](https://bitbucket.org/mangomap/mapstack/commits/93dbfc82d49be5cfa82c3c344908851257b36eec))
* 🐛 fixed and merged ([ea1e6da](https://bitbucket.org/mangomap/mapstack/commits/ea1e6da32c933174c8e56b6c63c926efa9d5af8d))
* 🐛 fixed chipinput cause warning ([15ff283](https://bitbucket.org/mangomap/mapstack/commits/15ff283a9ef7e832a8a5d0a0d8dd4e62af6ad362))
* 🐛 fixed editing numeric filter ([c9421b8](https://bitbucket.org/mangomap/mapstack/commits/c9421b80280bd5abaa5662dc8d890155ff0ac239))
* 🐛 fixed failing specs ([883bf4b](https://bitbucket.org/mangomap/mapstack/commits/883bf4bbb5d41898893d1b20106d75517d87a741))
* 🐛 fixed failing tests ([67391ed](https://bitbucket.org/mangomap/mapstack/commits/67391edc50b06a20db7062f8c8a452e92dd0988b))
* 🐛 fixed failing tests ([5f32733](https://bitbucket.org/mangomap/mapstack/commits/5f3273377f9f3ad23bcaf2616785362244a0fffc))
* 🐛 fixed farling test and update storybook ([8917a6b](https://bitbucket.org/mangomap/mapstack/commits/8917a6b18cce0b2c5cbbbfde14fadea9a95a07e1))
* 🐛 fixed filter ([4a55dda](https://bitbucket.org/mangomap/mapstack/commits/4a55ddadbaf065f1ae838bb0327f9b8591a8e6aa))
* 🐛 Fixed filter ([ff4be87](https://bitbucket.org/mangomap/mapstack/commits/ff4be871de86f9bc9ea9c1f1de1e8c715488f90c))
* 🐛 fixed removing filter cause error ([56ea1de](https://bitbucket.org/mangomap/mapstack/commits/56ea1de4ed6fc798ad46deef01bf2505fa5b97db))
* 🐛 fixed selectionSet init ([53787d6](https://bitbucket.org/mangomap/mapstack/commits/53787d68f0d4aa1788ecc584085bc9d2e3cb9313))
* 🐛 fixed typo ([1c46d5c](https://bitbucket.org/mangomap/mapstack/commits/1c46d5c5f913be1d9cfd14b08787be2629ce1b34))
* 🐛 fixing theme not being applied ([d1f02e5](https://bitbucket.org/mangomap/mapstack/commits/d1f02e538cb762ff20fb9a1df3c8d7ba66b630de))
* 🐛 hide MapTiler URL ([544087e](https://bitbucket.org/mangomap/mapstack/commits/544087e319b6c0709b46c9d437f04f6d4268cc9c))
* 🐛 implement follow ui improvement request ([561742f](https://bitbucket.org/mangomap/mapstack/commits/561742ff0f655627a04f083eec72ef4aebda55b5))
* 🐛 remove multiple filters and recreate ([42e263b](https://bitbucket.org/mangomap/mapstack/commits/42e263b5e37742365b28ee1006cb5b008964744c))
* 🐛 replace geoextend with turf to get bbox of a layer ([3878f26](https://bitbucket.org/mangomap/mapstack/commits/3878f26627dcb7918bd2ad7199fdcec3f5a418e3))
* 🐛 trying to fix theme ([99067ac](https://bitbucket.org/mangomap/mapstack/commits/99067acc885d982cd8cedde56bb93a3386b3be64))
* 🐛 update filter ui improvement ([a89e91d](https://bitbucket.org/mangomap/mapstack/commits/a89e91d2dee8ebd0ba64e5c586135ac9ad7cf307))
* 🐛 viewport responsive ([6b21262](https://bitbucket.org/mangomap/mapstack/commits/6b21262ea99eb6b8ce5cd1dfb8915aeab3cbdc2b))
>>>>>>> master


### Features

* 🎸 added chip input ([6df86dc](https://bitbucket.org/mangomap/mapstack/commits/6df86dc433df9fcc532424fd2b8fe5a69a9e54be))
* 🎸 added chip to ui-components ([b60b227](https://bitbucket.org/mangomap/mapstack/commits/b60b227809b7a35e86cc89384a8db9e8f77354ca))
* 🎸 added feature highligh ([215b36a](https://bitbucket.org/mangomap/mapstack/commits/215b36a8505a3a051abad4ab247839aee7d0eb0b))
* 🎸 added filter ([6d1c8c8](https://bitbucket.org/mangomap/mapstack/commits/6d1c8c82058f369c64bb88070c7364fcc3b3a01a))
* 🎸 added remove filter functionality ([8ce9d57](https://bitbucket.org/mangomap/mapstack/commits/8ce9d576f3530d3897ea70de456790ed7361ed39))
* 🎸 added stories book for stringOperator component ([0ef2446](https://bitbucket.org/mangomap/mapstack/commits/0ef24468db79f13fd5dc6d8fbf0b218246ed6830))
* 🎸 added stories for numericOperator ([49e2383](https://bitbucket.org/mangomap/mapstack/commits/49e23833e2e54e4b246381f36f1b9ff6d45dce18))
* 🎸 added stories for NumericOperatorAndValue, String ([6a840ac](https://bitbucket.org/mangomap/mapstack/commits/6a840acb3d24ff7683bf9659f250a090db03b5f3))
* 🎸 added stories for SidebarHeader component ([22ca722](https://bitbucket.org/mangomap/mapstack/commits/22ca722b99ad8f99e37c7f09b2788d8392d684ff))
* 🎸 apply theme ([e44be28](https://bitbucket.org/mangomap/mapstack/commits/e44be28cbaac3e715b0b1ae2542173215154c1b1))
* 🎸 chip with add icon ([6a92700](https://bitbucket.org/mangomap/mapstack/commits/6a927007782cb83bd8b10fe97be809269743d2d5))
* 🎸 chipinput ([ecc4daf](https://bitbucket.org/mangomap/mapstack/commits/ecc4daf310743d2f5eb9572f56165aee1a123970))
* 🎸 clear highligh when filter is cleared ([faca3a7](https://bitbucket.org/mangomap/mapstack/commits/faca3a776f5ca3032cf545e904276dd75b25b076))
* 🎸 clear selectionSet before highlight new one ([bcae0d1](https://bitbucket.org/mangomap/mapstack/commits/bcae0d118d5ccc846594e1aac7ee488063fec40d))
* 🎸 close side panel when applied feature highlight ([a5eca0e](https://bitbucket.org/mangomap/mapstack/commits/a5eca0e7c0b460c7f745fb4bae7bfad5a1e56172))
* 🎸 created story for propChips ([b84d965](https://bitbucket.org/mangomap/mapstack/commits/b84d96505ebeb815336c9d4131398699da4f8b4b))
* 🎸 edit fitler ([57a47c4](https://bitbucket.org/mangomap/mapstack/commits/57a47c4e0858817f41c36d62e931a0c5f6021519))
* 🎸 editing filter ([c980a53](https://bitbucket.org/mangomap/mapstack/commits/c980a53aebcd430bd5ec1b801cdf7080be4b2f85))
* 🎸 filter ([a8d710e](https://bitbucket.org/mangomap/mapstack/commits/a8d710e78227b741eb2fc196eef9dfe2aa05421a))
* 🎸 highlight feature when selectionSet is selected ([78af0d7](https://bitbucket.org/mangomap/mapstack/commits/78af0d7c1b0892c7dd2cbaa498ee26c68a184e18))
* 🎸 highlight selection set ([e55212c](https://bitbucket.org/mangomap/mapstack/commits/e55212cdd2b6f55e11faa5cbba32744d7408a8f0))
* 🎸 implement chip and buttons ([8af261c](https://bitbucket.org/mangomap/mapstack/commits/8af261ccac0d97d2dfb4f9b90c3fe56d2f8788f6))
* 🎸 implement edit filter ([83a99f6](https://bitbucket.org/mangomap/mapstack/commits/83a99f61885c2f05956df39a69baadb63b72e717))
* 🎸 implement regularbutton and textinput ([f12e742](https://bitbucket.org/mangomap/mapstack/commits/f12e74234fb293d4b6fee39166b7e2f062f5128d))
* 🎸 implemented close ([47f8de3](https://bitbucket.org/mangomap/mapstack/commits/47f8de3d78af70d1f57a0a14c301c2f5beeb6eb8))
* 🎸 remove feature highlight when filter is removed ([ff53a61](https://bitbucket.org/mangomap/mapstack/commits/ff53a6139d93cc3436c4b473e2164ad1a190a912))
* 🎸 set options for autocomplete chipinput ([c11fba4](https://bitbucket.org/mangomap/mapstack/commits/c11fba4e3f330971f231feebf15b80c13af241ea))
* 🎸 show filter list ([0117d8f](https://bitbucket.org/mangomap/mapstack/commits/0117d8fe0fdc9fa7988e4eeb17735facf8b8b2af))
* 🎸 show filter list ([4b2ae3c](https://bitbucket.org/mangomap/mapstack/commits/4b2ae3c5dba5db7f9588a503fabd057b2f9e132c))
* 🎸 storybook for atoms ([b3c6e18](https://bitbucket.org/mangomap/mapstack/commits/b3c6e185d0a4da71d9b1553a4af8630ed52b8e46))
* 🎸 ui tweak ([9e41dc9](https://bitbucket.org/mangomap/mapstack/commits/9e41dc9f2aab17e14e36980c80374db831685848))
* 🎸 update chipinput ([56a8d1f](https://bitbucket.org/mangomap/mapstack/commits/56a8d1f14d986fccea8eee5ec727ea52dfa4d6a4))
* 🎸 update regular button props for styling ([99121f6](https://bitbucket.org/mangomap/mapstack/commits/99121f662d1b03bcc15b9530cbbe3a1e1e64607c))
* 🎸 update storybook ([27c1ac0](https://bitbucket.org/mangomap/mapstack/commits/27c1ac0c4cd3dc754f6c2cdfcfc11ea0635ea319))
* 🎸 update theme and style ([e2ee055](https://bitbucket.org/mangomap/mapstack/commits/e2ee0559080531c4cdea051eef7c007381d5d0ee))
* 🎸 update theme of chipinput ([f520bf0](https://bitbucket.org/mangomap/mapstack/commits/f520bf095663120c92af4d6f2d2572a42acce8fa))
* 🎸 update to ui-component filterEdit ([ffe93a6](https://bitbucket.org/mangomap/mapstack/commits/ffe93a6ca6034046b59f605cc2068ec75418140d))
* 🎸 updated ([657b4a7](https://bitbucket.org/mangomap/mapstack/commits/657b4a7a719852f05adc3cbf9ead36b30b56cf57))
* 🎸 updated theme ([0ed97df](https://bitbucket.org/mangomap/mapstack/commits/0ed97dfe229f071df100720193c1a4dabf95b1fe))
* implement chip for filter ([b032ac1](https://bitbucket.org/mangomap/mapstack/commits/b032ac113c3f71da622a51d5fc5322638bb4f6b5))
